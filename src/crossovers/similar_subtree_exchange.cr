class GP::Crossover
  def self.similar_subtree_exchange(cx_prob,max_size,env) : Crossover 
    proc = ->(parents : Array(Array(Indv)), pop : Array(Indv)) do 
      parents.map do |ary| 
        next ary.sample.clone if rand > cx_prob 
        p1,p2 = ary 
        
        p1 = p1.clone 
        p2 = p2.clone 
        
        p1index = (0 ... p1.size) 
        p2index = (0 ... p2n.size).min_by do |p2index| 
          Dist.euc_dist(p1[p1index].semantic, p2[p2index].semantic) 
        end 

        nsexp = self.exchange_nodes(p1[p1index].sexp,p2[p2index].sexp)

        next ary.sample.clone if nsex.size > max_size 
        indv = Indv.new(sexp: nsexp, env: env.clone) 
        indv.age = Math.max(p1.age,p2.age) 
        indv 
      end 
    end 

    return Crossoer.new("similar-subree-exchange",proc) 
  end 





  def self.similar_subtree_exchange(p1, p2, similarity) : Sexp
    p1 = p1.clone
    p2 = p2.clone
    
    p1n = rand(0 ... p1.size)

    # Find a similar node in p2 
    p2index = (0 ... p2n.size).min_by do |p2index|
      similarity.call(p1n,p2index)
    end 
    
    p2n = p2[p2index] 

    return self.exchange_nodes(p1n,p2n)
  end

end 
