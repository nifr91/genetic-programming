require "../individual.cr" 
# Object that represents a crossover operator 
#
class GP::Crossover
  @proc : Proc(Array(Array(Indv)),Array(Indv),Array(Indv))
  @name : String
  def initialize(
    @name = "random-parent",
    @proc = ->(parents : Array(Array(Indv)), pop : Array(Indv)) do 
      parents.map{|i| i.sample}
    end)
  end 
  def call(*args) 
    @proc.call(*args) 
  end 
  def to_s(io)
    io << @name
  end
end 

require "./subtree_exchange.cr"

