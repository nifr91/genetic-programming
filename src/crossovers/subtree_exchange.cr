class GP::Crossover


  # Basic crossover operator, one node from each parent is selected 
  # with uniform probability and the node is substituted in the first parent
  # 
  # p1 : sexp -- s-exp of the first parent 
  # p2 : sexp -- s-exp of the second parent 
  #
  # sexp -- returns an sexpression with two random nodes interchange from 
  #         both parents 
  #
  def self.subtree_exchange(cx_prob,max_size,env) : Crossover
    proc = ->(parents : Array(Array(Indv)), pop : Array(Indv)) do 
      parents.map do |ary|
        if rand > cx_prob 
          ary.sample.clone
        else 
          p1,p2 = ary 
          nsexp = self.subtree_exchange(p1.sexp,p2.sexp)
          if nsexp.size >= max_size
            indv = ary.sample.clone
          else 
            indv = Indv.new(sexp: nsexp, env: env.clone)
            indv.age = Math.max(p1.age,p2.age) 
            indv
          end 
        end 
      end 
    end 

    return Crossover.new("subtree_exchange",proc)
  end 


  # Basic crossover operator, one node from each parent is selected 
  # with uniform probability and the node is substituted in the first parent
  # 
  # p1 : sexp -- s-exp of the first parent 
  # p2 : sexp -- s-exp of the second parent 
  #
  # sexp -- returns an sexpression with two random nodes interchange from 
  #         both parents 
  #
  def self.subtree_exchange(p1 : Sexp,p2 : Sexp) : Sexp
    # Clone the parents so as to not modify the population
    p1 = p1.clone
    p2 = p2.clone 
    
    # Select a random node from each tree
    p1n = p1.sample
    p2n = p2.sample

  
    # Get parents of selected nodes 
    p1p = p1n.parent? 
    p2p = p2n.parent? 

    # If the node from p1 is not the root (i.e it has a parent), then 
    # the p1n node is substituted from the parent with p2n. If the 
    # node from p1 is the root parent then the new node from p2  is 
    # a new root node and is returned
    if p1p
      p1p.child_sub(p1p.index(p1n),p2n)
      return p1
    else
      p2n.parent = nil if p2p
      return p2
    end 
  end 
end 
