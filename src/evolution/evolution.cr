require "../individual.cr" 
require "../timer.cr"
require "../crossovers/crossover.cr" 
require "../stop-criteria/stop-criteria.cr"
require "../init-methods/init-method.cr" 
require "../selections/selection.cr"
require "../mutations/mutation.cr" 
require "../replacements/replacement.cr" 

class GP::Evolution(T)
  property best        : T        = (T.new{})
  property pop         : Array(T) = [] of T
  property off         : Array(T) = [] of T
  property parents     : Array(Array(T)) = [] of Array(T)
  property gen         : Int32       = 0
  property timer       : Timer       = Timer.new
  property num_evals   : Int32       = 0
  property pop_init    : InitMethod        = InitMethod.new
  property selection   : Selection      = Selection.new
  property crossover   : Crossover   = Crossover.new
  property mutation    : Mutation         = Mutation.new
  property replacement : Replacement     = Replacement.new
  property stop_crit   : StopCriteria    = StopCriteria.new 
  property indv_eval   : (T) -> Void = ->(i : T){ i.fit = -1/0.0}  
  property select_best : (Array(T)) -> T = ->Selection.best_indv(Array(T)) 
  
  property! on_new_gen     : (Proc(Void))  | Nil 
  property! after_selection   : (Proc(Void))  | Nil 
  property! after_crossover   : (Proc(Void))  | Nil 
  property! after_mutation    : (Proc(Void))  | Nil 
  property! after_replacement : (Proc(Void))  | Nil 
  property! on_end_gen     : (Proc(Void))  | Nil 
  property! on_return      : (Proc(Void))  | Nil 
  property! on_enter       : (Proc(Void))  | Nil 

  def self.new 
    evol = Evolution(T).new 
    yield evol 
    return evol 
  end 

  # Basic evolution structure for a genetic algorithm it has the following phases
  #
  #
  # | population initialization  
  # | until a stop criteria is reached 
  # | | select parents 
  # | | crossover 
  # | | mutation 
  # | | evaluate individuals 
  # | | replacement 
  # | end 
  # 
  def evolve() 
    self.on_enter.call if self.on_enter? 

    # Init population 
    @pop = self.pop_init.call
     
    # Evaluate population 
    @pop.each do |indv|
      self.indv_eval.call(indv)
      @num_evals += 1
    end

    @best = select_best.call(pop).clone 
    @timer.tic 
    
    until self.stop_crit.call() 
      self.on_new_gen.call if self.on_new_gen?
      
      @parents = self.selection.call(@pop) 
      self.after_selection.call if self.after_selection?
      
      @off = self.crossover.call(@parents,@pop) 
      self.after_crossover.call if self.after_crossover?

      @off = self.mutation.call(@off,@pop) 
      self.after_mutation.call if self.after_mutation?
  
      # Evaluate the offspring  only if necessary, which should be indicated
      # by a positive infinite fitness (i.e 1/0.0) 
      @off.each do |indv| 
        next if indv.fit.finite?
        self.indv_eval.call(indv) 
        @num_evals += 1
      end

      @pop = self.replacement.call(@off,@pop) 
      self.after_replacement.call if self.after_replacement?
      
      # get best individual
      genbest = self.select_best.call(pop).clone
      @best = self.select_best.call([@best,genbest])
  
      @gen += 1 
      self.on_end_gen.call if self.on_end_gen?
    end 
    
    self.on_return.call  if self.on_return? 
    return @best
  end 
  
end 
