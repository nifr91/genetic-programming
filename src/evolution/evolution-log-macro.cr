class GP::Evolution
  macro logs() 
    # If requested the configuration is logged 
    if conf["log-config"] 
      evo.on_enter = Proc(Void).new do 
        log_array = conf.to_a.map do |key,val| 
          case val 
          when Iterable 
            keyarr = [key] of (typeof(key)|typeof(val.first))
            val.each do |v| keyarr.push(v) end 
            keyarr
          else {key.to_s,val.to_s} 
          end
        end
        .reject do |e| 
          es = e[0].to_s 
          test = (!conf["log-traindata"] && es =~ /^.train$/ ) 
          test = test || (!conf["log-valdata"] && es =~ /^.val$/) 
        end 
        log.puts(log_array)
      end 
    end  
  

    # At the end the solution 
    evo.on_return = Proc(Void).new do 
      best_sol = evo.best.clone 
      best_log = { {"train-fit",best_sol.fit} }
      unless conf["xval"].empty?
        best_sol.evaluate(conf["xval"], conf["yval"],
          conf["vars"], obj_fun, conf["infinity"])
        best_log = best_log + ({ {"val-fit", best_sol.fit} })
      end 
      best_log = best_log + { {"size",best_sol.sexp.size} }
      best_log = best_log + { {"best-sol", best_sol} }
      log.puts(best_log)
    end 



    # At the start of each generation some stats are logged 
    %get_pair_diffs = ->(arr : Array(Float64)) do 
      pair_diffs = [] of Float64
      (0 ... arr.size).each do |i|
        (i+1 ... arr.size).each do |j| 
          dfit = (arr[i] - arr[j]).abs
          pair_diffs.push(dfit)
        end
      end
      pair_diffs.push(0) if pair_diffs.empty? 
      return pair_diffs
    end 

    %get_median = ->(arr : Array(Float64)) do 
      arr = arr.sort
      mid = (arr.size - 1) // 2
      median = if arr.size.odd? 
        arr[mid]
      else 
       (arr[mid] + arr[mid+1]) / 2.0
      end
      return median 
    end 
    %prev_time = evo.timer.toc 
    %log_acc_time = 0.0
    %log_elapsed_time = 0.0
    evo.on_new_gen = Proc(Void).new do 
      if conf["log-evo"] 
        %log_start = evo.timer.toc

        # Calculamos el fitness del mejor individuo en validación 
        best_copy = evo.best.clone 
        best_copy.evaluate(conf["xval"],conf["yval"],conf["vars"],obj_fun,conf["infinity"])

        lt = {
          {"generation",evo.gen},
          {"time","%0.12e" % evo.timer.toc}, 
          {"evaluations",evo.num_evals},
          {"best-size", evo.best.sexp.size}, 
          {"best-train-fit","%0.12e" %  evo.best.fit},
          {"best-val-fit",   "%0.12e" % best_copy.fit}  }
      
        # iteration time 
        lt += { {"iter-time", "%0.12e" % (evo.timer.toc - (%prev_time + %log_elapsed_time))} }

        # population size 
        lt += { {"pop-size",evo.pop.size}  } 

        if conf["log-stats"] 
          {{yield }}
          
          # pop avg size 
          pop_avg_size = evo.pop.reduce(0) do |a,e| a + e.sexp.size end / evo.pop.size 
          lt += { {"avg-size","%d" % pop_avg_size} }
          
          # dcn edit2-distance 
          edit2 = ->(a : Indv, b : Indv) do Distance.edit2_normalized(a.sexp,b.sexp,0.5)  end 
          dist_array = Distance.dcn(evo.pop,edit2,1e-12)
          d = dist_array.reduce(0.0) do |a,e| a + e end 
          d /= evo.pop.size 
          lt += { {"avg-dcn-edit2","%0.12e" % d} }
            
          # overlap distance 
          avg_overlap = evo.pop.map_with_index do |indv_a,i|
            avg = 0.0 
            evo.pop.each_with_index do |indv_b,j| 
              next if i== j 
              avg += Distance.overlap_normalized(indv_a.sexp,indv_b.sexp) 
            end 
            avg /= (evo.pop.size-1) 
          end 
          avg_avg_overlap = avg_overlap.reduce(0.0) do |a,avg| a + avg end 
          avg_avg_overlap /= avg_overlap.size 
          lt += { {"avg-overlap","%0.12e" % avg_avg_overlap} } 

          # size 
          avg_size = evo.pop.reduce(0.0) do |a,e| a + e.sexp.size end 
          avg_size /= evo.pop.size 
          
          # evaluación de la población en entrenamiento 
          pop_train_fit   = evo.pop.map do |indv| indv.fit end 
          pairs_fit_dists = %get_pair_diffs.call(pop_train_fit) 
          median = %get_median.call(pairs_fit_dists) 
          lt += { {"median-pair-train-fit-dist", "%0.12e" % median} } 
          
          # mediana de fitness 
          plen = evo.pop.size 
          mid = (plen - 1) // 2
          median_per_indv = pop_train_fit.map_with_index do |fit,i|
            distances = pop_train_fit.map do |f| (fit - f).abs end 
            %get_median.call(distances)
          end 
          median = %get_median.call(median_per_indv) 
          lt += { {"median-fit-dist", "%0.12e" % median} } 

          # sampling semantic distance 
          pop_sem = evo.pop.map do |i| i.semantic end 
          median_ssd = pop_sem.map_with_index do |semi,i|
            n = semi.size 
            ssds = pop_sem.map_with_index do |semj,j|
              ssd = (0 ... n).reduce(0.0) do |a,k|
                a + (semi[k] - semj[k]).abs
              end 
              ssd /= n
            end 
            %get_median.call(ssds) 
          end 
          median = %get_median.call(median_ssd) 
          lt += { {"median-ssd","%0.12e" %  median} } 

          
 
          # Número de individuos unicos 
          lt += { {"uniq-elem",evo.pop.uniq.size} } 


          # Densidad del marcador más frecuente con altura 5
          markercount = {} of String => Int32
          evo.pop.each do |indv| 
            marker = Sexp.genetic_marker(indv.sexp,0,5)
            markercount[marker] = (markercount[marker]? || 0) + 1
          end 
          lt += { {"mark-cnt",markercount.size},
            {"max-mark-den","%0.12e" % (markercount.values.max / evo.pop.size) } } 
        end 

        # Población 
        if conf["log-pop"] 
          lt = lt + {["population"] + (evo.pop.sort_by do |e| e.fit end)}
        end 

        %prev_time = %log_start  
        %log_elapsed_time = (evo.timer.toc) - %log_start
        lt += { {"log-elapsed-time", "%0.12e" % %log_elapsed_time}}  

        log.puts(lt) 
      end 
    end

  end 

end 
