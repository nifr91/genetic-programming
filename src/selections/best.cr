class GP::Selection

  # Get the best individual  by fitness from an array 
  def self.best_indv(ary : Array(Indv))
    best = ary[0]
    ary.each do |indv|
      next if best.fit < indv.fit
      if (best.fit > indv.fit) || (best.sexp.size > indv.sexp.size)
        best = indv
      end 
    end 
    return best
  end 

end 
