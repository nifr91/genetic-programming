class GP::Selection
# -------------------------------------
    
    # lexicase :: Epsilon lexicase 
    #
    # Epsilon lexicase selection for regression 
    # La Cava, Spector, Danai
    # Proceedings of the genetic and evolutionaru computation conference
    # 10.1145/2908812.2908898
    #
    # It randomly selects individuals with different strenghts. To select a parent
    # , test cases are randomly selected and potential individuals are filtered
    # based on their performance on the case, until either tehere are no more
    # test cases or only one individual is fit. 
    #
    def self.lexicase(ary,cases_fit)

      candidates = ary.size.times.map{|i| i}.to_a
      # Shuffle cases
      cases = cases_fit.first.size.times.map{|i| i}.to_a
      cases.shuffle!

      fit = Array(Float64).new(candidates.size)
      fitaux = Array(Float64).new(candidates.size)

      # Loop over the test cases 
      until cases.empty? || (candidates.size == 1)
        t = cases.pop 
        fit.clear
        fitaux.clear
        candidates.each do |ci|
          cf = cases_fit[ci][t]
          fit.push(cf)
          fitaux.push(cf)
        end 

        # Median 
        fitaux.sort!
        n = fit.size 
        mf = (fitaux[((n)/2).floor.to_i32] + fitaux[((n)/2).ceil.to_i32]) / 2

        # Calcular Median Absolute Deviation
        absdev = fit.size.times.map{|i| (fit[i] - mf).abs}.to_a.sort!
        mad = (absdev[((n)/2).floor.to_i32] + absdev[((n)/2).ceil.to_i32]) / 2

        # Filter individuals by cases 
        ptr = 0
        eps = fit.min + mad 
        candidates.size.times do |i| 
          if fit[i] <= eps
            candidates[ptr] = candidates[i]
            ptr += 1
          end 
        end 
        # Select a random solution from filtered candidates 
        candidates.pop(candidates.size-ptr)
      end 

      return ary[candidates.sample]
    end
end 
