require "../individual.cr" 

  class GP::Selection
    @proc : Proc(Array(Indv),Array(Array(Indv)))
    @name : String
    def initialize(
      @name = "random",
      @proc = ->(p : Array(Indv)){p.map{ [p.sample,p.sample]}})
    end 

    def call(*args) 
      @proc.call(*args) 
    end 

    def to_s(io)
      io << @name
    end
end 
require "./*"
