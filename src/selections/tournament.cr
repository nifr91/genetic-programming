class GP::Selection 
 def self.tournament(off_size : Int,tournament_size : Int)
   proc = ->(pop : Array(Indv)) do 
     off = [] of Array(Indv)
     off_size.times do |i| 
       p1 = Selection.tournament(pop,tournament_size){|indv| indv.fit}
       p2 = Selection.tournament(pop,tournament_size){|indv| indv.fit}
       off.push([p1,p2])
     end 
     off
   end 
  return self.new("tournament",proc)
 end 

 
  # Tournament selection of size 'tsize', it yields the element for 
  # the minimizing comparison. 
  #
  # ary : [object] -- an array 
  # tsize : int   -- size of the tournament 
  # 
  # object -- returns the object in a subset of size 'tsize' of 'ary' 
  #           that minimizes the yielded expression
  #
  def self.tournament(ary : Iterable,tsize : Int) 
    return ary.sample(tsize).min_by{|e| yield e}
  end 

  # Tournament selection of size 'tsize', it yields the element for 
  # the minimizing comparison.
  #
  # ary   : [object] -- an array
  # tsize : int      -- size of the tournament 
  #
  # int -- index of the object in a subsample of size 'tsize' of 'ary' 
  #        that minimizes the yielded expression 
  #
  def self.tournament_index(ary : Iterable,tsize : Int)
    return (0 ... ary.size).to_a.sample(tsize).min_by{|i| yield ary[i]}
  end 
end 
