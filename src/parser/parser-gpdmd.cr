module GP::ConfigParser
  def dmdgp_opts(ll) 
    return NamedTuple.new(
      **self.base(ll),
      **find("edit2-depth-wgt",ll,Float64),
      **find("dmdgp-req-dist",ll,Float64))
  end 
end
