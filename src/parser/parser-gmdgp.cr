module GP::ConfigParser
  
  def gmdgp_opts(ll) 
    NamedTuple.new(
      **self.base(ll),
      **find("gmdgp-marker-mindepth",ll,Int32),
      **find("gmdgp-marker-maxdepth",ll,Int32))
  end 
end 
