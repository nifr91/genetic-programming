module GP::ConfigParser
  def alps_opts(ll) 
    return NamedTuple.new(
      **self.base(ll),
      **find("edit2-depth-wgt",ll,Float64),
      **find("alps-num-layers",ll,Int32),
      **find("alps-num-elites",ll,Int32),
      **find("alps-age-gap",ll,Int32))
  end 
end
