module GP::ConfigParser
  
  def knobelty_opts(ll) 
    NamedTuple.new(
      **self.base(ll),
      **find("knobelty-sample-size",ll,Int32),
      **find("knobelty-lambda",ll,Float64),
      **find("knobelty-archive-size",ll,Int32))
  end 
end 
