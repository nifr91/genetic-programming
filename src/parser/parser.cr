require "../logger/loglist.cr" 

module GP::ConfigParser
  extend self 

  def config(**opts)
    NamedTuple.new(**opts)
  end 

  macro find(key,ll,type)
    %lst = {{ll}}.assq_ref({{key}}) 
    raise "expected parameter: \"#{{{key}}}\" " if %lst.empty?
    %val = %lst.car.as(String)
    {% if type.stringify.downcase =~ /^string$/ %}
      NamedTuple.new({{key}}: %val)
    {% elsif type.stringify.downcase =~ /^int$/ %}
      NamedTuple.new({{key}}: {{type}}.new(%val.to_f64))
    {% elsif type.stringify.downcase =~ /^bool$/ %}
      NamedTuple.new({{key}}: !(%val.downcase =~ /^f(alse)?/))
    {% elsif type.stringify.downcase =~ /^array/ %}
      %ary = {% if type.stringify.downcase =~ /int/ %}
        %lst.each.map do |e| e.as(String).to_i32 end.to_a
      {% elsif type.stringify.downcase =~ /float/ %} 
        %lst.each.map do |e| e.as(String).to_f64 end.to_a
      {% else %}
        %lst.each.map do |e| e.as(String) end.to_a
      {% end %} 
      NamedTuple.new({{key}}: %ary)
    {% else  %}
      NamedTuple.new({{key}}: {{type}}.new(%val))
    {% end %} 
  end 
  
  def argv2alist(args = ARGV) 
    String.build do |alist|
      alist << "("
      alist << "(configuration v2.0)"
      doubled_dash_index = 0
      handled_args = [] of Int32 
      arg_index = 0 
      while arg_index < args.size 
        arg = args[arg_index] 

        # -- means to stop parsing arguments 
        if arg == "--" 
          doubled_dash_index = arg_index
          handled_args  << arg_index 
          break 
        end 

        # val = "" 
        if arg.starts_with?("--")
          val_index = arg.index("=")
          if val_index
            flag = arg[2...val_index]
            val = arg[val_index + 1 ..]
            arg_index += 1
          else
            flag = arg[2..]
            val = args[arg_index+1]
            arg_index += 2
          end 
        elsif arg.starts_with?("-") 
          flag = arg[1..]
          val = "true" 
          arg_index += 1
        else 
          raise %(unknown argument pair "#{arg}") 
        end
        
        alist << "\n" 
        alist << "("
        alist << flag << " "
        if val =~ /\s/ 
          val= %("#{val}")
        end 
        if val =~ /:/
          val = val
            .split(":")
            .map do |e| %<(#{e})> end
            .join(" ")
        end 
        if val =~ /,/ 
          val = val
            .split(",").join(" ")
        end 
        alist << val  << ")"
      end 
      alist << ")"
    end 
  end 

  def base(ll) 

    data_conf = ConfigParser.data(ll)
    names_conf = ConfigParser.names(ll) 
    env_conf = ConfigParser.environment(ll) 


    cnt = 0
    while env_conf["vars"].size < data_conf["xtrain"].first.size 
      env_conf["vars"].push("x#{cnt}")
      cnt += 1
    end  
    while env_conf["vars"].size > data_conf["xtrain"].first.size 
      env_conf["vars"].pop
    end

    gpparams_conf = ConfigParser.base_gp_params(ll) 

    conf = find("configuration",ll,String)
      .merge(names_conf)
      .merge(env_conf)
      .merge(gpparams_conf)
      .merge(data_conf)

    return conf 
  end 

 
  def base_gp_params(ll) 
    return NamedTuple.new(
      **{date: Time.local},
      # Base gp params 
      **find("pop-size",ll,Int32),
      **find("off-size",ll,Int32),
      **find("max-size",ll,Int32),
      **find("rand-tree-min-depth",ll,Int32),
      **find("rand-tree-max-depth",ll,Int32),
      **find("tournament-size",ll,Int32),
      **find("max-depth",ll,Int32),
      **find("mut-prob",ll,Float64),
      **find("cx-prob",ll,Float64),
      # Constants 
      **find("infinity",ll,Float64),
      **find("epsilon",ll,Float64),
      # Flags 
      **find("log-config",ll,Bool),
      **find("log-pop",ll,Bool),
      **find("log-traindata",ll,Bool),
      **find("log-valdata",ll,Bool),
      **find("log-evo",ll,Bool),
      **find("log-stats",ll,Bool))

  end 
  

  def environment(ll) 
    vars = [] of String
    funs = [] of String 
    terms = [] of String 

    lst = ll.assq_ref("vars")
    unless lst.empty? 
      vars = lst.each.map do |e| e.as(String)  end.to_a 
    end 
    
    lst = ll.assq_ref("funs") 
    unless lst.empty? 
      funs = lst.each.map do |e| e.as(String)  end.to_a 
    end 

    lst = ll.assq_ref("terms") 
    unless lst.empty? 
      terms = lst.each.map do |e| e.as(String)  end.to_a 
    end 
 
    
    return {"vars": vars, "terms": terms, "funs": funs}
  end 

  # TODO Al leer la 'll' un string no debe contener \". 
  def names(ll) 
    run_name = "" 
    dataset_name = "" 
    alg_name = "" 

    lst = ll.assq_ref("run-name") 
    unless lst.empty? 
      run_name = lst.car.as(String).gsub("\"","") 
    end 
     
    lst = ll.assq_ref("dataset-name") 
    unless lst.empty?
      dataset_name = lst.car.as(String).gsub("\"","") 
    end

    lst = ll.assq_ref("alg-name") 
    unless lst.empty?
      alg_name = lst.car.as(String).gsub("\"","") 
    end
     
    return {
      "run-name": run_name,
      "dataset-name": dataset_name,
      "alg-name": alg_name,
      }
  end 


  def data(ll)
    ytrain = [] of Float64
    xval   = [] of Array(Float64) 
    yval   = [] of Float64 

    # t1 - [x0 x1 ... xn]
    # ...
    # tn - [x0 x1 ... xn]
    lst = ll.assq_ref("xtrain")
    unless lst.empty?
      xtrain = self.load_xdata(lst) 
    else 
      lst = ll.assq_ref("xtrain-file") 
      raise "required xtrain values" if lst.empty? 
      xtrain = self.load_xdata(lst.car.as(String).gsub("\"",""))
    end 

    lst = ll.assq_ref("ytrain")
    unless lst.empty?
      ytrain = load_ydata(lst) 
    else 
      lst = ll.assq_ref("ytrain-file") 
      raise "required ytrain values" if lst.empty? 
      ytrain = self.load_ydata(lst.car.as(String).gsub("\"",""))
    end 

    lst = ll.assq_ref("xval")
    unless lst.empty?
      xval = self.load_xdata(lst)
    else 
      lst = ll.assq_ref("xval-file") 
      xval = self.load_xdata(lst.car.as(String).gsub("\"","")) unless lst.empty?
    end 

    lst = ll.assq_ref("yval")
    unless lst.empty?
      yval = load_ydata(lst) 
    else 
      lst = ll.assq_ref("yval-file") 
      yval = self.load_ydata(lst.car.as(String).gsub("\"","")) unless lst.empty?
    end

    return {"xtrain": xtrain,"ytrain": ytrain,"xval": xval, "yval": yval}
  end 

    # Loads the input data from the specified file 
    #
    # The expected format is: each line is a input data sample, and on 
    # each linea every column (space separated) is a variable. 
    #
    # | x00 x01 x02 x03 ... 
    # | x10 x11 x12 x13 ... 
    # | ... 
    #
    # fname : string -- name of the file containing the input data
    # 
    # [[float]] -- vector with data entries 
    #
    def load_xdata(fname)
      return xs = File
        .read_lines(fname)
        .map{|l| l
          .split(/\s+/)
          .map{|e| e.to_f64}}
    end 

    def load_xdata(lst : LogList) 
      xs = lst.each.map do |row|
        values = row.as(LogList).each.map do |e|
          e.as(String).to_f64
        end
        values.to_a 
      end
      return xs.to_a 
    end 

    # Loads the expected output data from the specified file 
    #
    # The expected format is: each line is a output data sample
    #
    # | y0 
    # | y1 
    # | ... 
    #
    # fname : string -- name of the file containing the input data
    # 
    # [[float]] -- vector with data entries 
    #
    def load_ydata(fname)
      return self.load_xdata(fname).map(&.first)
    end 

    def load_ydata(lst : LogList)
      ys = lst.each.map do |e|
        val = e.as(String).to_f64
      end 
      return ys.to_a 
    end 
end 

require "./*"
