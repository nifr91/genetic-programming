module GP::ConfigParser
  
  def sis_opts(ll) 
    NamedTuple.new(
      **self.base(ll),
      **find("sis-semantic-threshold",ll,Int32))
  end 
end 
