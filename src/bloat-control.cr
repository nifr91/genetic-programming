# Métodos de control de bloat 
#
module GP::Bloat

  # Tarpeian method for bloat-control 
  #
  # Random selection of a fixed proportion of the individuals in the 
  # population among whose size is above the current average program size
  # Their fitness is set tot the lowest possible value available. This 
  # effectively means that those individuals will not be used to create the 
  # next generation.
  #
  # "A simple but Theoretically-Motivated Method to Control Bloat in Genetic 
  # Programming" 
  # Poli, Riccardo 
  # European Conference on Genetic Programming 2003
  # 
  def self.tarpeian(pop,parsimony,bad_fitness = 1/0.0,log : Nil|Logger = nil)
    # Get the average length 
    mean_len = pop.reduce(0){|acc,indv|  acc + indv.len} // pop.size 

    # For each population, if its size is greater than average length and 
    # with a probability 'w' its fitness is penalized
    pop.size.times do |i|
      pop[i].pfit = pop[i].fit
      next if pop[i].len < mean_len 
      next if rand(0.0..1.0) > parsimony
      pop[i].pfit = bad_fitness
    end 
  end

  # Double Tournament for bloat-control
  #
  # Selects and individual using tournament selection however the tournament
  # contestants are each a winner from another tournament selection. The
  # algorithm has two parameters, fitness tournament size, and a parsimony
  # value that controls the pressure, 0 = corresponds to an random selection
  # between the larger and shorter individual and a value of 1 is a tournament
  # of size 2 using the length.
  #
  # "Fighting bloat with non parametric parsimony pressure"
  # Luke,Sean and Liviu Panait
  # International Conference on Parallel Solving from Nature 2002
  #
  def self.double_tournament(pop,parsimony,tsize=2,log : Nil|Logger = nil)
    p1 = Select.tournament(pop,tsize){|i| i.fit} 
    p2 = Select.tournament(pop,tsize){|i| i.fit}

    if rand < ((1.0 + parsimony)/2)
      (pop[p1].len < pop[p2].len) ? p1 : p2
    else 
      (pop[p1].len > pop[p2].len) ? p1 : p2
    end 
  end 

  # Proportional Tournament for bloat control 
  #
  # The proportional tournament selects and individual using tournament 
  # selection as usual, using some fixed tournament size. However, a 
  # proportion of tournaments will select based on parsimony rather than on 
  # fitness. A fixed parameter defines the proportion, where 1 implies all
  # tournaments will select based on fitness, while 0.5 implies that tournaments
  # will select on fitness or size with equal probability.
  # 
  # "Fighting bloat with non parametric parsimony pressure"
  # Luke,Sean and Liviu Panait
  # International Conference on Parallel Solving from Nature 2002
  #
  def self.proportional_tournament(pop,tsize,parsimony,log : Nil|Logger = nil)
    if rand < parsimony 
      Select.tournament(pop,tsize){|i| i.fit}
    else 
      Select.tournament(pop,tsize){|i| i.len}
    end 
  end 



  def self.dcn_tarpeian(pop, new_pop, dist,parsimony,log : Nil|Logger = nil)
    pop_sz = new_pop.size 
    new_pop.clear
    pop.swap(Select.best(pop),-1) 
    new_pop.push(pop.pop)

    mean_len = pop.len.reduce(0){|acc,l|  acc + l} // pop.size 

    Div.dcn_edit_dist(pop,new_pop)

    (1 ... pop_sz).each do |i|
      # Calcular la distancia 
      # Actualizar la distancia al vecino mas cercano en la nueva población
      (0 ... pop.size).each do |j| 
        d,t = Div.edit_dist2(pop.sexp[j],new_pop.sexp[-1])
        d = d/t
        # Penalización por distancia a la población
        pop.dist[j] = d if d < pop.dist[j] 

        # Penalización por distancia del programa
        next if pop.len[i] < mean_len 
        next if rand(0.0..1.0) > parsimony
        pop.pen_fit[i] = 1/0.0
      end 

      #Penalizar 
      (0 ... pop.size).each do |j| 
        pop.pen_fit[j] = (pop.dist[j] < dist) ? 1/0.0 : pop.fit[j]
      end 

      # Seleccionar el mejor de los no penalizados, si están todos penalizados
      # se elije el más alejado
      index = 0
      (0 ... pop.size).each do |j|
        next if pop.pen_fit[index] < pop.pen_fit[j] 

        if pop.pen_fit[j] < pop.pen_fit[index]
          index = j
        elsif pop.dist[j] > pop.dist[index] 
          index = j 
        elsif pop.len[j] < pop.len[index]
          index = j 
        end 
      end

      # Agregar el individuo seleccionado
      new_pop.push(pop.swap(index,-1).pop)
    end 

    new_pop 
  end 


  # Reemplazamiento por frente de pareto se seleccionan los 
  # elementos no dominados
  #
  def self.dcn_pareto(pop,new_pop,dist,log : Nil|Logger = nil)
    pop_sz = new_pop.size 
    new_pop.clear 
    pop.swap(Select.best(pop),-1) 
    new_pop.push(pop.pop)

    Div.dcn_edit_dist(pop,new_pop)

    (1 ... pop_sz).each do |i|
      # Calcular la distancia 
      # Actualizar la distancia al vecino mas cercano en la nueva población
      (0 ... pop.size).each do |j| 
        d,t = Div.edit_dist2(pop.sexp[j],new_pop.sexp[-1])
        d = d/t
        pop.dist[j] = d if d < pop.dist[j] 
      end 

      #Penalizar 
      (0 ... pop.size).each do |j| 
        pop.pen_fit[j] = (pop.dist[j] < dist) ? 1/0.0 : pop.fit[j]
      end 

      # Seleccionar un elemento aleatoriamente del frente 
      index = MultObj.get_nondom(pop).sample()

      # Agregar el individuo seleccionado
      new_pop.push(pop.swap(index,-1).pop)
    end 

    return new_pop
  end 

end 
