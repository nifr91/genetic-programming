require "../gp.cr"

###############################################################################
# DMDGP :: Dynamic Management of Diversity for Genetic Programming 
#
# DMDGP: Dynamic Management of Diversity for Genetic Programming 
# Nieto-Fuentes R., Carlos Segura 
# journal
# doi 
#
# It is a replacement method that uses a multi-objective apporach with 
# tuple (fitness,size). Its novelty is a dynamic diversity management as a
# function of the stopping criteria, enforced by a fitness penalization of 
# genotypically similar individuals to the already selected solutions. 
# The key idea is to allow less fit individuals to be selected for the next
# generation and gradually start selecting fitter individuals.
#
################################################################################

# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 

conf = ConfigParser.config(
  **ConfigParser.dmdgp_opts(ll),
  **ConfigParser.find("max-evals",ll,Int32))





# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error

dist_fun = ->(a : Indv, b : Indv) do 
  Distance.edit2_normalized(a.sexp,b.sexp,conf["edit2-depth-wgt"])
end 
# Minimum distance required for fitness penalization as a linear decrement

evolution = GP::Evolution(Indv).new do |evo|

# function of stopping criteria. 
  distance_decrement_fun = ->() do 
    conf["dmdgp-req-dist"] - ((conf["dmdgp-req-dist"] / conf["max-evals"])*evo.num_evals)
  end 

  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo) 
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.tournament(conf["off-size"],conf["tournament-size"])
  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])

  evo.replacement = Replacement.new("rmddc",
    ->(pop : Array(Indv), off : Array(Indv)) do 

      # Calculate the new distance requirement
      req_dist = distance_decrement_fun.call
   
      # The candidates for the new population as a union of current population 
      # and offspring 
      candidates = (pop + off).uniq

      # Find the best individual for elitism and remove from population 
      best = Selection.best_indv(candidates)
      index = candidates.index(best).not_nil!
      candidates.swap(index,-1).pop

      # New population 
      new_pop = [] of Indv 
      new_pop.push(best)

      # Calculate the distance to the best individual
      dcn = Distance.dcn(candidates,new_pop,dist_fun,conf["epsilon"])

      penalized = [] of Tuple(Indv,Float64)
      non_penalized = [] of Indv 

      # Select individuals until the population size is reached or there are no 
      # more candidates
      while new_pop.size < conf["pop-size"]&& candidates.size > 0
        # Update distance to nearest neighbor to new pop 
        candidates.each_with_index do |indv,i|
          d = dist_fun.call(indv,new_pop[-1])
          dcn[i] = d if (d - dcn[i]) < -conf["epsilon"]
        end 

        # Classify individuals 
        penalized.clear
        non_penalized.clear 
        candidates.each_with_index do |indv,index|
          if ((dcn[index] - req_dist) > -conf["epsilon"])
            non_penalized.push(indv)
          else 
            penalized.push({indv,dcn[index]})
          end 
        end

        unless non_penalized.empty? 
          # Generate the (fitness, size) tuple for Pareto dominance. 
          objectives = (0 ... non_penalized.size).map do |i|
            {non_penalized[i],{non_penalized[i].fit,candidates[i].sexp.size}}
          end 
          non_dom = MultObj.nondom(objectives,conf["epsilon"]) do |_,_| {false,false} end  
          selected = non_dom.sample 
        else 
          # Select the individual with the greatest diversity 
          selected = (penalized.max_by do |(indv,d)| d end).first
        end 
       
        # delete the selected object 
        index = candidates.index(selected).not_nil!
        candidates.swap(index,-1).pop
        dcn.swap(index,-1).pop

        # add the selected indv to the new population 
        new_pop.push(selected)
      end 

      # Return the new population 
      new_pop
    end) 

  # ----------------------------------------------------------------------------
  Evolution.logs 
end 

best_sol = evolution.evolve



