require "../gp.cr"

###############################################################################
# STDGP :: Standard (Vanilla) Genetic Programming 
#
# This is the basic template for the genetic programming, the classical 
# genetic operators are performed, and the classic ramped half and half 
# initial population generation method is used. The parent selection is 
# a tournament and replacement is a generational scheme with elitism 
# 
# All methods are derived from this basic template, they extend procedures 
# (closures). 
#
###############################################################################



# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32)) 


# Initialization -----------------------------------------------
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

log = GP::Logger.new
obj_fun = GP::ObjectiveFunction.mean_squared_error

semantic_log = [] of Array(String)

evolution = GP::Evolution(Indv).new do |evo|
  if conf["log-config"] 
    evo.on_enter = Proc(Void).new do 
      log_array = conf.to_a.map do |key,val| 
        case val 
        when Iterable 
          keyarr = [key] of (typeof(key)|typeof(val.first))
          val.each do |v| keyarr.push(v) end 
          keyarr
        else {key.to_s,val.to_s} 
        end
      end
      .reject do |e| 
        es = e[0].to_s 
        test = (!conf["log-traindata"] && es =~ /^.train$/ ) 
        test = test || (!conf["log-valdata"] && es =~ /^.val$/) 
      end 
      log.puts(log_array)
    end 
  end  
 
  evo.indv_eval   = Proc(Indv,Void).new do |i| 
    i.evaluate(
    conf["xtrain"],
    conf["ytrain"],
    conf["vars"] ,
    obj_fun,
    conf["infinity"])
  end 

  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh(
    conf["pop-size"],
    conf["max-depth"],
    env)

  evo.selection   = Selection.tournament(
    conf["off-size"],
    conf["tournament-size"])

  evo.crossover   = Crossover.subtree_exchange(
    conf["cx-prob"],
    conf["max-size"],
    env
  )
  evo.mutation    = Mutation.subtree_replacement(
    conf["mut-prob"],
    conf["rand-tree-min-depth"],
    conf["rand-tree-max-depth"],
    conf["max-size"])

  evo.replacement = Replacement.generational_elitist
 
 
  # Comparamos los hijos con los padres 
  evo.after_crossover = Proc(Void).new do 
    semantic_log = [] of Array(String)

    avg_hamming = (0 ... evo.off.size)
      .each
      .map do |i|
        sa = evo.parents[i][0].binarize(conf["ytrain"],conf["epsilon"])
        sb = evo.parents[i][1].binarize(conf["ytrain"],conf["epsilon"])
        evo.indv_eval.call(evo.off[i])
        sc = evo.off[i].binarize(conf["ytrain"],conf["epsilon"])
        ha = Distance.hamming(sa,sc)
        hb = Distance.hamming(sb,sc)
        (ha + hb).to_f / 2.0
      end
      .reduce(0.0) do |a,h| a + h end  / evo.off.size 
 
    avg_euc = (0 ... evo.off.size)
      .each
      .map do |i|
        ea = Distance.euclidean(evo.parents[i][0].semantic,evo.off[i].semantic)
        eb = Distance.euclidean(evo.parents[i][1].semantic,evo.off[i].semantic)
        (ea + eb) / 2.0
      end
      .reduce(0.0) do |a,e| a + e end / evo.off.size 

    avg_edit2 = (0 ... evo.off.size)
      .each 
      .map do |i|
        ea = Distance.edit2_normalized(evo.parents[i][0].sexp,evo.off[i].sexp,0.5)
        eb = Distance.edit2_normalized(evo.parents[i][1].sexp,evo.off[i].sexp,0.5)
        (ea + eb) / 2.0
      end 
      .reduce(0.0) do |a,e| a + e end / evo.off.size 
    
    avg_fit = (0 ... evo.off.size).each
      .map do |i| 
        fa = (evo.parents[i][0].fit - evo.off[i].fit).abs
        fb = (evo.parents[i][1].fit - evo.off[i].fit).abs
        (fa + fb) / 2.0 
      end 
      .reduce(0.0) do |a,e| a + e end / evo.off.size 

    # Diversidad de la población en fitness
    pop_avg_fitness_pair_dist =  (0 ... evo.pop.size).each
      .map do |i| 
        (0 ... evo.pop.size).each.map do |j|
          (evo.pop[i].fit - evo.pop[j].fit).abs 
        end.reduce(0.0) do |a,e| a + e end / (evo.pop.size - 1)
      end 
      .reduce(0.0) do |a,e| a + e end / evo.off.size
    
    # Diversidad de la población en distancia 
    pop_avg_edit_dist = (0 ... evo.pop.size)
      .each.map do |i|
        (0 ... evo.pop.size).each.map do |j|
          Distance.edit2_normalized(evo.pop[i].sexp,evo.pop[j].sexp,0.5)
        end.reduce(0.0) do |a,e| a + e end / (evo.pop.size-1) 
      end 
      .reduce(0.0) do |a,e| a + e end / evo.off.size 
  
    # Diversidad en semántica 
    pop_avg_euc_dist = (0 ... evo.pop.size)
      .each.map do |i|
        (0 ... evo.pop.size).each.map do |j|
          Distance.euclidean(evo.pop[i].semantic,evo.pop[j].semantic)
        end.reduce(0.0) do |a,e| a + e end / (evo.pop.size-1) 
      end 
      .reduce(0.0) do |a,e| a + e end / evo.off.size 
  

    semantic_log += [["pop-avg-euc-dist",pop_avg_euc_dist]]
    semantic_log += [["pop-avg-pair-dist",pop_avg_fitness_pair_dist]]
    semantic_log += [["pop-avg-edit-dist",pop_avg_edit_dist]]
    semantic_log += [["avg-edit2",avg_edit2.to_s]]
    semantic_log += [["avg-fit",avg_fit.to_s]]
    semantic_log += [["avg-euclidean",avg_euc.to_s]]
    semantic_log += [["avg-hamming",avg_hamming.to_s]]

    # semantic_log = semantic_log + [["parents"] + evo.parents]
    # semantic_log = semantic_log + [["offspring"] +evo.off]
  end 

  # Al inicio de cada iteración 
  evo.on_new_gen = Proc(Void).new do 
    if conf["log-evo"] 
      lt = {
        {"gen",evo.gen},
        {"time",evo.timer.toc}, 
        {"evals",evo.num_evals},
        {"best-size",evo.best.sexp.size},
        {"best-fit","%0.4f" % ((evo.best.fit*1e4).round(3)/1e4)},}
      lt = lt + { ["crossover-dynamics"]+semantic_log }
      if conf["log-pop"] 
        lt = lt + {["pop"] + (evo.pop.sort_by do |e| e.fit end)}
      end 
      log.puts(lt) 
    end 
  end

  # Imprimir resultado
  evo.on_return = Proc(Void).new do 
    best_sol = evo.best.clone 
    best_log = { {"train-fit",best_sol.fit} }
    unless conf["xval"].empty?
      best_sol.evaluate(conf["xval"], conf["yval"],
        conf["vars"], obj_fun, conf["infinity"])
      best_log = best_log + ({ {"val-fit", best_sol.fit} })
    end 
    best_log = best_log + { {"size",best_sol.sexp.size} }
    best_log = best_log + { {"best-sol", best_sol.to_s} }
    log.puts(best_log)
  end 
end 
evolution.evolve 



