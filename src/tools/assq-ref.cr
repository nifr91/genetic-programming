require "../gp.cr" 


ref = ARGV.dup 
values = Array(Array(String)).new 

# Read from stdin
while line = gets 
  next if line =~ /^#.*$/ 
  next if line =~ /configuration/ 
  next unless line =~ /#{ref.join("|")}/

  ll = LogList.new(line) 
 
  ary = [] of String
  ref.each do |ref|
    val = ll.assq_ref(ref)
    ary.push(val.val.join(",")) 
  end
  puts ary.join(" ")
end 
