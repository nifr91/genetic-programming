require "./crisp/crisp.cr"

require "./individual.cr" 
require "./timer.cr"
require "./crossovers/crossover.cr" 
require "./stop-criteria/stop-criteria.cr"
require "./init-methods/init-method.cr" 
require "./selections/selection.cr"
require "./mutations/mutation.cr" 
require "./replacements/replacement.cr" 
require "./stop-criteria/stop-criteria.cr" 
require "./logger/logger.cr" 
require "./parser/parser.cr" 
require "./objective-functions/objective-function.cr" 
require "./distances/distance.cr"
require "./evolution/evolution.cr"
require "./mult-obj.cr" 

require "./evolution/evolution-log-macro.cr"
include Crisp
include GP
