class GP::Replacement
  # The generational elitist is the simplest replacement method, the best 
  # solution in the current population 'pop' is preserved into the new 
  # population. The new population is the offspring 'off' with the worst 
  # element replaced with the best from 'pop'.
  #
  def self.generational_elitist
    proc = ->(off : Array(Indv), pop : Array(Indv)) do 
      new_pop = [] of Indv
      pbest = Selection.best_indv(pop)
      worst_fit = pbest.fit
      worst_index = nil
      (0 ... off.size).each do |i|
        if worst_fit < off[i].fit 
          worst_index = i
          worst_fit = off[i].fit 
        end
      end 
      if worst_index
        new_pop.push(pbest)
      end
      off.each_with_index do |indv,i| 
        new_pop.push(indv) unless worst_index && (i == worst_index)
      end 
      new_pop
    end 
    return self.new("generational-elitist",proc) 
  end 
end 
