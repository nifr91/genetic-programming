require "../individual.cr" 

# Object that represents the replacement genetic operator 
# 
class GP::Replacement
  @proc : Proc(Array(Indv),Array(Indv),Array(Indv))
  @name : String
  def initialize(
    @name = "random",
    @proc = ->(p : Array(Indv), o : Array(Indv)){o.map{ [o.sample,p.sample].sample}})
  end 

  def call(*args) 
    @proc.call(*args) 
  end 
  def to_s(io)
    io << @name
  end
end   

require "./generational-elitist.cr"
