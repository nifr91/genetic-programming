require "../individual.cr"

# Object representing a mutation genetic operator 
#
class GP::Mutation
 @proc : Proc(Array(Indv),Array(Indv),Array(Indv))
 @name : String
 def initialize(
   @name = "no-op",
   @proc = ->(off : Array(Indv),pop : Array(Indv)){off})
 end 
 def call(*args) 
   @proc.call(*args) 
 end 
 def to_s(io)
   io << @name
 end

end 

require "./subtree-replacement.cr"

