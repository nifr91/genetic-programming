class GP::Mutation
  def self.subtree_replacement(
    mut_prob,
    rand_tree_min_depth,
    rand_tree_max_depth,
    max_size)
    proc = ->(off : Array(Indv), pop : Array(Indv)) do 
      off.map do |indv|
        # If the probability indicates a mutation, otherwise the 
        # individual is copied
        if rand > mut_prob 
          indv.clone 
        else 
          nsexp = Mutation.subtree_replacement(indv.sexp.clone, ->(){InitMethod.grow(rand_tree_min_depth,rand_tree_max_depth,indv.env)})
          if nsexp.size >= max_size 
            indv.clone 
          else 
            nindv = Indv.new(nsexp,indv.env.clone)
            nindv.age = indv.age
            nindv
          end 
        end 
      end 
    end 
    return self.new("subtree-replacement",proc)
  end 

 #----------------------------------------------

 def self.subtree_replacement(sexp,tree_init)

   # choose a random node 
   node = sexp.sample

   # generate the new sub-tree 
   newnode = tree_init.call

   # find the parent of the selected node 
   p = node.parent?

   # replace the node with the new node in parent
   if p && (i = p.index(node)) 
     p.child_sub(i,newnode)
     sexp
   else 
     newnode
   end 
 end 
end 
