class GP::StopCriteria 
  @proc : Proc(Bool) 
  @name : String 

  # Constructor 
  # 
  # ```
  # name : string         -- name of the criteria 
  # proc : (void) -> bool -- function that returns true when the criteria is met
  # ```
  def initialize(
    @name = "never",
    @proc = ->(){STDERR.puts("WARNING StopCriteria is never");true})
  end 
  
  # Evaluates the criteria function 
  def call(*args) @proc.call(*args) end 
  
  # Returns the name 
  def to_s(io) io << @name end 

  # Merges two stop criteria objects with the and operator, meaning both
  # operators must be true for the calling function to stop. 
  # 
  # ```
  # a : StopCrit -- first stopping criteria 
  # b : StopCrit -- second stopping criteria 
  #
  # stop-criteria -- a new stopping criteria that requires a && b to be true
  # ```
  #
  def self.and (a : StopCriteria, b : StopCriteria)
    name = "#{a}--and--#{b}" 
    proc = ->(){ a.call() && b.call() }  
    StopCriteria.new(name,proc)
  end 

  # Merges two stop criteria objects with the and operator, meaning both
  # operators must be true for the calling function to stop. 
  #
  # ```
  # a : StopCrit -- first stopping criteria 
  # b : StopCrit -- second stopping criteria 
  #
  # stop-criteria -- a new stopping criteria that requires a || b to be true
  # ```
  #
  def self.or (a : StopCriteria, b : StopCriteria)
    name = "#{a}-or-#{b}"
    proc = ->(){a.call() || b.call()}
    StopCriteria.new(name,proc)
  end 

  # Stop criteria by elapsed generations it is true when the number of 
  # generation exceeds the specified max value number.
  #
  # ```
  # max   : Int    -- max value 
  # state : Obj    -- object with property #gen 
  #
  # stop-criteria -- a callable object that returns true if the criteria is met
  # ```
  #
  def self.by_gens(max_gens,state) 
    self.new("by-gens",->(){state.gen >= max_gens}) 
  end 

  # Stop criteria by number of objective function evaluations it is true 
  # when the number of times the objective function is called exceeds the specified 
  # max value number.
  #
  # ```
  # max   : Int    -- max value 
  # state : Obj    -- object with property #num_evals 
  #
  # stop-criteria -- a callable object that returns true if the criteria is met
  # ```
  #
  def self.by_evals(max_evals,state) 
    self.new("by-evals",->(){state.num_evals >= max_evals})
  end 

  # Stop criteria by elapsed time it is true when the time exceeds the 
  # specified max value 
  #
  # ```
  # max   : Int    -- max value 
  # state : Obj    -- object with property #toc
  #
  # stop-criteria -- a callable object that returns true if the criteria is met
  # ```
  #
  def self.by_time(max_time,timer)
   self.new("by-time",->(){timer.toc >= max_time})
  end
end 

