require "../crisp/crisp.cr" 
require "../objective_function.cr" 
require "../init-methods.cr" 
require "../selection.cr"
require "../crossovers/crossover.cr" 
require "../mutation.cr" 
require "../replacement.cr" 
require "./configuration.cr" 
require "../stop-criteria.cr" 
require "option_parser" 


module GP::Configuration::DataLoadConfig 
  property xtrain : Array(Array(Float64)) = [] of Array(Float64)
  property ytrain : Array(Float64)        = [] of Float64
  property xval   : Array(Array(Float64)) = [] of Array(Float64)
  property yval   : Array(Float64)        = [] of Float64
  property dataset_name  : String  = "-"

  def config_parser_dataload(parser) 
    parser.on("-x FILE","--xtrain=FILE","input file (xtrain)") do |file|
      @dataset_name = File.basename(file,File.extname(file))
      @xtrain = BaseConfig.load_xdata(file)
    end 
    parser.on("-y FILE","--ytrain=FILE","expected output (ytrain)") do |file|
      @ytrain = BaseConfig.load_ydata(file)
    end 
    parser.on("--xval=FILE","input file (xvalidation)") do |file|
      @xval = BaseConfig.load_xdata(file)
    end 
    parser.on("--yval=FILE","expected output (yvalidation)") do |file|
      @yval = BaseConfig.load_ydata(file)
    end 
  end 
end 

module GP::Configuration::RunInfoConfig 
  property run_name      : String  = "crips"
  property date          : String  = Time.local.to_s

  def config_parse_runinfo(parser) 
    parser.on("--run-name=STR","name of the run") do |str|
      @run_name = str
    end 
  end 
end 


module GP::Configuration::EnvironmentConfig 
  property env                  : Crisp::Env            = Crisp::Env.new
  property funs                 : Array(String)         = ["+","-"]
  property terms                : Array(String)         = ["int"]
  property vars                 : Array(String)         = ["x"]

  def config_parser_environment(parser)   
    parser.on("--vars=STR1,STR2,..","comma separated list of variable names") do |str|
      @vars = str.split(",")
    end 
    parser.on("--terms=STR1,STR2,..","comma separated list of terminals") do |str|
      @terms = str.split(",")
    end 
    parser.on("--funs=STR1,STR2,..","comma separated list of functions") do |str|
      @funs = str.split(",")
    end 
    parser.parse 
  end
end 

module GP::Configuration::PopulationConfig 
  
end 




class GP::BaseConfig  < GP::Configuration
  include DataLoadConfig
  include RunInfoConfig
  include EnvironmentConfig

  property cx_prob              : Float64               = 1.0 
  property mut_prob             : Float64               = 1.0 
  property pop_size             : Int32                 = 10
  property off_size             : Int32                 = 10
  property max_gens             : Int32                 = 10
  property max_time             : Float64               = 1.0
  property max_size             : Int32                 = 1024
  property max_evals            : Int32                 = 100
  property rand_tree_min_depth  : Int32                 = 1
  property rand_tree_max_depth  : Int32                 = 10
  property tournament_size      : Int32                 = 3
  property epsilon              : Float64               = 1e-12
  property infinity             : Float64               = 1e100
  property obj_fun              : ObjFun                = ObjFun.new
  property pop_init             : Init                  = Init.new
  property selection            : Select                = Select.new
  property crossover            : Crossover             = Crossover.new
  property mutation             : Mut                   = Mut.new
  property replacement          : Replace               = Replace.new
  property stop_criteria        : StopCrit              = StopCrit.new

  def parse()
    parser = OptionParser.new 
    parser.banner = "Usage: ./gp [arguments]"

    config_parser_dataload(parser) 
    config_parse_runinfo(parser)  
    config_parser_environment(parser) 

    parser.on("--pop-size=INT","size of the population") do |num|
      @pop_size = num.to_i32
    end 
    parser.on("--off-size=INT","size of the offspring") do |num|
      @off_size = num.to_i32
    end 
    parser.on("--max-time=FLOAT","max duration of the optimization [s]") do |num|
      @max_time = num.to_f64
    end 
    parser.on("--max-gens=INT", "max number of generations") do |num|
      @max_gens = num.to_i32
    end 
    parser.on("--cx-prob=FLOAT","crossover probability") do |num|
      @cx_prob = num.to_f64
    end 
    parser.on("--mut-prob=FLOAT","mutation probability") do |num|
      @mut_prob = num.to_f64
    end 
    parser.on("--tourn-size=INT","tournament_size") do |num|
      @tournament_size= num.to_i32
    end 
    parser.on("--max-size=INT","maximum size of a solution") do |num|
      @max_size = num.to_i32
    end 

    parser.on(
      "--epsilon=FLOAT", 
      "epsilon for two numbers to be considered equal") do |num|
      @epsilon = num.to_f64
    end 
    parser.on(
      "--infinity=FLOAT",
      "a very bad value used in penalizations") do |num|
      @infinity = num.to_f64 
    end 

    parser.on("--max-evals=INT",
      "Max number of objective function evaluations") do |num|
      @max_evals = num.to_i32
    end 

    parser.on("--rand-tree-max-depth=INT",
        "random tree max depth") do |num|
      @rand_tree_max_depth = num.to_i32
    end 

    parser.on("--rand-tree-min-depth=INT", 
              "random tree min depth") do |num|
      @rand_tree_min_depth = num.to_i32 
    end 


    parser.on("-h","--help","shows this message") do 
      STDERR.puts parser 
      exit(0)
    end 

    parser.invalid_option do |flag|
      STDERR.puts "Error: #{flag} is not a valid option."
      STDERR.puts parser
      exit(1)
    end 

    parser.parse
    # Data validation 
    if @xtrain.empty? || @ytrain.empty? 
      STDERR.puts "ERROR: the xtrain and ytrain data are required"
      STDERR.puts parser
      exit(1)
    end 

    if (@xval.size != @yval.size) || (@xtrain.size != @ytrain.size)
      STDERR.puts "ERROR: the x and y arrays dont have same dimentions"
      STDERR.puts parser
      exit(1)
    end 


    # Check if the names of the variables are equal to the number of variables,
    # if less autocompletion with letters is used else extra letters are 
    # ignored
    @vars = @vars[0 ... @xtrain.first.size]
    cnt = 0
    while @vars.size < @xtrain.first.size
      @vars.push("x#{cnt}")
      cnt+=1
    end 
    self.update_env 

    return self
  end 

    # Loads the input data from the specified file 
    #
    # The expected format is: each line is a input data sample, and on 
    # each linea every column (space separated) is a variable. 
    #
    # | x00 x01 x02 x03 ... 
    # | x10 x11 x12 x13 ... 
    # | ... 
    #
    # fname : string -- name of the file containing the input data
    # 
    # [[float]] -- vector with data entries 
    #
    def self.load_xdata(fname)
      return xs = File
        .read_lines(fname)
        .map{|l| l
          .split(/\s+/)
          .map{|e| e.to_f64}}
    end 

    # Loads the expected output data from the specified file 
    #
    # The expected format is: each line is a output data sample
    #
    # | y0 
    # | y1 
    # | ... 
    #
    # fname : string -- name of the file containing the input data
    # 
    # [[float]] -- vector with data entries 
    #
    def self.load_ydata(fname)
      return self.load_xdata(fname).map(&.first)
    end 

    def update_env
      @env = Env.new 
      @funs.each{|f| env.register_fun f } 
      @terms.each{|t| env.register_term t}
      @vars.each{|v| env.register_input v}
    end 

end 

module GP::LogConfig 
  property logEvo : Bool = true 
  def parse 
    self
  end 

  def self.test 
    super
    puts "logconfig" 
  end 
end 

module GP::DMDGPConfig 
  property logEvo : Bool = true 
  def parse 
    self
  end 

  def self.test 
    super
    puts "dmdgpconfig" 
  end 
end 


class GP::ExtendedConfig < GP::Configuration 
  include GP::LogConfig
  include GP::DMDGPConfig
end 


conf = GP::ExtendedConfig.new
pp! conf.vars 
conf.parse
pp! conf
pp! conf.is_a?(GP::Configuration)
