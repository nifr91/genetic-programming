module GP::MultObj

  # Pareto dominance, a dominates b if for all variables a is better or equal 
  # than b and in at least one it is strictly better. 
  #
  # a -<= b if a_i <= b_i forall i in {1..n} and exists i : a_i < b_i 
  # 
  # int -- -1 si a -< b , 0 si a == b , 1 si a !-< b
  #
  def self.adomb(a,b,eps)
    eq  = true
    (0 ... a.size).each do |i| 
      d = a[i] - b[i]
      return 1 if d > eps 
      eq = eq && (d.abs <= eps)
    end
    return (eq ? 0 : -1)
  end 

  # Calculate the non dominate set 
  def self.nondom(candidates,eps) 
    ndom,_ = MultObj.nondomanddom(candidates,eps){|a,b| yield a,b}
    return ndom.map(&.first)
  end 

  # Calculate the dominated and non dominated sets 
  def self.nondomanddom(candidates, eps)
    ndom = candidates.dup.clear 
    dom  = ndom.dup
    
    (0 ... candidates.size).each do |i| 
      ia,oa = candidates[i]
      isdom = false
      (0 ... ndom.size).reverse_each do |j|
        break if isdom
        ib,ob = ndom[j]
        adom = MultObj.adomb(oa,ob,eps)
        bdom = MultObj.adomb(ob,oa,eps)
        if adom == 0
          adom,bdom = yield ia,ib
        else 
          adom = adom < 0 
          bdom = bdom < 0
        end 

        dom.push(ndom.swap(j,-1).pop()) if adom 
        isdom = true if bdom 
      end 

      if isdom 
        dom.push(candidates[i])
      else 
        ndom.push(candidates[i])
      end
    end 
    return ndom,dom 
  end 

  
end 
