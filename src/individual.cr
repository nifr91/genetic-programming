require "./crisp/crisp.cr" 


  
  # A genetic programming individual 
class GP::Indv
  class Node 
    property sexp : Crisp::Sexp 
    property semantic : Array(Float64) 

    def initialize(@sexp) 
      @semantic = [] of Float64
    end 

    def clone 
      indv = typeof(self).new(@sexp.clone) 
      indv.semantic = @semantic.clone 
      return indv 
    end 
  end 


    property nodes : Array(Indv::Node) 
    # S-expresion 
    property sexp : Crisp::Sexp
    # Fitness 
    property fit  : Float64
    # Environment 
    property env  : Crisp::Env
    # Age of the genetic code 
    property age : Int32

    def initialize(@sexp : Sexp, @env : Env)
      @fit   = 1/0.0
      @age   = 0
      @nodes = [] of GP::Indv::Node 
    end

    def self.new(sexp : Sexp, env : Env) : Indv
      indv = Indv.new(sexp,env)
      yield indv 
      indv 
    end 

    def self.new()
      indv = Indv.new(Sexp.new(Num.new(0)),Env.new)
      yield indv  
      indv
    end 

    def size
      return @sexp.size
    end 

    def to_s 
      String.build do |str| self.to_s(str) end 
    end 
    def to_s(io)
      io << sexp
    end 

    # deep copy 
    def clone 
      indv = Indv.new(@sexp.clone,@env)
      indv.age = @age
      indv.fit = @fit 
      indv.nodes = @nodes.clone 
      return indv 
    end 

    # Hash the indiviual 
    def hash(hasher)
      @sexp.hash(hasher)
      hasher
    end 
    # Two solutions are equal if their s-expressions are equal 
    def == (other : Indv)
      return (@sexp == other.sexp)
    end
  

    # Evaluation of a s
    def evaluate_sexp(si,sexp = @sexp,index = 0,env = @env) 
      # @nodes[index].sexp = sexp 
      val = case ast = sexp.val
      when Symb
        case var = env[ast.to_s]
        when Func
          args = Array(Sexp).new
          acc = 1
          sexp.childs.each do |c|
            args.push(self.evaluate_sexp(si,sexp: c,index: index + acc,env: env))
            acc += c.num_subnodes + 1
          end
           var.call(args) 
        else 
          var
        end 
      else 
        ast 
      end
      # @nodes[index].semantic[si] = val.as(Num).to_f
      return Sexp.new(val) 
    end 


    def predict(xs,vars,infty) 
      @nodes.clear 
      # (0 ... @sexp.size).each do
      #   node = GP::Indv::Node.new(@sexp) 
      #   node.semantic = Array.new(xs.size,0.0) 
      #   @nodes.push(node)
      # end 
      @nodes.push(GP::Indv::Node.new(@sexp))
      @nodes[0].semantic = Array.new(xs.size,0.0)
      
      # For each case 
      xs.each_with_index do |x,si| 
        # Asign the value of the variables 
        (0...x.size).each do |i| env[vars[i]] = Num.new(x[i]) end
        @nodes[0].semantic[si] = self.evaluate_sexp(si).val.as(Num).to_f
      end 
      
      @nodes.each do |node|
        (0 ... node.semantic.size).each do |i| 
          val = node.semantic[i]
          node.semantic[i] = (val.finite? && !val.nan? && val.abs < infty)? val : infty
        end 
      end 
    end 

    # Evaluates the s-expression with the 'xs' input data
    #
    # xs    : [[float]] -- array with the input data
    # sexp  : sexp      -- program to evaluate 
    # env   : env       -- environment of the program to evaluate
    # vars  : [string]  -- names of the variables
    #
    # [float] -- returns an array with the result
    #
    def evaluate(xs ,ys,vars,obj_fun,infty)
      self.predict(xs,vars,infty)
      @fit = obj_fun.call(self.semantic,ys)
      @fit = infty unless @fit.finite? && !@fit.nan? && @fit.abs < infty
    end 

    # Return the semantics 
    def semantic
      @nodes[0].semantic 
    end 

    # Define if two semantics are equal with a threshold 
    def same_semantic_as(other : Indv,th)
      return false if self.semantic.size != other.semantic.size
      self.semantic.size.times do |i|
        return false unless (self.semantic[i] - other.semantic[i]).abs < th
      end 
      return true
    end 

    # Compare fitness and size to determine if the solution is better than the other 
    def better_than?(other : Indv)
      if @fit == other.fit 
        @sexp.size < other.sexp.size 
      else 
        @fit < other.fit 
      end 
    end

    # Binarize, its binary string is a function of 
    # its test cases, if the jth testcase is less than the average of all 
    # test cases of the individual 1 otherwise 0.
    def binarize(ys,th)
      error = Array.new(ys.size) do |i| (self.semantic[i] - ys[i]).abs end 
      eavg  = (error.reduce(0.0) do |acc,err| acc + err end) / error.size 
      return error.map do |e| (e - eavg).abs < th ? 1 : 0 end 
    end 
end

