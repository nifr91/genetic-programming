# The module Crisp is a basic implementation of a lisp like language for gp
module Crisp
end 

require "./*"

# =============================================================================
# Author  : Ricardo Nieto Fuentes
# EMAIL   : nifr91@gmail.com
# LICENSE : MIT
# =============================================================================
