module Crisp 

  # This is the parser for crisp
  class Parser
    # Constructor 
    def initialize(@tokens : Array(String))end 

    # Read a sequence (like a list) 
    def read_sequence(open,close,pos) : Tuple(Sexp,Int32)
      token = @tokens[pos]
      raise  "expected '#{open}', got #{token}" unless token == open

      pos += 1
      sexptoken = @tokens[pos]
      sexp = Sexp.new(Symb.new(sexptoken,0))
      pos += 1

      loop do
        token = @tokens[pos]
        break if token == close

        child,pos = read_form(pos)
        sexp.push child
      end
      sexp.val = Symb.new(sexptoken,sexp.childs.size)
      pos+=1

      {sexp,pos}
    end 

   
    # Reads a structure 
    def read_form(pos) : Tuple(Sexp,Int32)
      while @tokens[pos] =~ /^;.*/
        pos += 1
      end 
      token = @tokens[pos]

      case token
      when "(" then read_sequence("(",")",pos)
      when ")" then raise "unexpected ')'"
      else read_atom(pos)
      end 
    end 

    # Reads an atom 
    def read_atom(pos) 
      token = @tokens[pos]
      raise "expected atom but got EOF" unless token 

      val = case 
      when token =~/^(\+|-)?\d+$/ then Num.new(token.to_i64)
      when token =~/^(\+|-)?\d*\.?\d+([eE](-|\+)?\d+)?$/ then Num.new(token.to_f64)
      else Symb.new(token,0)
      end 
      {Sexp.new(val),pos+1}
    end 
  end 


  # Split in tokens the string 
  def self.tokenize(str : String) 
    regex = /[\s,]*(~@|[\[\]{}()'`~^@]|"(?:\\.|[^\\"])*"?|;.*|[^\s\[\]{}('"`,;)]*)/
    str.scan(regex).map{|m| m[1]}.reject(&.empty?)
  end 

  # Generates the s-expresion represented in a string 
  def self.read_str(str) : Sexp
    tokens = tokenize(str) 
    return Sexp.new(Null.new) if tokens.size == 0
    r = Parser.new(tokens)
    r.read_form(0).first
  end 

end 

# =============================================================================
# Author  : Ricardo Nieto Fuentes
# EMAIL   : nifr91@gmail.com
# LICENSE : MIT
# =============================================================================
