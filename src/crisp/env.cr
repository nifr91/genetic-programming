module Crisp 
  # An Environment for the crips expressions, internally is implemented as a
  # hash String=>Node. 
  #
  class Env
    property env, fset,tset
    # Environment 
    @env : Hash(String,Node) = {} of String => Node
    # List if functions 
    @fset : Array(Symb) = [] of Symb
    # List of terminals 
    @tset : Array(Node) = [] of Node

    def clone 
      env = Env.new 
      env.env = @env.clone 
      env.fset = @fset.clone
      env.tset = @tset.clone
      return env 
    end 

    def register_fun(sym : String | Symbol) 
      sym = sym.to_s 
      self.register_fun sym,*FSET[sym]
    end 

    def register_fun(sym : String | Symbol,func : Func,arity : Int32)
      sym = sym.to_s 
      @fset.push(Symb.new(sym,arity))
      @env[sym] = func
    end 

    def register_term(sym : String | Symbol )
      sym = sym.to_s
      self.register_term(sym,TSET[sym])
    end 

    def register_term(sym : String | Symbol, term : Node)
      sym = sym.to_s 
      @tset.push(term)
      @env[sym] = term
    end 

    def register_input(sym : String | Symbol)
      self.register_term(sym,Symb.new(sym.to_s,0))
    end 

    def has_key?(key)
      @env.has_key?(key.to_s) 
    end 

    def []?(key) @env[key.to_s]? end 
    def [](key) @env[key.to_s] end 
    def []=(key,val) @env[key.to_s] = val end 

    FSET = { } of String => Tuple(Func,Int32)
    TSET = { } of String => Node

    TSET["int"] = Ephemeral.new(0,9)
    TSET["float"] = Ephemeral.new(0.0,1.0)


    FSET["+"]   = {Func.new(->plus(Array(Sexp)),2)  , 2}
    FSET["-"]   = {Func.new(->minus(Array(Sexp)),2) , 2}
    FSET["*"]   = {Func.new(->mult(Array(Sexp)),2)  , 2}
    FSET["/"]   = {Func.new(->div(Array(Sexp)),2)   , 2}
    FSET["pow"] = {Func.new(->pow(Array(Sexp)),2)   , 2}

    FSET["exp"] = {Func.new(->exp(Array(Sexp)),1)   , 1}
    FSET["cos"] = {Func.new(->cos(Array(Sexp)),1)   , 1}
    FSET["sin"] = {Func.new(->sin(Array(Sexp)),1)   , 1}
    FSET["ln"]  = {Func.new(->ln(Array(Sexp)),1)    , 1}
    FSET["sqrt"]= {Func.new(->sqrt(Array(Sexp)),1)  , 1} 

    FSET["sq"] = {Func.new(->sq(Array(Sexp)),1), 1}
    FSET["rpcl"] = {Func.new(->reciprocal(Array(Sexp)),1),1}

    TSET["pi"]  = Num.new(Math::PI).as(Node)
    TSET["e"]   = Num.new(Math::E).as(Node)

    def self.reciprocal(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      Num.new(1.0 / ((n.val != 0) ? n.val : 1.0)).as(Node)
    end 

    def self.sq(a : Array(Sexp)) : Node
      n = a[0].val.as(Num)
      (n**Num.new(2.0)).as(Node)
    end 

    def self.minus(a : Array(Sexp)) : Node
      n1 = a[0].val.as(Num)
      n2 = a[1].val.as(Num)
      (n1 - n2).as(Node)
    end 

    def self.mult(a : Array(Sexp)) : Node
      n1 = a[0].val.as(Num) 
      n2 = a[1].val.as(Num)
      (n1 * n2).as(Node)
    end 

    def self.plus(a : Array(Sexp)) : Node
      n1 = a[0].val.as(Num) 
      n2 = a[1].val.as(Num)
      (n1 + n2).as(Node)
    end 

    def self.div(a : Array(Sexp)) : Node
      num = a[0].val.as(Num)
      den = a[1].val.as(Num)
      ((den.val != 0) ? num / den : Num.new(1.0)).as(Node)
    end 

    def self.ln(a : Array(Sexp)) : Node
      n = a[0].val.as(Num)
      ((n.val == 0) ?  Num.new(-200.0) : Num.new(Math.log(n.abs))).as(Node)
    end 

    def self.sqrt(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      if (n.val < 0)
        n.as(Node)
      else 
        (n ** Num.new(0.5)).as(Node) 
      end 
    end

    def self.pow(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      p = a[1].val.as(Num)
      if (p.to_i != p.to_f) && (n.val < 0)
        Num.new(n.val).as(Node)
      else 
        (n**p).as(Node)
      end  
    end

    def self.exp(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      (Num.new(Math.exp(n.val))).as(Node)
    end 

    def self.cos(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      (Num.new(Math.cos(n.val))).as(Node)
    end 

    def self.sin(a : Array(Sexp)) : Node 
      n = a[0].val.as(Num)
      (Num.new(Math.sin(n.val))).as(Node)
    end 
  end 


end 

# =============================================================================
# Author  : Ricardo Nieto Fuentes
# EMAIL   : nifr91@gmail.com
# LICENSE : MIT
# =============================================================================
