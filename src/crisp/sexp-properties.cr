module Crisp
  class Sexp

    # Calculate the genetic marker 
    def self.genetic_marker(sexp,min_depth,max_depth)
      marker = String.build do |io|
        Sexp.rec_genetic_marker(sexp,io,min_depth,max_depth,0)
      end 
      return marker
    end 


    # Auxiliar method for recursively calculating the genetic marker 
    def self.rec_genetic_marker(sexp,io,min_depth,max_depth,depth)
      if depth < min_depth
        sexp.childs.each_with_index do |c,i| 
          rec_genetic_marker(c,io,min_depth,max_depth,depth+1)
          io << " " if i < sexp.childs.size-1
        end 
      elsif min_depth <= depth <=  max_depth
        val = sexp.val
        io << "(" if val.is_a?(Symb) && !sexp.term?
        io << sexp.val
        if depth < max_depth
          sexp.childs.each do |c|
            io << " " 
            rec_genetic_marker(c,io,min_depth,max_depth,depth+1)
          end 
        end 
        io << ")" if val.is_a?(Symb) && !sexp.term? 
      end 
      return io
    end 

  end 
end 
