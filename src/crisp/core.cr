module Crisp

  # This class represents a S-expression node
  #
  class Sexp 
    property  val          : Node
    property! parent       : Sexp | Nil
    getter    childs       : Array(Sexp)
    getter    height       : Int32
    getter    num_subnodes : Int32
    getter    id           : UInt64 = rand(UInt64::MAX)

    # Two s-expressions are equal if all its contents are equal 
    def ==(other : Sexp)
      return false if @val != other.val 
      return false if @num_subnodes != other.num_subnodes
      return false if @height != other.height
      return false if @childs.size != other.childs.size 
      (0 ... @childs.size).each do |i| 
        return false unless @childs[i] == other.childs[i]
      end 
      return true 
    end 

    # The hash value of an s-expression is calculated as the has of it's string
    # representation 
    def hash(hasher)
      hasher = "#{self}".hash(hasher)
      return hasher
    end 

    # The size of an s-expression is the number of nodes 
    def size() @num_subnodes + 1 end 

    # Constructor 
    def initialize(@val : Node,@parent = nil) 
      @childs = []  of Sexp
      @num_subnodes = 0 
      @height = 0
    end 

    # The arity of an s-expression is 0 if a terminal and the number of 
    # arguments if a function
    def arity() 
      case v = @val 
      when Symb then v.arity 
      when Func then v.arity
      else 0
      end 
    end 

    # The s-expression is a leaf node if its arity is 0. 
    def term?() self.arity == 0 end  

    # The evaluation of an s-expression 
    def eval(env) : Sexp
      val = case ast = @val
      when Symb
        raise "symbol #{ast} not defined" unless env.has_key?(ast.to_s)
        case var = env[ast.to_s]
        when Func 
          args = Array(Sexp).new
          @childs.each{|c| args.push(c.eval(env))}
          exp,got = ast.arity,args.size
          raise "expected #{exp} args for #{ast} got #{got}" if exp != got
          var.call(args)
        else
          var
        end 
      else 
        ast
      end
      Sexp.new(val)
    end 
   
    # The s-expression string representation 
    def to_s(io) 
      io << "(" if (@val.is_a?(Symb) && !self.term?)
      io << @val
      self.childs.each{|n| io << " " << n} 
      io << ")" if (@val.is_a?(Symb) && !self.term?)
    end 

    # Deep copy of an s-expression node 
    def clone
      s = Sexp.new(@val,@parent)
      @childs.each do |arg|
        s.push(arg.clone)
      end 
      return s
    end 

    # Update the node information like the number of subnodes 
    def update
      @height = if @childs.size == 0
        0
      else 
       @childs.reduce(0){|acc,c| ((c.height > acc) ? c.height : acc)} + 1
      end 
      @num_subnodes = @childs.size
      @num_subnodes += @childs.reduce(0){|acc,n| acc + n.num_subnodes} 

      self
    end 

    # Recursive updating of the node 
    def update_tree 
      self.update
      @parent.try{|p| p.update_tree}
      self 
    end 


    # Replace a child , it updates the values of the nodes 
    def child_sub(index : Int32, val)
      val.parent = self
      @childs[index] = val
      self.update_tree
      val
    end 

    # Find the index of a child in the childs array 
    def index(obj : Sexp)
      raise "could not find argument #{obj}" unless i = @childs.index(obj)
      i
    end 

    # Gets the node of a index 
    def node_at(index)
      index = index + self.size if index < 0
      unless 0 <= index <= self.size 
        raise "index out of bounds #{index} size is #{self.size}"
      end 
      find_node_by_index(self,index)
    end 

    # Gets a random subnode
    def sample() return self.node_at(rand(self.size)) end 

    # Recursive search for a node by it's index 
    private def find_node_by_index(node,index)
      return node if index == 0 
      curr_pos = 0
      left_nodes = 0
      node.childs.each do |child| 
        curr_pos += child.num_subnodes + 1
        if curr_pos >= index 
          index -= left_nodes + 1
          node = find_node_by_index(child,index)
          break
        end
        left_nodes = curr_pos 
      end 
      return node
    end 

    # Adds a child to the current node, it updates the tree 
    def push(sexp) 
      case sexp 
      when Sexp 
        sexp.parent = self
      else 
        sexp = Sexp.new(sexp,self)
      end 

      @childs.push(sexp)
      self.update_tree
      sexp 
    end 

    # Prints a tree string representation 
    def tree_str(node = self,io =STDOUT, depth = [] of String) 
      io.puts "(#{node.val}, #{node.num_subnodes})"
      depth.push " " 
      len = node.childs.size - 1

      (0 .. len).each do |i|
        depth.each{|c| io.print c} 
        unless i == len 
          io.print "├" 
          depth.push "│"
          tree_str node.childs[i],io, depth
          depth.pop 
        else 
          io.print "└"
          depth.push " " 
          tree_str node.childs[i],io, depth
          depth.pop
        end 
      end 
      depth.pop 
    end 
  
    # Prints the infix representation
    def to_infix
      String.build do |io|
        self.to_infix(self,io)
      end 
    end
    def to_infix(sexp,io)
      oper = sexp.val
      case sexp.childs.size
      when 0 then io << oper
      when 1
        io << oper 
        io << "("
        self.to_infix(sexp.childs[0],io)
        io << ")"
      when 2..3
        io << "("
        (0 ... sexp.childs.size-1).each do |k|
          self.to_infix(sexp.childs[k],io)
          io << " "
          io << oper
          io << " "
        end 
        self.to_infix(sexp.childs[-1],io)
        io << ")"
      end 
    end 

    def subtrees() 
      ary = self.to_a.clone.map! do |n|
        n.parent = nil
        n.update_tree
      end 
      ary.sort_by! do |n| n.size end 

      return ary
    end 
    

    def bfs() 
      queue = [self]
      until queue.empty?
        node = queue.shift
        node.childs.each do |child| queue.push(child) end 
        yield node
      end
      return self
    end


    def dfs() 
      stack = [{self,false}]
      until stack.empty?
        node,visited = stack[-1]
        unless visited
          yield node
          stack[-1] = {node,true} 
          node.childs.reverse_each do |c|
            stack.push({c,false})
          end
        else
          stack.pop
        end
      end 
      return self
    end 

    def to_a() 
      ary = Array(Sexp).new(self.size)
      self.dfs do |n| ary.push(n) end 
      return ary
    end 

    def inspect(io) 
      io << self 
    end 
  end 

  # A Node representation  
  abstract struct Node 
  end

  # Null node 
  struct Null < Node
    getter val 
    @val : Nil = nil
    def == (other : Null) true end
    def == (other : Node) false end 
    def to_s (io) io << @val end
    def clone() self end 
  end 

  # A symbol 
  struct Symb < Node
    getter val 
    getter arity : Int32
    def initialize(@val : String,@arity) end 
    def == (other : Symb) @val == other.val end
    def to_s (io) io << @val end
    def clone() self end 
  end

  # A numeric value 
  struct Num < Node
    include Comparable(Num)
    getter val 
    @val : Float64 | Int64
    def initialize(val : Int | Float | Num)
      case val 
      when Num then @val = val.val 
      when Int then @val = val.to_i64
      else @val = val.to_f64
      end 
    end 

    def *(other  : Num)  Num.new(@val * other.val)  end 
    def **(other : Num)  Num.new(@val ** other.val) end 
    def +(other  : Num)  Num.new(@val + other.val)  end 
    def -(other  : Num)  Num.new(@val - other.val)  end 
    def /(other  : Num)  Num.new(@val / other.val)  end 
    def abs() Num.new(@val.abs) end 
    def <=>(other : Num) (@val <=> other.val) end

    def to_num()  self end 
    def to_f()  @val.to_f end
    def to_i() @val.to_i end
    def to_s(io) @val.to_s(io) end 

    def clone() self end 
  end 

  # A function 
  struct Func < Node
    getter val : Proc(Array(Sexp),Node)
    getter arity : Int32
    def initialize(@val,@arity) end 
    def call(args : Array(Sexp))  @val.call(args) end 
    def to_s(io) io << @val end 
    def clone() self end 
  end 

  # Representation of a ephemeral (new on each generation) terminal 
  struct Ephemeral < Node
    def initialize(l : Int | Float | Num, r : Int | Float | Num)
      @l = Num.new(l)
      @r = Num.new(r)
    end 
    def val
      val = case @l.val 
      when Int then rand(@l.to_i .. @r.to_i)
      else          rand(@l.to_f .. @r.to_f)
      end 
      Num.new(val)
    end 
    def to_s(io) io << @l << ".." << @r end 
    # def clone
      # s = Ephemeral.new(@l,@r)
    # end 
    
    def clone() return self end
  end 

end 

# =============================================================================
# Author  : Ricardo Nieto Fuentes
# EMAIL   : nifr91@gmail.com
# LICENSE : MIT
# =============================================================================
