{% if false %}
require "./crisp.cr"
include Crisp

n1 = Symb.new("+",2)
pp! n1

n2 = Num.new(1.0)
pp! n2

n3 = Num.new(4)
pp! n3

n4 = Symb.new("-",2) 

n5 = Num.new(1.1)
n6 = Num.new(1.3)

n7 = Symb.new("*",2)


pp! s = Sexp.new(n1)
s.push n2 
s.push n3
pp! s.to_s

env = Env.new 
env.register_fun :+
env.register_fun :-
env.register_fun :*
env.register_input "in0"
env["in0"] = Num.new(0)
pp! s.eval(env).to_s

s1 = Sexp.new(n4)
s1.push(n5)
s1.push(n6)
pp! s1.to_s 
pp! s1.eval(env).to_s


s3 = Sexp.new(n7)

s3.push(s1)
s3.push(s)
pp! s3.to_s 
pp! s3.eval(env).to_s 

pp! s3.num_subnodes
pp! s3.size
s3.tree_str

(0...s3.size).each do |i| 
  pp! s3.node_at(i).val.to_s
end 

puts 
puts 

pp! Crisp.read_str("(+ (- 3 1) 2)").to_s
pp! Crisp.read_str("(+ (- 3 1) ;comentario \n2) ; comentario").eval(env).to_s
pp! Crisp.read_str(%((+ (- 1 2.2) ; comentario \n1 ) ;; comentario)).eval(env).to_s

pp! Crisp.read_str(Crisp.read_str("(+ (- 3.1 3) (* 2 2))").to_s).eval(env).to_s 

puts 
puts 
# pp! var = Var.new("in0") 
# pp! var2 = Var.new("in0")
# pp! var3 = Var.new("in1")
pp! sym = Symb.new("rand",0) 
pp! sym2 = Symb.new("rand",0)
pp! sym3 = Symb.new("exp",1)
pp! num = Num.new(1.0)
pp! num2 = Num.new(1)
pp! num3 = Num.new(2)

puts 
puts 
puts 

pp! sym == sym2
pp! sym == sym3
pp! sym == sym 
pp! sym == num 

puts 
pp! num == num2 
pp! num == num3
pp! num == num
pp! num == sym 

n = Sexp.new(Symb.new("+",2))
n.push Sexp.new(Symb.new("in0",0))
n.push Sexp.new(Num.new(1.0))
pp! n.to_s 
n.tree_str

pp! Crisp.read_str("(+ (- 3 1) 2)").height

pp! Crisp.read_str("(+ (/ (* in0 in0) in0) (* in0 (* in0 (+ 1 (+ (* (ln (ln (exp (exp in0)))) in0) in0)))))").height
pp! Crisp.read_str("(/ (/ (cos (* (* (* 4 (/ (/ 4 (* in0 (sin (sin (sin (exp (ln in0))))))) 3)) in0) in0)) 4) (ln (/ (ln (/ 3 (* in0 (sin (sin (ln in0)))))) (* in0 (sin (sin (ln (* in0 (* (cos (ln 6)) (* (cos (ln 6)) in0))))))))))").height
puts Crisp
  .read_str("(/ (/ (cos (* (* (* 4 (/ (/ 4 (* in0 (sin (sin (sin (exp (ln in0))))))) 3)) in0) in0)) 4) (ln (/ (ln (/ 3 (* in0 (sin (sin (ln in0)))))) (* in0 (sin (sin (ln (* in0 (* (cos (ln 6)) (* (cos (ln 6)) in0))))))))))")
  .tree_str
pp! Crisp
  .read_str("(+ (* (cos in1) (sin (* (sin in0) (cos (* (sin (sin in0)) (* (sin (* (cos (* (* (* (sin in0) (cos (* in0 4))) (sin (sin (cos (* (* in0 in0) (* (* in0 (cos (* (* 6 in0) (sin (sin (cos (* (* in0 in0) (* (* in0 (* in0 in0)) (cos (* (cos (* in0 in0)) (cos (* (* (sin in0) (cos (* in0 4))) 4)))))))))))) (cos (* (* 6 in0) (sin (sin (cos (* (* in0 in0) (* (* in0 (cos (* (* 6 in0) (* (sin in0) (cos (* in0 4)))))) (cos (* (cos (* in0 in0)) (* (cos (* (sin (* (cos (* (* (* (cos (* in0 4)) (cos (* (* (* in0 in0) (* (* in0 (ln 7)) in0)) 4))) (sin (* (cos (* (* in0 (cos (/ (* in1 (* in0 4)) in0))) (* (* in0 (cos (/ (* in1 (* in0 4)) in0))) in0))) (cos (* in0 4))))) (cos (* (* in0 4) (exp in0))))) 4)) (sin 1))) (cos (* in0 4)))))))))))))))))) (cos (* in0 in0)))) (sin in0))) (sin in0))))))) (* in0 (cos in1)))")
  .height
puts Crisp
  .read_str("(+ (* (cos in1) (sin (* (sin in0) (cos (* (sin (sin in0)) (* (sin (* (cos (* (* (* (sin in0) (cos (* in0 4))) (sin (sin (cos (* (* in0 in0) (* (* in0 (cos (* (* 6 in0) (sin (sin (cos (* (* in0 in0) (* (* in0 (* in0 in0)) (cos (* (cos (* in0 in0)) (cos (* (* (sin in0) (cos (* in0 4))) 4)))))))))))) (cos (* (* 6 in0) (sin (sin (cos (* (* in0 in0) (* (* in0 (cos (* (* 6 in0) (* (sin in0) (cos (* in0 4)))))) (cos (* (cos (* in0 in0)) (* (cos (* (sin (* (cos (* (* (* (cos (* in0 4)) (cos (* (* (* in0 in0) (* (* in0 (ln 7)) in0)) 4))) (sin (* (cos (* (* in0 (cos (/ (* in1 (* in0 4)) in0))) (* (* in0 (cos (/ (* in1 (* in0 4)) in0))) in0))) (cos (* in0 4))))) (cos (* (* in0 4) (exp in0))))) 4)) (sin 1))) (cos (* in0 4)))))))))))))))))) (cos (* in0 in0)))) (sin in0))) (sin in0))))))) (* in0 (cos in1)))")
  .tree_str
puts Crisp.read_str("1").update_tree.height

puts 
puts
puts Crisp
  .read_str("(+ 1 2)").eval(env)

puts Crisp
  .read_str("(+ 1.0 2.0)").eval(env)

puts Crisp
  .read_str("(+ (+ 1E+10 1e-10) 1.2e11)").eval(env)
{% end %}
