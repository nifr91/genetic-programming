module GP

  # Object for measuring time intervals  
  # 
  # @ti   : time -- initial monotic time 
  # @tf   : time -- final monotic time  
  # @span : span -- time interval between @ti and  @tf 
  #
  class Timer 
    @ti = Time.monotonic
    @tf = Time.monotonic
    @span = Time::Span.new(nanoseconds: 0)
    
    # Starts timer 
    def tic
      @span = Time::Span.new(nanoseconds: 0)
      @ti = Time.monotonic
      @ti.seconds
    end
  
    # Ends timer 
    def toc
      @tf = Time.monotonic    
      @span = (@tf - @ti)
      self.seconds 
    end 
    
    # Elapsed time in seconds 
    def seconds
      @span.total_seconds
    end 
  end 

end 
