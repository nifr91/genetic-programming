class GP::Logger 
  property io : IO
  
  def initialize(@io : IO = STDOUT)
  end 

  def self.new(str : String,mode="w+") 
    log = Logger.new(File.open(str,mode))
  end
  
  def self.string_build 
    String.build do |io| 
      log = Logger.new(io)
      yield log 
    end  
  end 
  
  def comment(str)
    str.each_line do |line|
      @io << ";; " << line.strip << "\n"
    end 
  end 

  def puts(*args) 
    self.print(*args)
    @io.puts 
    return self 
  end 

  def print(*args) 
    @io << "("  if args.size > 1 
    args.each_with_index do |e,i|
      case e
      when Iterable
        @io << "(" 
        e.each_with_index do |ee,ii| 
          self.print(ee) 
          io << " " if ii != e.size-1
        end  
        @io << ")" 
      when String 
        @io << (e =~ /\s/ ?  %("#{e.gsub("\"","\\\"")}") : e)
      else 
        @io << e 
      end 
      io << " " if i != args.size-1
    end 
    @io << ")"  if args.size > 1 
  end 

  def close 
    io.close
  end 
end 
