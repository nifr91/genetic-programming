# TODO arreglar el parser de strings para permitir cadenas de 
# texto 

module GP
  class LogList
    property val
    @val : Array(String|LogList) = Array(String|LogList).new

    def initialize() end 

    def initialize(*args)
      args.each do |v| 
        case v 
        when LogList then @val.push(v)
        else @val.push(v.to_s)
        end 
      end 
    end 
  
    def clone 
      ll = LogList.new 
      ll.val = @val.clone 
      ll 
    end 
  
    def self.new(str : String)
      tokens = str
        .gsub(/;+.*/,"")
        .split(/\s+|(\(|\)|")/,remove_empty: true)
        .reverse
      from_string_array(tokens)
    end

    def ==(other : LogList)
      return false if @val.size != other.val.size 
      @val.size.times do |i|
        return false if @val[i] != other.val[i]
      end 
      return true 
    end 

    def self.equal?(alst,blst)
      return false if typeof(alst) != typeof(blst)
      return (alst == blst) 
    end 

    def self.car?(lst)
      case lst
      when LogList 
        return nil if lst.val.empty? 
        lst.val.first
      else nil
      end 
    end 

    def self.cdr?(lst)
      case lst 
      when LogList
        ll = LogList.new
        ll.val = lst.val[1..]
        ll
      else nil
      end 
    end 
  
    def self.assoc?(key,lst)
      return nil unless lst.is_a?(LogList)
      lst.val.each do |v|
        value = case v
        when LogList then LogList.car?(v)
        else nil 
        end 
        if value && (value == key)
          return v
        end 
      end 
      return nil 
    end 

    def +(other : self) 
      ll = self.clone 
      other.val.each do |v|
        ll.val.push(v)
      end 
      return ll
    end 
    
    def []?(str : String | Symbol) 
      str = str.to_s 
      lst = self.assq_ref(str)
      return nil if lst.empty?
      lst.val.map do |e| e.to_s end.join(" ")
    end

    def empty? 
      @val.empty?
    end 

    def car
      return self if @val.empty?
      return @val.first
    end

    def cdr
      return LogList.new if @val.size < 2
      cdr = LogList.new 
      cdr.val = @val[1..]
      return cdr
    end 

    def assq_ref(key)
      @val.each do |pair|
        break unless pair.is_a?(self) 
        k = pair.car
        if (k == key) 
          return pair.cdr
        end
      end
      return LogList.new 
    end 
  
    def empty?
      return @val.empty?
    end 
  
    def each 
      return @val.each 
    end 

    def each 
      @val.each do |e|
        yield e 
      end 
    end 

    def self.from_string_array_iter(array : Array(String))
      stack = [] of LogList
      ll = LogList.new
      until array.empty? 
        array.pop?
        while !array.empty? && (array[-1] != ")")
          if array[-1] == "\""
            str = String.build do |io|
              io << array.pop?
              while (t = array.pop?) && (t != "\"")
                io << t
              end 
              io << t 
            end 
            ll.push(str)
            next
          end 
          
          if array[-1] == "("
            stack.push(ll)
            ll = LogList.new
            array.pop?
            next
          end 
          v = array.pop?
          ll.push(v)
        end 
        break if stack.empty?
        stack[-1].push(ll)
        ll = stack.pop
      end
      # puts ll
      return ll 
    end 


    def self.from_string_array(array : Array(String),ll = LogList.new)
      raise "error not a list" if (t = array.pop?) && (t != "(")
     
      while !array.empty? && (array[-1] != ")")

        if array[-1] == "\""
          str = String.build do |io|
            io << array.pop?
            while (t = array.pop?) && (t != "\"")
              io << t
            end 
            io << t 
          end 
          ll.push(str)
          next
        end 

        v = if array[-1] == "("
          LogList.from_string_array(array)
        else 
          array.pop?
        end 

        ll.push(v)
      end 

      array.pop?
      return ll
    end 

    def self.new(ary : Array)
      ll = LogList.new 
      ary.each do |i| 
        v = case i 
        when Array then LogList.new(i)
        else i.to_s 
        end 
        ll.push(v)
      end 
      return ll
    end 

    def self.wrapstr(str)
      %("#{str}")
    end 

    def push(*args) 
      args.each do |arg| 
        v = case arg 
        when LogList then arg
        else arg.to_s 
        end 
        @val.push(v)
      end 
    end 

    def to_str(io,lvl)
      io << "("
      @val.each_with_index do |v,i| 
        case v 
        when LogList 
          v.to_str(io,lvl+1) 
        else 
          io << v 
        end 
        io << " " if i < (@val.size-1)
      end 
      io << ")"
    end 

    def to_s(io) to_str(io,0) end 
  end 

end 
