
# This objects represents an objective function
# 
# name : string   -- the name of the objective function 
# proc : function -- the evaluation of the objective function 
#
class GP::ObjectiveFunction
  # The lambda expression of the objective function 
  @proc : Proc(Array(Float64),Array(Float64),Float64)
  # The name of the function 
  @name : String 
  def initialize(
    @name = "constant", 
    @proc = ->(yh : Array(Float64),y : Array(Float64)){1.0})
  end 
  def call(*args) 
    @proc.call(*args) 
  end 
  def to_s(io) 
    io << @name 
  end 


  # Measure of  the average squared difference between the estimated 
  # values and the actual value.
  # Is a measure of the quality of an estimator, values (always greater 
  # than zero) are better as they converge to zero. Incorporates the 
  # variance of the estimator (how spread it is) and its bias. 
  #
  # 1/n (sum_(i=1)^N (y_i - \hat(y)_i)^2)
  #
  # def self.mean_squared_error(yhats,ys)
  #   mse = 0.0 
  #   (0 ... ys.size).each do |i| 
  #     mse += (ys[i] - yhats[i])**2.0 
  #   end 
  #   mse /= ys.size 
  #   return mse 
  # end 
  def self.mean_squared_error()
    proc = ->(yhats : Array(Float64),ys : Array(Float64)) do 
      mse = 0.0 
      (0 ... ys.size).each do |i|
        mse += (yhats[i] - ys[i])**2.0
      end 
      mse /= ys.size 
      mse
    end 
    return self.new("mse",proc) 
  end 
end 
 
