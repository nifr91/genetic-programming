require "../gp.cr"

###############################################################################
# STDGP :: Standard (Vanilla) Genetic Programming 
#
# This is the basic template for the genetic programming, the classical 
# genetic operators are performed, and the classic ramped half and half 
# initial population generation method is used. The parent selection is 
# a tournament and replacement is a generational scheme with elitism 
# 
# All methods are derived from this basic template, they extend procedures 
# (closures). 
#
###############################################################################


# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 

conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32))


# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo|
  
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.tournament(conf["off-size"],conf["tournament-size"])
  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.generational_elitist
  
  Evolution.logs
end 

best_sol = evolution.evolve

