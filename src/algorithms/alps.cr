require "../gp.cr"

###############################################################################
# ALPS :: Age Layered Population Structure 
#
# The core idea is to protect less fit individuals in the population from being
# overshadowed by fitter individuals, To archive this, the population is
# partitioned in layers, each layer has a capacity and age limit. At each
# generation individuals are promoted to the next layer if it's age is grater
# than it's current layer limit. When an individual is promoted, if better than
# the worst individual in the next layer it replaces it, otherwise is
# discarded. The first layer has new individuals (age 0), for other layers the
# age is inherited from the oldest parent, and each iteration the age is
# incremented once, regardless of how many times it was used. Cross over is
# limited to the current and previous layer. 
# And a elitism of size k is considered (the fittest individuals in pop 
# are considered as candidates in the offspring)
#
# Increasing diversity and controlling bloat in linear genetic programming
# Cao B. Jiang Z. 
# 3rd International Conference on Information Science and Control Engineering
# 2016
# 10.1109/ICISCE.2016.97
#
# ALPS: the age-layered population structure for reducing the problem of p remature convergence. 
# Gregory S. Hornby. 
# Proceedings of the 8th annual conference on Genetic and evolutionary computation (GECCO '06) 
# 2006 
# 10.1145/1143997.1144142
#
###############################################################################


# ALPS PARAMETERS
#
# alps-num-layers [int] ;; the number of layers the population is divided 
# alps-age-gap    [int] ;; scaling of scheduling scheme 
# alps-num-elites [int] ;; number of elite individual in pop selected as 
#                       ;; candidates in replacement in each layer



# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 

conf = ConfigParser.config(
  **ConfigParser.alps_opts(ll),
  **ConfigParser.find("max-evals",ll,Int32))




# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error

dist_fun = ->(a : Sexp, b : Sexp) do 
  Distance.edit2_normalized(a,b,conf["edit2-depth-wgt"])
end 
# Minimum distance required for fitness penalization as a linear decrement

evolution = GP::Evolution(Indv).new do |evo|
  # The number of individuals in each layer 
  indvs_per_layer = conf["pop-size"] // conf["alps-num-layers"]

  # Max age in each layer, the last layer has no age limit 
  max_age = (1 .. conf["alps-num-layers"]).each.map{|e| conf["alps-age-gap"]*(e**2)}.to_a 
  max_age[-1] = Int32::MAX - 1

  # Active layer flag > 0 when active 0 just enabled < 0 inactive
  active = max_age.dup 
  (1 ... active.size).reverse_each do |i| active[i] = -active[i-1] end 
  active[0] = 0 


  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)

  evo.selection   = Selection.new("alps-selection",
    ->(pop : Array(Indv)) do 

      # Auxiliar temporal array for a layer 
      sl = Array(Indv).new(indvs_per_layer,pop.first)  

      # Parent selection 
      psel = [] of Array(Indv) 

      # For each layer 
      conf["alps-num-layers"].times do |l|
        # Ignore if the layer is inactive 
        next if active[l] < 0 

        # For each individual in the layer 
        indvs_per_layer.times do |i|
          # Parents selection  
          ps = Array(Indv).new(2)  do 
            # Select a layer from current and previous randomly 
            # When the layer is recently activated we take from prevous layer
            # When the layer is 0 we select from l0. 
            li = rand(l-1..l)
            li = (active[l] == 0) ? l-1 : li 
            li = (li < 0) ? 0 : li; 

            # Get the layer 
            offset = li * indvs_per_layer
            sl.clear 
            (0 ... indvs_per_layer).each do |indx| 
              break if pop[offset+indx].age >= max_age[-1]
              sl.push(pop[offset+indx])
            end 

            # Get the index of the fitness tournament winner 
            wi = Selection.tournament_index(sl,conf["tournament-size"]) do |e| e.fit end 
            wi += offset

            pop[wi]
          end 
           
          psel.push(ps.to_a)
        end 
      end
      # Return selection 
      psel
    end)

  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])

  evo.replacement = Replacement.new("alps-replacement",
    ->(off : Array(Indv), pop : Array(Indv)) do 
      # The new generation 
      new_pop = [ ] of Indv
 
      # Increment the age of a parent used in crossover  
      (0 ... pop.size).each do |i| pop[i].age += 1 end 
 
      # Cache of candidates for promotion 
      promoted_indvs = [] of Indv 

      # Layer arrays 
      off_layer = [] of Indv 
      pop_layer = [] of Indv 

      (0 ... conf["alps-num-layers"]).each do |l| 
        next if active[l] < 0 
        offset = l * indvs_per_layer 

        # Get layers
        off_layer.clear
        pop_layer.clear 
        (offset ... (offset + indvs_per_layer)).each do |i| 
          off_layer.push(off[i])  if i < off.size   
          pop_layer.push(pop[i])  if i < pop.size  
        end 

        # Get layer individuals
        off_layer += promoted_indvs 

        # Select individuals that require promotion 
        promoted_indvs = off_layer.select{|indv| indv.age > max_age[l]} 
        (0 ... pop_layer.size).each do |i| 
          promoted_indvs.push(pop_layer[i]) if pop_layer[i].age > max_age[l]
        end 
        promoted_indvs.reject! do |indv| indv.age >= max_age[-1] end 

        # Select individuals that are elegible for the layer 
        off_layer.reject!{|indv| indv.age > max_age[l]}
      
        # Increment offspring age
        off_layer.each do |indv| indv.age += 1 end 

        # Elitism 
        pop_layer.select! do |indv| indv.age <= max_age[l] end 
        pop_layer.sort_by! do |indv| indv.fit end 

        unless pop_layer.size < conf["alps-num-elites"]
          conf["alps-num-elites"].times do |i| 
            indv = pop_layer[i]
            off_layer.push(indv)
          end 
        end 

        # Sort layer
        off_layer.sort_by! do |indv| indv.fit end 

        # Delete rejected individuals
        while off_layer.size > indvs_per_layer
          off_layer.pop 
        end 
        off_layer.sort_by! do |indv| indv.age end 
 
        # Generational replacement
        (0 ... indvs_per_layer).each do |i| 
          if i < off_layer.size 
            new_pop.push(off_layer[i])
          else
            bad_indv = Indv.new() do |bad_indv| 
              bad_indv.age = max_age[-1]
            end 
            new_pop.push(bad_indv)
          end
        end 
      end 

      # At age-gap iterations reset layer 0 population
      if ((evo.gen % conf["alps-age-gap"]) == 0)
        indvs_per_layer.times do |i|
          indv = Indv.new do |indv|
            indv.age = 0
            indv.sexp = InitMethod.grow(
              min_depth: conf["rand-tree-min-depth"], 
              max_depth: conf["rand-tree-max-depth"],
              env: env)
            indv.env = env.clone 
            evo.indv_eval.call(indv) 
          end  
          new_pop[i] = indv
        end 
      end 

      # Update layers age activation 
      active.map! do |i| i + 1 end 

      new_pop
    end) 


  # --------------------------------------------------------------------------- 
  Evolution.logs 
end 

best_sol = evolution.evolve
