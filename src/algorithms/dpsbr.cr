require "../gp.cr"

##############################################################################
# DPSBR :: Diverse Partner Selection with Brood Recombination 
#
# Diverse partner selection with brood recombination in genetic programming
# Muhammad Waqar
# Applied soft computing
# 10.1016/j.asoc.2018.03.035
#
#
# It promotes useful sematic diversity that can be used by the recombination
# operator. The algorithm selects parents with higher probability that the
# recombination produces offspring capable of solving cases that the parents
# only solves partially. Parents are selected by fitness, then they are
# categorized in recipient (solves more cases) and donor (solves less cases).
# When the possible improvement of offspring with the donor (relative to
# recipient) is greater than the room for improvement of the recipient, the
# crossover si performed, otherwise a second candidate is s selected. 
#
##############################################################################


# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 

conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32))



# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new
# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo| 

  # Array to mark which parents are used for crossover 
  iscrossover = Array.new(conf["off-size"],false)

  # Number of cases solved by a donor (less 1s), not solved by the recipient 
  # (more 1s). 
  advantage = ->(recipient : Array(Int32), donor : Array(Int32)) do 
    a = 0
    recipient.size.times do |i| 
      a += ((!(recipient[i] == 1)) && (donor[i] == 1)) ? 1 : 0
    end 
    a
  end

  # Percentage of cases solved by a donor , not solved by a recipient. 
  alpha = ->(recipient : Array(Int32), donor : Array(Int32)) do
    a = advantage.call(recipient,donor)
    recipient_misses = (recipient.reduce(0){|a,e| a + ((!(e == 1))? 1 : 0)})
    a / recipient_misses
  end 

  # Room of improvement in recipient
  beta = ->(recipient : Array(Int32), donor : Array(Int32)) do 
    recipient_hits = recipient.reduce(0) do |a,e| a + e end 
    (1.0 - (recipient_hits / recipient.size))
  end 



  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
 

  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)


  evo.selection   = Selection.new("dps",
    ->(pop : Array(Indv)) do 

      # restore crossover binary array  
      iscrossover.size.times{|i| iscrossover[i] = false} 

      # Restore probabilities
      mut_prob = conf["mut-prob"]
      cx_prob  = conf["cx-prob"] 

      # Binarize , for each ith individual its binary string is a function of 
      # its test cases, if the jth testcase is less than the average of all 
      # test cases of the individual 1 otherwise 0.
      bin  = Array.new(pop.size) do |i| 
        pop[i].binarize(conf["ytrain"],conf["epsilon"]) 
      end 

      Array.new(conf["off-size"]) do |i| 
        # Select parents 
        p1i = Selection.tournament_index(pop,conf["tournament-size"]){|e| e.fit}  
        p2i = Selection.tournament_index(pop,conf["tournament-size"]){|e| e.fit}

        # if crossover is performed
        if rand < cx_prob  

          # Get binary array of first parent 
          p11s = bin[p1i].reduce(0){|a,e| a + (e ? 1 : 0)}

          update_genop_prob = true
          (pop.size//2).times do |_|
            p21s = bin[p2i].reduce(0){|a,e| a + (e ? 1 : 0)} 

            recipient = (p11s > p21s) ? bin[p1i] : bin[p2i]
            donor     = (p11s > p21s) ? bin[p2i] : bin[p1i]
      
            a = alpha.call(recipient,donor)
            b = beta.call(recipient,donor)

            # If the crossover is allowed 
            if a > b
              iscrossover[i] = true
              update_genop_prob = false
              break 
            end 

            # Select a new parent
            p2i = Selection.tournament_index(pop,conf["tournament-size"]){|e| e.fit}
          end
      
          # If no possible crossover was found the probabilities are updated
          if update_genop_prob
            cx_prob = Math.max(0.0,cx_prob - (1.0/pop.size))
            mut_prob = Math.min(1.0, mut_prob + (1.0/pop.size))
          end 
        end 

        # The selected parents
        [pop[p1i],pop[p2i]]
      end 
    end)



  evo.crossover   = Crossover.new("br", 
    ->(parents : Array(Array(Indv)), pop : Array(Indv)) do 
      parents.map_with_index do |ary,i|
        if !iscrossover[i]
          ary.sample.clone 
        else 
          p1,p2 = ary 
          # For brood recombination a tournament of 5 childrens is performed
          childs = Array.new(5) do 
            nsexp = Crossover.subtree_exchange(p1.sexp,p2.sexp)
            if nsexp.size > conf["max-size"]
              indv = {p1,p2}.sample.clone 
            else 
              indv = Indv.new(nsexp,env.clone)
              indv.age = Math.max(p1.age,p2.age)
              evo.indv_eval.call(indv)
              indv
            end 
          end  
          Selection.best_indv(childs)
        end 
      end 
  end)

  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.generational_elitist

  # ---------------------------------------------------------------------------
  Evolution.logs
end 
best_sol = evolution.evolve
