require "../gp.cr"

###############################################################################
# FOCUS :: Find Only and Complete Undominated Sets 
#
# Reducing bloat and promoting diversity using multi-objective methods
# De Jong E.D., Watson R.A, POllack J.B. 
# 3rd Annual conference on genetic and evolutionary computation 
#
# It uses a multi-objective approach where the objectives are a triplet of
# (fitness,size,diversity). It selects the non-dominated solutions to form 
# the new population 
#
###############################################################################

# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32))



# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error

dist_fun = ->GP::Distance.overlap_normalized(Sexp,Sexp)

evolution = GP::Evolution(Indv).new do |evo|
  
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.tournament(conf["off-size"],conf["tournament-size"])
  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.new("focus",
  ->(pop : Array(Indv), off : Array(Indv)) do 
    # Union of offspring and population 
    candidates = (pop + off).uniq

    # Calculate the distance between solutions  and generate the triplet 
    obj = candidates.map do |indv|
      avgdist = candidates.reduce(0.0) do |acc,val| 
        acc + (dist_fun.call(indv.sexp,val.sexp)**2)
      end 
      avgdist /= (candidates.size-1)
      {indv,{indv.fit,indv.sexp.size,-avgdist}}
    end
 
    # Get the nondominated set. 
    MultObj.nondom(obj,conf["epsilon"]) do |a,b|
      {true,false}
    end 
  end )


  # --------------------------------------------------------------------------
  Evolution.logs 
end 

best_sol = evolution.evolve

