require "../gp.cr"

###############################################################################
#
# KNOVELTY :: K-novelty selection 
#
# Improving genetic programming with novel exploration - exploitation control 
# Kelly Johnathan, Hemberg Erick 
# European Conference on Genetic programming
# 10.1007/978-3-030-16670-0_5
#  
# The parents are selected by lexicase with probability (1-k) and by novelty
# with probability k. The novelty is approximated comparing the candidate to an
# archive of unique solutions, as the average of a distance over a feature of
# the individual with the pool of solutions.  The key idea is that diversity
# and "quality" are not always in conflict, therefore the mixed pool will
# decrease converge to a local optima. 
#
###############################################################################


# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.knobelty_opts(ll),
  **ConfigParser.find("max-evals",ll,Int32))



# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error

# Novelty approximation 
def novelty_approx(indv, dist, cache, sample_size) 
  novelty = 0.0
  sample_size.times do 
    o = cache.sample 
    novelty += dist.call(indv,o)
  end 
  novelty /= sample_size
  return novelty
end 

evolution = GP::Evolution(Indv).new do |evo|
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 

  # History of unique individuals 
  archive_indvs = Deque(Indv).new(conf["knobelty-archive-size"])
  # History of fitness of unique individuals
  archive_fit = Deque(Array(Int32)).new(conf["knobelty-archive-size"])
  # History of unique individuals 
  archive = {} of Indv => Bool
  # Adaptive decrement of knobelty_prob 

  kprob = ->() do 
    2.0 ** (-conf["knobelty-lambda"] * evo.num_evals)
  end 


  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.new("knobelty",
    ->(pop : Array(Indv)) do 
      # Get the per case fitness 
      cases_fit = pop.map do |indv|
        byhat = [0.0] 
        by    = [0.0]
        (0 ... conf["ytrain"].size).map do |i|
          byhat[0] = indv.semantic[i]
          by[0]    = conf["ytrain"][i]
          obj_fun.call(byhat,by)
        end.to_a
      end

      bin = pop.map do |e| e.binarize(conf["ytrain"],conf["epsilon"]) end

      # Add to the history 
      pop.size.times do |i|
        indv = pop[i]
        fits = bin[i]
        if !archive[indv]? 
          archive_indvs.push(indv)
          archive_fit.push(fits)
          archive[indv] = true
        end 
      end 

      # Delete the oldest individuals from archive
      while (archive.size > conf["knobelty-archive-size"])
        archive_fit.shift
        archive.delete(archive_indvs.shift)
      end 


      # Select the parents
      Array.new(conf["off-size"]) do |i|
        2.times.map do 
          if rand < kprob.call() 
            si = Selection.tournament_index(bin,conf["tournament-size"]) do |e| 
              nv = novelty_approx(
                e,
                ->Distance.hamming(Array(Int32),Array(Int32)),
                archive_fit,
                conf["knobelty-sample-size"])
              -nv 
            end 
            pop[si]
          else
            Selection.lexicase(pop,cases_fit)
          end 
        end.to_a
      end 
    end)


  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.generational_elitist
  
  Evolution.logs
end 

best_sol = evolution.evolve

