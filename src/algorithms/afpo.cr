require "../gp.cr" 

###############################################################################
#
# AFPO :: Age Fitness Pareto Optimization 
#
# Age-fitness pareto optimization 
# Schmidt, Lipson
# Proceedings of the 12th Anual Conference on Genetic and Evolutionary Computation
# https://doi.org/10.1145/1830483.1830584
#
#
# The algorithm selects the non dominated solutions using the objective tuple
# of [fitness, age]. The key idea is the concept of genetic age, that is, 
# by considering the number of generations (age) that the genetic code has 
# been in the evolutionary process, newer but less fit solutions are allowed to
# pass their genetic code. 
#
#
###############################################################################

# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 


conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32))


# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new
# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo| 

  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
 

  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.tournament(conf["off-size"],conf["tournament-size"])
  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])

  evo.replacement = Replacement.new("age-fitness",
    ->(off : Array(Indv), pop : Array(Indv)) do 
      # only consider unique elements
      candidates = (pop + off).uniq

      # advance ages
      candidates.each{ |indv| indv.age += 1}

      # Add a new random individual 
      randindv = Indv.new do |indv| 
        indv.env = env.clone 
        indv.sexp = InitMethod.grow(
          min_depth: conf["rand-tree-min-depth"], max_depth: conf["rand-tree-max-depth"],env: indv.env)
        evo.indv_eval.call(indv) 
      end 
      candidates.push(randindv)


      # get multi-objective objectives (fitness,age)
      obj = candidates.map do |indv|
        {indv,{indv.fit,indv.age}}
      end 

      # find pareto front
      ndom,dom =MultObj.nondomanddom(obj,conf["epsilon"]) do |ia,ib|
        adom = ia.sexp.size <= ib.sexp.size
        {adom,!adom}
      end 

      # variant of pareto tournament, if the pareto front is equal or bigger than
      # the target size return directly, else select randomly non dominated
      # elements from the dominated set
      if ndom.size >= conf["pop-size"]
        return ndom.map(&.first)
      else
        dom.shuffle!
        while (ndom.size + dom.size) > conf["pop-size"]
          a = dom.pop 
          b = dom.pop 
          adom = MultObj.adomb(a.last,b.last,conf["epsilon"])
          if adom < 0
            dom.push a
          elsif MultObj.adomb(b.last,a.last,conf["epsilon"]) < 0
            dom.push b
          else 
            # when neither are dominated then select the smallest
            dom.push((a.first.sexp.size < b.first.sexp.size) ? a : b)
          end 
          dom.swap(-1,rand(dom.size))
        end 

        # complete the non-dominated-set with the winners in the dominated set 
        # to reach the population target size. 
        new_pop = [] of Indv
        ndom.each{|e| new_pop.push(e.first)}
        dom.each{|e| new_pop.push(e.first)}

        # return the new population 
        return new_pop
      end 
    end)

  # ---------------------------------------------------------------------------
  Evolution.logs 
end 
best_sol = evolution.evolve
