require "../gp.cr"

###############################################################################
# SIS :: Semantics in Selection
#
# Using semantics in the selection mechanism in genetic programming: A simple 
# method for promoting semantic diversity. 
# Galván-López, Cody-Kenny
# IEEE Congress on Evolutionary Computation 
# 10.1109/CEC.2013.6557931
# 
#
# Selects individuals that improve "quality" and promotes behavioral diversity
# In order to chose a pair of parents, the firs parent ios selected by its
# fitness, then the second one will be selected using the firs as a reference.
# a pool of potential second parents is randomly selected using and their
# semantics are compared to the first. Among the candidates different to the
# first parent the best fitness i selected. 
#
###############################################################################

# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.sis_opts(ll),
  **ConfigParser.find("max-evals",ll,Int32))


# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo|
  
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.new("sis",
    ->(pop : Array(Indv)) do 
      parents = Array(Array(Indv)).new(conf["off-size"]) do 
        pa = Selection.tournament(pop,conf["tournament-size"]){|indv| indv.fit}
        candidates = pop.sample(conf["tournament-size"])
        semantic_candidates = candidates.select{|c| !c.same_semantic_as(pa,conf["sis-semantic-threshold"])}
        pb = if !semantic_candidates.empty? 
          Selection.best_indv(semantic_candidates) 
        else 
          candidates.sample 
        end 
        [pa,pb]
      end 
    end)


  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.generational_elitist
  
  Evolution.logs
end 

best_sol = evolution.evolve

