require "../gp.cr"

# =============================================================================
# GDMDGP :: Genetic Marker Density Genetic Programming 
#
# An efficient structural diversity technique for genetic programming
# Burks A.R., Punch W.F. 
# Proceedings of the 2015 annual conference on genetic and evolutionary computation
# 10.1145/2739480.2754649
#
# Based on Pareto dominance selects the next generation wit the tuple of 
# (marker density, fitness) and a Pareto tournament. The marker density is 
# defined as the percentage of the population with a specific marker, 
# that is the structure of the tree from root up to a specified depth. 
# This promotes exploration by preventing the convergence to single tree 
# structure near the root of trees. 
#
#
# =============================================================================

# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.gmdgp_opts(ll),
  **ConfigParser.find("max-evals",ll,Int32))



# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo|
  
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.tournament(conf["off-size"],conf["tournament-size"])
  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.new("gmd",
    ->(off : Array(Indv), pop : Array(Indv)) do 
      # Union of offspring and population 
      candidates = pop + off
      
      # Calculate the marker density 
      markercount = {} of String => Int32
      candidates.each do |indv|
        marker = Sexp.genetic_marker(indv.sexp,conf["gmdgp-marker-mindepth"],conf["gmdgp-marker-maxdepth"])
        markercount[marker] = (markercount[marker]? || 0) + 1
      end 

      # Generate the tuples 
      obj = candidates.map do |indv|
        marker = Sexp.genetic_marker(indv.sexp,conf["gmdgp-marker-mindepth"],conf["gmdgp-marker-maxdepth"])
        {indv,{indv.fit,markercount[marker]/candidates.size}}
      end 

      # Select the pareto front 
      ndom,dom = MultObj.nondomanddom(obj,conf["epsilon"]) do |ia,ib| 
        adom = ia.sexp.size <= ib.sexp.size 
        {adom,!adom}
      end 

      if ndom.size >= conf["pop-size"]
          # If the pareto front is bigger than desired population return the pareto front
          return ndom.map(&.first)
        else 
          # The pareto front is smaller than desired population, perform pareto
          # tournament on dominated set to complete the population 
          dom.shuffle!
          while (ndom.size + dom.size) > conf["pop-size"]
            a = dom.pop
            b = dom.pop 
            adom = MultObj.adomb(a.last,b.last,conf["epsilon"])
            if adom < 0
              dom.push a
            elsif MultObj.adomb(b.last,a.last,conf["epsilon"]) < 0
              dom.push b
            else 
              if adom == 0
                dom.push((a.first.sexp.size < b.first.sexp.size) ? a : b) 
              else 
                dom.push((rand(0.0..1.0) < 0.5) ? a : b)
              end 
            end 
            dom.swap(-1,rand(dom.size))
          end 
          # Return the new population 
          new_pop  = [] of Indv 
          ndom.each{|e| new_pop.push(e.first)}
          dom.each {|e| new_pop.push(e.first)}
          return new_pop
        end 
    end)

  #--------------------------------------------------------------------------- 
  Evolution.logs
end 

best_sol = evolution.evolve

