require "../gp.cr"

##############################################################################
# lexicase :: Epsilon lexicase 
#
# Epsilon lexicase selection for regression 
# La Cava, Spector, Danai
# Proceedings of the genetic and evolutionaru computation conference
# 10.1145/2908812.2908898
#
# It randomly selects individuals with different strenghts. To select a parent
# , test cases are randomly selected and potential individuals are filtered
# based on their performance on the case, until either tehere are no more
# test cases or only one individual is fit. 
#
#
##############################################################################


# Parse Arguments ----------------------------------------------
config_str = GP::ConfigParser.argv2alist
ll = LogList.new(config_str) 
if (config_fname = ll["config-file"]?) && File.exists?(config_fname)
  config_str = File.read(config_fname)
  ll += LogList.new(config_str) 
end 
conf = ConfigParser.config(
  **ConfigParser.base(ll),
  **ConfigParser.find("max-evals",ll,Int32))


# Initialization -----------------------------------------------

# Environment 
env = Crisp::Env.new 
conf["funs"].each do |f| env.register_fun f end 
conf["terms"].each do |t| env.register_term t end 
conf["vars"].each do |v| env.register_input v end 

# Log 
log = GP::Logger.new

# Obj Fun 
obj_fun = GP::ObjectiveFunction.mean_squared_error


evolution = GP::Evolution(Indv).new do |evo|
  
  # How to evaluate each individual (the fitness of the individual is modified) 
  evo.indv_eval = Proc(Indv,Void).new do |i| 
    i.evaluate(conf["xtrain"], conf["ytrain"], conf["vars"],obj_fun, conf["infinity"])
  end 
  
  evo.stop_crit   = StopCriteria.by_evals(conf["max-evals"],evo)
  evo.pop_init    = InitMethod.rhh( conf["pop-size"], conf["max-depth"], env)
  evo.selection   = Selection.new("lexicase",
    ->(pop : Array(Indv)) do 

      parents    = Array(Array(Indv)).new(conf["off-size"])

      # Calculate the fitness for each case 
      batchyhat = [0.0]
      batchy    = [0.0]
      cases_fit = pop.map do |indv|
        (0 ... indv.semantic.size).map do |i|
          batchyhat[0] = indv.semantic[i]
          batchy[0]    = conf["ytrain"][i]
          obj_fun.call(batchyhat,batchy)
        end.to_a
      end 

      # Generate the offspring
      conf["off-size"].times do 
        p = 2.times.map do 
          Selection.lexicase(pop,cases_fit)
        end.to_a
        parents.push(p)
      end  
      parents
    end) 

  evo.crossover   = Crossover.subtree_exchange(conf["cx-prob"], conf["max-size"],env)
  evo.mutation    = Mutation.subtree_replacement(conf["mut-prob"], conf["rand-tree-min-depth"], conf["rand-tree-max-depth"], conf["max-size"])
  evo.replacement = Replacement.generational_elitist
  #----------------------------------------------------------------------------  
  Evolution.logs
end 

best_sol = evolution.evolve






