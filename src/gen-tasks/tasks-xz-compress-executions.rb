#!/usr/bin/env ruby 

require "optparse"


# =============================================================================
# PARÁMETROS
# =============================================================================
#
workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "can't find WORKDIR" 
errdir   = File.join(workdir,"err")
outdir   = File.join(workdir,"out-xz")
program  = File.join("xz --keep --stdout ")


progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}

# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing of their compression.

  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    options["inputfile"] = file
  end 

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 

  parser.on("--man","detailed help") do
    puts <<~man
    NAME
      #{progname.upcase}
    SYNOPSIS
      find $PWD/out -type 'f' -print | #{progname} [options] --if=-
      #{progname} [options] --if=-  < files.txt

      --if=FILE    inputfile
      --help,-h    help
      --man        detailed help

    DESCRIPTION

      This program reads from a file the path  of files in the working 
      directory to generate the tasks of compressing the files 
      using the program XZ for it's parallel execution.
      
    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |_ input-files-directory
         |_...

    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit 0
  end 
end 
parser.parse!


# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 


# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e) && File.file?(e)}

files.each do |infile|
  name = File.basename(infile)
  puts %(cd #{workdir} && #{program} #{infile} > #{File.join(outdir,name)}.xz 2> #{errdir}/#{name}--xz-compress.err)
end 
