#!/usr/bin/env ruby 

require "optparse"

# =============================================================================
# PARAMETROS
# =============================================================================

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "cant find WORKDIR" 

fset = [
  "koza-01",
  "koza-02",
  "koza-03",
  "nguyen-01",
  "nguyen-03",
  "nguyen-04",
  "nguyen-05",
  "nguyen-06",
  "nguyen-07",
  "nguyen-08",
  "nguyen-09",
  "nguyen-10",
  "keijzer-06",
  "keijzer-10",
  "keijzer-12",
  # "korns-12",
  "pagie-01",
  "vladislavleva-04",
] 
funset    = "'+,-,*,/,sin,cos,exp,ln,sq,rpcl'"
termset   = "int,float"

nruns = 30

exp_name = ENV["EXPNAME"] || "test"

exp_des  = <<-des
Comparar entre las estrategias de control de diversidad focus vs proposal
des

max_evals  = 76800 # ~512 iteraciones standard-gp

opts = { 
  "run-name" => "",
  "terms" => termset,
  "funs" => funset,
  "pop-size" => 200,
  "off-size" => 200,
  "stop-criteria" => "by-evals",
  "max-evals" => max_evals, 
  "cx-prob" => 0.9,
  "mut-prob" => 0.2,
  "tourn-size" => 3,
  "req-dist" => 0.5,
  "obj-fun" => "mse",
  "dist-fun" => "edit2-normalized",
  "vars" => "x0,x1,x2,x3,x4,x5",
  "max-size" => 1024,
  "genmarker-mindepth" => 0,
  "genmarker-maxdepth" => 3,
  "semantic-threshold" => 0.01,
  "epsilon" => 1e-7,
  "infinity" => 1e50,
  "edit2-k" => 0.5, 
  "rand-tree-min-depth" => 1,
  "rand-tree-max-depth" => 8,

  "alps-age-gap" => 3,
  "alps-num-layers" => 10,
  "alps-num-elites" => 3,

  "knobelty-sample-size" => 100,
  # 3/4 decrement in the first 20% evaluations 
  "knobelty-lambda" => -Math.log(0.25,2)/(0.2*max_evals),

  "archive-size" => 2000,
  "log-evo" => nil,
  "log-conf" => nil,
  "log-traindata" => nil,
  "log-valdata" => nil,
} 


algs = {} 

# ----------------------------
# K = 0.5 
opts["run-name"] = "gpdmd-analysis"
algs["gpdmd-analysis"] = opts.dup 

opts["run-name"] = "gpdmd-farthest-analysis"
algs["gpdmd-farthest-analysis"] = opts.dup 

opts["run-name"] = "gpdmd-front-analysis"
algs["gpdmd-front-analysis"] = opts.dup 

opts["run-name"] = "gpdmd-smallest-analysis"
algs["gpdmd-smallest-analysis"] = opts.dup 



# =============================================================================
# PROGRAMA 
# =============================================================================

verbose = false
if verbose 
  puts <<~header
  # Tasks #{Time.now}
  #
  # exp: 
  # #{exp_name}
  #
  # desc: 
  # #{exp_des.lines.join("# ")}
  #
  # functions   : 
  #  - #{fset.join("\n#  - ")}
  #
  # algorithms  : 
  #  - #{algs.keys.join("\n#  - ")} 
  #
  # runs per combination #{nruns}

  header
end 


algs.each do |alg,conf|
  begin 
  if verbose 
    puts "# #{alg} #{"="*70}"
    conf.each{|p,v| puts "# #{p.ljust(10,' ')} : #{v}"} 
    puts 
  end 
  fset.each do |f|
    puts "# #{f.ljust(10)} #{"-"*70}" if verbose 
    nruns.times do |r|
      print <<~config
      cd #{workdir} \
      && \
      bin/#{conf["run-name"]}.app \
      --xtrain=training/#{f}.in \
      --ytrain=training/#{f}.out \
      --xval=validation/#{f}.in \
      --yval=validation/#{f}.out \
      #{conf.each_key.map{|k| "--#{k}#{conf[k] ? "=#{conf[k]}" : ""}"}.to_a.join(" ")} \
      2> err/#{f}--#{alg}--#{exp_name}.err--#{"%02d" % r} \
      > pop-analysis/#{f}--#{alg}--#{exp_name}.pr-sz-dcn--#{"%02d" % r} 
      config
    end 
  end 
  rescue
    exit
  end 
end 

