#!/usr/bin/env ruby

require "optparse"

# =============================================================================
# PARAMETROS
# =============================================================================

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {} 

# =============================================================================
# OPCIONES 
# =============================================================================
#
parser = OptionParser.new do |parser|
  parser.banner = <<~banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing 

  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    options["inputfile"] = file
  end 

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 
  
  parser.on("--man", "detailed help") do
    puts <<~man
    NAME
      #{progname.upcase}
    SYNOPSIS
      ls -d $PWD/out/* | #{progname} [options] --if=-

      OPTIONS
        --if        input file
        --help,-h   help
        --man       detailed help
      
    DESCRIPTION
      This program reads from a file the path of files in the working directory 
      to generate the tasks of extracting the best solution metrics using the 
      program MERGE-BEST-METRICS.RB for its parallel execution.

    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:

      WORKDIR
      |- bin
      |  |- merge-best-metrics.rb
      |  |_ ...
      |- err
      |  |_...
      |_ input-files-directory
         |_...

    LISCENCE
      MIT

    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit 0
  end 
end 
parser.parse!


# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 

# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "cant find WORKDIR" 
bindir   = File.join(workdir,"bin")
errdir   = File.join(workdir,"err")
program  = File.join(bindir,"best-merge-metrics.rb")
inputdir = File.join(File.dirname(files.first))

# Obtener las instancias, métodos y ids
instances = {} # of String => String
methods   = {} # of String => String
ids       = {}
files.each do |fname|
  name = File.basename(fname)
  inst,met,id = name.split(/--/)
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
end 
instances = instances.values.sort
methods = methods.values.sort
ids     = ids.values.sort
raise "se encontraron diferentes experimentos" if ids.size != 1

# Generar las tareas para el procesamiento en paralelo
instances.each do |inst|
  methods.each do |met|
    inputfile = File.join(inputdir,"#{inst}--#{met}--*")
    puts %(cd #{workdir} && ls -d #{inputfile} | ruby #{program} --if=- --out-dir=proc/best 2> #{errdir}/#{inst}--#{met}--best-merge.err)
  end 
end 
