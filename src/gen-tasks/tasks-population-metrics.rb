#!/usr/bin/env ruby

require "optparse"

# =============================================================================
# PARÁMETROS
# =============================================================================

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}

# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing

  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    options["inputfile"] = file
  end 

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 

  parser.on("--man","detailed help") do
    puts <<~man
    NAME
      #{progname.upcase}
    SYNOPSIS
      ls -d out/* | #{progname} [options] --if=-

      --if=FILE    inputfile
      --help,-h    help
      --man        detailed help

    DESCRIPTION

      This program reads from a file the path  of files in the working 
      directory to generate the tasks of extracting the population metrics 
      using the program POPULATION-METRICS.APP for it's parallel execution.
      
    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |- bin
      |  |- population-metrics.app
      |  |_...
      |- err
      |  |_...
      |_ input-files-directory
         |_...

    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit 0
  end 
end 
parser.parse!


# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 


# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "cant find WORKDIR" 
bindir   = File.join(workdir,"bin")
errdir   = File.join(workdir,"err")
program  = File.join(bindir,"population-metrics.app")

files.each do |inputfile|
  next unless File.file?(inputfile)
  name = File.basename(inputfile,File.extname(File.basename(inputfile)))
  ext  = File.extname(File.basename(inputfile)).split(/\.|--/).last
  puts %(cd #{workdir} && #{program} #{inputfile}  2> #{errdir}/#{name}--population-metrics-#{ext}.err)
end 
