#!/usr/bin/env ruby

require "optparse"

# =============================================================================
# PARÁMETROS
# =============================================================================

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "cant find WORKDIR" 
bindir   = File.join(workdir,"bin")
errdir   = File.join(workdir,"err")
program  = File.join(bindir,"population-merge-metrics.rb")
rng_mtr = "evals"

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}
options["nbins"] = 512
# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing

  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    options["inputfile"] = file
  end 

  parser.on("--begin=NUM","begin of range") do |num|
    options["begin"] = num
  end 

  parser.on("--end=NUM", "end of range") do |num|
    options["end"] = num
  end 

  parser.on("--nbins=INT", "histogram's bins number") do |num|
    options["nbins"] = num
  end 

  parser.on("--rng-metric=STR", "metric used for the histogram") do |str|
    rng_mtr = str
  end

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 

  parser.on("--man","detailed help") do
    puts <<~man
    NAME
      #{progname.upcase}
    SYNOPSIS
      ls -d out/* | #{progname} [options] --if=- --begin=0 --end=256 --nbins=256

      --if=FILE         inputfile
      --begin=NUM       range start 
      --end=NUM         range end 
      --nbins=INT       histogram resolution (number of bins)
      --rng-metric=STR  metric used for the histogram (evals is default)
      --help,-h         help
      --man             detailed help

    DESCRIPTION

      This program reads from a file the path of files in the working directory
      to generate the tasks of mergin the independent executions population
      metrics using the program POPULATION-MERGE-METRICS.APP for it's parallel 
      execution. 

      It is expected that each metric is in a sub-folder inside the directory 

      input-files-directory
      |- ametric
      |  |- [instance]--[method]--[id]--ametric.evo--[run-id]
      |  |_ ... 
      |- bmetric
      |- ithmetric
      |_ ...
      
    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |- bin
      |  |- population-merge-metrics.app
      |  |_ ...
      |- err
      |  |_ ...
      |_ input-files-directory
         |_ ...

    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit 0
  end 
end 
parser.parse!


# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 

unless options["begin"] && options["end"]
  STDERR.puts "ERROR begin and end of range must be given" 
  STDERR.puts parser
  exit(1)
end 

# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

# Obtener la instancia, método, etiqueta y metrica
instances = {}
methods   = {}
ids       = {}
metrics   = {}
dirs      = {} 

files.each do |fname|
  dir =  File.dirname(fname)
  name = File.basename(fname,File.extname(fname))
  inst,met,id,mtr = name.split("--")
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
  dirs[dir] = dir 
end 

instances = instances.values 
methods   = methods.values
ids       = ids.values
metrics   = metrics.values

# Solo se admite un directorio 
raise "ERROR more than one input dir" if dirs.size != 1
inputdir      = dirs.values.first

# Para cada combinación 
ids.each do |id|
  instances.each do |inst|
    methods.each do |met|
      metrics.each do |mtr|
        next if mtr == rng_mtr
        # Obtener el directorio
       
        # Generar nombres de los archivos
        file_prefix = "#{inst}--#{met}--#{id}--#{mtr}"
        error_fname = File.join(errdir,"#{file_prefix}-merge.err")

        # Ejecutar el programa
        print %( cd #{workdir} )
        print %( && ls -d #{File.join(inputdir,file_prefix)}* )
        print %(          #{File.join(inputdir,file_prefix.gsub(mtr,rng_mtr))}* )
        print %( | #{program} --if=- )
        print %( --begin=#{options["begin"]} ) 
        print %( --end=#{options["end"]} ) 
        print %( --nbins=#{options["nbins"]} )
        print %( --rng-metric=#{rng_mtr} )
        print %( 2> #{error_fname} )
        puts ""
      end 
    end 
  end 
end 
