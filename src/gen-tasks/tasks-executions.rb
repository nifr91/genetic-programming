#!/usr/bin/env ruby 

require "optparse"

# =============================================================================
# PARAMETROS
# =============================================================================

workdir = ENV["WORKDIR"] || ENV["PWD"] or raise "error cant find WORKDIR"

fset = [
  "koza-01",
  "koza-02",
  "koza-03",
  "nguyen-01",
  "nguyen-03",
  "nguyen-04",
  "nguyen-05",
  "nguyen-06",
  "nguyen-07",
  "nguyen-08",
  "nguyen-09",
  "nguyen-10",
  "keijzer-06",
  "keijzer-10",
  "keijzer-12",
  "korns-12",
  "pagie-01",
  "vladislavleva-04",
  "keijzer-07", 
  "keijzer-08", 
  "keijzer-09", 
  "korns-01",
  "korns-05",
  "korns-08",
  "vladislavleva-06",
  "keijzer-01",
] 

nruns = 30

exp_name = ENV["EXPNAME"] || "test"

exp_des  = <<-des
Comparar entre las estrategias de control de diversidad focus vs proposal
des

max_evals  = 76800 # ~512 iteraciones standard-gp

opts = { 
  # name of the runn (a unique id) 
  "run-name" => "",
  "alg-name" => "", 

  # general GP options 
  "terms" => "int,float",
  "vars" => "x0,x1,x2,x3,x4,x5,x6",
  "funs" =>  "'+,-,*,/,sin,cos,exp,ln,sq,rpcl'",
  "pop-size" => 200,
  "off-size" => 200,
  "max-evals" => max_evals, 
  "max-depth" => 17,
  "cx-prob" => 0.6,
  "mut-prob" => 0.2,
  "tournament-size" => 3,
  "max-size" => 1024,
  "epsilon" => 1e-12,
  "infinity" => 1e50,
  "rand-tree-min-depth" => 1,
  "rand-tree-max-depth" => 8,
   
  # edit2 distance 
  "edit2-depth-wgt" => 0.5,
   
  # dmdgp options 
  "dmdgp-req-dist" => 0.5,
  "control-point" => "0,0", 

  # logging-options 
  "log-evo" => "false",
  "log-config" => "true",
  "log-traindata" => "true",
  "log-valdata" => "true",
  "log-pop" => "false",
} 


algs = {} 


opts["alg-name"] = "dmdgp-constant" 
["0.00","0.125","0.25","0.5"].each do |istr|
  opts["dmdgp-req-dist"] = istr.to_f 
  algs["dmdgp-dc#{istr}"] = opts.dup
end 

opts["alg-name"] = "dmdgp" 
["0.00","0.125","0.25","0.5"].each do |istr|
  opts["dmdgp-req-dist"] = istr.to_f 
  algs["dmdgp-#{istr}"] = opts.dup
end


opts["dmdgp-req-dist"] = 0.5
opts["alg-name"] = "dmdgp" 
algs["dmdgp"] = opts.dup
opts["alg-name"] = "dmdgp-overlap" 
algs["dmdgp-overlap"] = opts.dup
opts["alg-name"] = "dmdgp-hamming" 
algs["dmdgp-hamming"] = opts.dup


opts["alg-name"] = "dmdgp-bezier" 
[["br","0,0"],["tl","#{max_evals},1"],["md","0,1"]].each do |id,istr|
  opts["dmdgp-req-dist"] = 0.5
  algs["dmdgp-bezier-#{id}"] = opts.dup
end 






# =============================================================================
# PROGRAMA 
# =============================================================================

verbose = false
if verbose 
  puts <<~header
  # Tasks #{Time.now}
  #
  # exp: 
  # #{exp_name}
  #
  # desc: 
  # #{exp_des.lines.join("# ")}
  #
  # functions   : 
  #  - #{fset.join("\n#  - ")}
  #
  # algorithms  : 
  #  - #{algs.keys.join("\n#  - ")} 
  #
  # runs per combination #{nruns}

  header
end 


algs.each do |id,conf|
  begin 
  if verbose 
    puts "# #{id} #{"="*70}"
    conf.each{|p,v| puts "# #{p.ljust(10,' ')} : #{v}"} 
    puts 
  end 
  fset.each do |f|
    puts "# #{f.ljust(10)} #{"-"*70}" if verbose 
    nruns.times do |r|
      print <<~config
      cd #{workdir} \
      && \
      bin/#{conf["alg-name"]}.app \
      --xtrain-file=training/#{f}.in \
      --ytrain-file=training/#{f}.out \
      --xval-file=validation/#{f}.in \
      --yval-file=validation/#{f}.out \
      --dataset-name=#{f} \
      #{conf.each_key.map{|k| "--#{k}#{conf[k] ? "=#{conf[k]}" : ""}"}.to_a.join(" ")} \
      2> err/#{f}--#{id}--#{exp_name}.err--#{"%02d" % r} \
      | xz - > out/#{f}--#{id}--#{exp_name}.out--#{"%02d" % r}.xz \
      2> err/#{f}--#{id}--#{exp_name}--xz.err--#{"%02d" % r}
      config
    end 
  end 
  rescue
    exit
  end 
end 


