#!/usr/bin/env ruby 
require "optparse"

# =============================================================================
# PARÁMETROS
# =============================================================================

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "cant find WORKDIR" 
bindir   = File.join(workdir,"bin")
program  = File.join(bindir,"eplot.app")

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}

# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing

  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    options["inputfile"] = file
  end 

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 

  parser.on("--man","detailed help") do
    puts <<~man
    NAME
      #{progname.upcase}
      
    SYNOPSIS
      ls -d out/* | #{progname} [options] --if=- 

    #{parser.summarize("",20,80,"    ")}

    DESCRIPTION

      This program reads from a file the path of files in the working directory
      to generate  the graphs of the median using the program EPLOT.APP for
      it's parallel execution. 

      It is expected that each metric is in a sub-folder inside the directory 

      input-files-directory
      |- ametric
      |  |- [instance]--[method]--[id]--[ametric].median
      |  |_ ... 
      |- bmetric
      |- ithmetric
      |_ ...
      
    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |- bin
      |  |- elot.app
      |  |_ ...
      |- err
      |  |_ ...
      |_ input-files-directory
         |_ ...

    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit 0
  end 
end 
parser.parse!


# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 


# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

# Obtener la instancia, método, etiqueta y metrica
instances = {}
methods   = {}
ids       = {}
metrics   = {}
dirs      = {} 

files.each do |fname|
  dir =  File.dirname(File.dirname(fname))
  name = File.basename(fname,File.extname(fname))
  inst,met,id,mtr = name.split("--")
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
  dirs[dir] = dir 
end 

instances = instances.values 
methods   = methods.values
ids       = ids.values
metrics   = metrics.values

# Solo se admite un directorio 
raise "ERROR more than one input dir" if dirs.size != 1
dir      = dirs.values.first

# Para cada combinación 
ids.each do |id|
  instances.each do |inst|
    metrics.each do |mtr|
      ylog = ((mtr =~ /fit/)) ? "--ylog" : ""

      # Generar nombres de los archivos
      fnames = File.join(dir,mtr,"#{inst}*.median")
      error_fname = File.join("err","#{inst}--svg.err")

      # Ejecutar el programa
      print %( cd #{workdir} )
      print %( && )
      print %( mkdir -p #{File.join("proc","svg",mtr)} )
      print %( && )
      print %( #{program} #{fnames} )
      print %( --legend )
      print %( --title="#{inst} -- #{mtr}" )
      print %( --xlabel="evals" )
      print %( --ylabel="#{mtr}" )
      print %( #{ylog} )
      print %( > #{File.join("proc","svg",mtr,"#{inst}--#{id}--#{mtr}.svg")} )
      print %( 2> #{error_fname} )
      puts ""
    end 
  end 
end
