#!/usr/bin/env ruby

require "optparse"

# =============================================================================
# PARAMETROS
# =============================================================================

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "can't find WORKDIR" 
bindir  = File.join(workdir,"bin")
outbasedir = File.join(workdir,"proc","svg","best-scatter")
program = File.join(bindir,"eplot.app")
progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}


# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing
  banner


  parser.on("--if=FILE","name of file to read (- for stdin)") do |file|
    options["inputfile"] = file
  end 

  
  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end 

  parser.on("--man", "detailed help") do 
    puts <<~man
    NAME 
      #{progname.upcase}
    
    SYNOPSIS
      ls -d path/to/dir/* | #{progname} [options] --if=-
    
    #{parser.summarize("",20,80,"    ")}
    
    DESCRIPTION
      This program reads from a file the path of files in the working directory
      to plot a scatter plot with EPLOT.APP  program for it's parallel execution.

      It is expected that each metric is in a subfolder inside the directory 

      input-files-directory
      |-ametric-vs-bmetric
      | |-[instance]--[method]--[id]--[ametric-vs-bmetric].merge
      | |_.... 
      |-bmetric
      | |_....
      |_...

      The expected output is for each metric pair, for each instance a 
      scatterplot. 

      output-dir
      |- svg 
      | |- scatter
      | |- ametri-vs-bmetric
      | | | |-[instance]--[id]--[ametric-vs-bmetric].svg
      | | | |_...
      | | |_...
      | |_...
      |_...

    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |- bin
      |  |- elot.app
      |  |_ ...
      |- err
      |  |_ ...
      |_ input-files-directory
      |  |_ ...
      |_...


    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit(0)
  end 
end 
parser.parse!

# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 



# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

# Obtener la instancia, método, etiqueta y metrica
instances = {}
methods   = {}
ids       = {}
metrics   = {}
dirs      = {} 

files.each do |fname|
  dir =  File.dirname(File.dirname(fname))
  name = File.basename(fname,File.extname(fname))
  inst,met,id,mtr = name.split("--")
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
  dirs[dir] = dir 
end 

instances = instances.values 
methods   = methods.values
ids       = ids.values
metrics   = metrics.values


# Solo se admite un directorio 
raise "ERROR more than one input dir" if dirs.size != 1
dir      = dirs.values.first


ids.each do |id|
  f0 = files.select{|f| f =~ /--#{id}--/}
  instances.each do |inst|
    f1 = f0.select{|f| f =~ /#{inst}--/}
    metrics.each do |mtr|
      f2 = f1.select{|f| f =~ /--#{mtr}/}.sort

      xl = mtr.split("-vs-").first
      yl = mtr.split("-vs-").last 

      print %(cd #{workdir} )
      print %(&& )
      print %(mkdir -p #{File.join(outbasedir,mtr)} )
      print %(&& )
      print %(#{program} #{f2.join(" ")} --title="#{inst}" --ylog --xlabel="#{xl}" --ylabel="#{yl}" --maxy=100 --miny=0 --minx=0 --maxx=1024 --legend --scatter )
      print %(> #{File.join(
        outbasedir,
        mtr,
        "#{inst}--#{id}--#{mtr}.svg")} )
      print %(2> #{File.join(workdir,"err","#{inst}--svg-scatter-#{mtr}.err")} )
      puts ""
    end 
  end 
end 





