#!/usr/bin/env ruby

require "optparse"

# =============================================================================
# PARAMETROS
# =============================================================================

workdir  = ENV["WORKDIR"] || ENV["PWD"] or raise "can't find WORKDIR" 
bindir  = File.join(workdir,"bin")
outbasedir = File.join(workdir,"proc","best","scatter")

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {}


# =============================================================================
# OPCIONES
# =============================================================================

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=[FILE]

  This program reads from FILE the filenames to process and creates the 
  corresponding tasks for parallel processing
  banner


  parser.on("--if=FILE","name of file to read (- for stdin)") do |file|
    options["inputfile"] = file
  end 

  
  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end 

  parser.on("--man", "detailed help") do 
    puts <<~man
    NAME 
      #{progname.upcase}
    
    SYNOPSIS
      ls -d path/to/dir/* | #{progname} [options] --if=-
    
    #{parser.summarize("",20,80,"    ")}
    
    DESCRIPTION
      This program reads from a file the path of files in the working directory
      to concatenate the measurements of metric 'x' and metric 'y' using the 
      GNU 'paste' program for it's parallel execution.

      It is expected that each metric is in a subfolder inside the directory 

      input-files-directory
      |-ametric
      | |-[instance]--[method]--[id]--[ametric].merge
      | |_.... 
      |-bmetric
      | |_....
      |_...

    ENVIRONMENT
      If defined the environment variable WORKDIR specifies the path to the 
      working directory. It must have the following structure:
    
      WORKDIR
      |- bin
      |  |- elot.app
      |  |_ ...
      |- err
      |  |_ ...
      |_ input-files-directory
         |_ ...

    LISCENCE
      MIT
    
    AUTHORS
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    man
    exit(0)
  end 
end 
parser.parse!

# Revisar que el nombre del archivo se encuentra presente
unless options["inputfile"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 



# =============================================================================
# PROGRAMA
# =============================================================================

# Leer los archivos
files = if options["inputfile"] == "-"
  STDIN.each_line(chomp: true).to_a
else
  File.read(options["inputfile"]).each_line(chomp: true).to_a
end 

# Filtrar los archivos que existen
files = files.select{|e| File.exists?(e)}

# Obtener la instancia, método, etiqueta y metrica
instances = {}
methods   = {}
ids       = {}
metrics   = {}
dirs      = {} 

files.each do |fname|
  dir =  File.dirname(File.dirname(fname))
  name = File.basename(fname,File.extname(fname))
  inst,met,id,mtr = name.split("--")
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
  dirs[dir] = dir 
end 

instances = instances.values 
methods   = methods.values
ids       = ids.values
metrics   = metrics.values


# Solo se admite un directorio 
raise "ERROR more than one input dir" if dirs.size != 1
dir      = dirs.values.first

# Se deben tener dos métricas 
if metrics.size != 2
  STDERR.puts "ERROR Two metrics must be specified"
  STDERR.puts "see --man for more information"
  exit(1)
end 

ids.each do |id|
  f0 = files.select{|f| f =~ /--#{id}--/}
  instances.each do |inst|
    f1 = f0.select{|f| f =~ /#{inst}--/}
    methods.each do |met|
      f2 = f1.select{|f| f =~ /--#{met}--/}

      ametfname = f2.select{|f| f =~ /--#{metrics.first}/ }.pop
      bmetfname = f2.select{|f| f =~ /--#{metrics.last}/ }.pop

      scatterdirname = metrics.join("-vs-")
      print %(cd #{workdir} )
      print %(&& )
      print %(mkdir -p #{File.join(dir,"scatter",scatterdirname)} )
      print %(&& )
      print %(paste -d' ' "#{bmetfname}" "#{ametfname}" )
      print %(> #{File.join(
        outbasedir,
        scatterdirname,
        "#{inst}--#{met}--#{id}--#{scatterdirname}.merge")} )
      print %(2> #{File.join(workdir,"err","#{inst}--scatter-merge-#{scatterdirname}.err")} )
      puts ""

      scatterdirname = metrics.reverse.join("-vs-")
      print %(cd #{workdir} )
      print %(&& )
      print %(mkdir -p #{File.join(dir,"scatter",scatterdirname)} )
      print %(&& )
      print %(paste -d' ' "#{bmetfname}" "#{ametfname}" )
      print %(> #{File.join(
        outbasedir,
        scatterdirname,
        "#{inst}--#{met}--#{id}--#{scatterdirname}.merge")} )
      print %(2> #{File.join(workdir,"err","#{inst}--scatter-merge-#{scatterdirname}.err")} )
      puts ""
    end 
  end 
end 





