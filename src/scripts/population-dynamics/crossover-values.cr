require "../../logger/loglist.cr" 



files = [] of String
while line = gets 
  files.push(line)  if File.exists?(line) 
end 


xzcat = Process.new("xzcat", args=[files[0]],output: Process::Redirect::Pipe)
ngens =  0
while line = xzcat.output.gets 
  ngens += 1 if line =~ /crossover-dynamics/ 
end 




keys = [
  "pop-avg-euc-dist",  #1
  "pop-avg-pair-dist", #2
  "pop-avg-edit-dist", #3
  "avg-edit2",         #4
  "avg-euclidean",     #5
  "avg-hamming",       #6
  "avg-fit"]           #7


arr_data = Array.new(ngens) do Array.new(keys.size,0.0) end 

files.each do |fname| 
  xzcat = Process.new("xzcat", args=[fname],output: Process::Redirect::Pipe) 
  io = xzcat.output
  lines = [] of Array(String) 

  g = 0
  while line = io.gets 
    ll = GP::LogList.new(line)
    lst = ll.assq_ref("crossover-dynamics") 
    unless lst.empty?

      keys.each_with_index do |key,k| 
        arr_data[g][k] += lst[key]?.not_nil!.to_f
      end 
      g+= 1
    end 
  end 
end 

arr_data.each do |g|
  puts (g.map do |e| e / files.size end).join(" ") 
end 


