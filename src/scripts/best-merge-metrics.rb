#!/usr/bin/env ruby

require "optparse"
require "fileutils"

# =============================================================================
# PARAMS 
# =============================================================================

# Metrics from the best solutions 
metrics = [ 
  "train-fit",
  "val-fit",
  "size",
  "best-sol",
  "sr",
]

# =============================================================================
# HELP
# =============================================================================
progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
options = {} 
options["out-dir"] = "best"

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: 
    ls [dir-path]/[instance]--[method]--[id].out--*  | #{progname} --if=-
  
  This program reads from FILE the filenames to process and extract from them 
  the fitness, sexp and size from the line with the best-solution. 
  banner

  parser.on("--if=FILE","File to read ")  do |fname|
    options["if"] = fname
  end 

  parser.on("--out-dir=DIR","Output directory")  do |dirname|
    options["out-dir"] = dirname  
  end 

  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end

  parser.on("--man","a more detailed help") do 
    puts  <<-man
    NAME
      #{progname.upcase}
   
    SYNOPSIS 
      ls [files] | #{progname} --if=-
     
      #{parser.summarize("",20,80,"    ")}

    DESCRIPTION 

      The program reads from STDIN the filenames to process. Each file is the 
      output of an independent execution of an algorithm in a problem. 
      
      The expected file format is a associative list as a s-expression in 
      each line. With the last line containing the following fields: 
      
      (#{metrics.join(" "){|e| "(#{e} ...)"}} ...)

      The output is a file for each metric with the following path structure

      [out-dir]/[instance]--[method]--[id]--[metric].merge

    EXAMPLES

      # Process all parallel executions of a "method" in a problem "instance"
      # in a directory "dir-path".

      ls [dir-path]/[instance]--[method]--[id].out--* | #{progname} -

    LICENSE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com

    man
    exit 0
  end 
end 
parser.parse!

unless options["if"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 

# =============================================================================
# MAIN PROGRAM 
# =============================================================================

# Read files from standard input or from file and filter invalid file names
files = if options["if"] == "-"
  STDIN.each_line(chomp: true).to_a
else 
  File.read(options["if"]).each_line(chomp: true).to_a 
end 
files = files.select{|fname| File.exists?(fname)}.sort!

# Get instance, method and filename  from files 
instances = {}
methods   = {}
ids       = {}

files.each do |fname|
  ext = File.extname(fname) 
  name = File.basename(fname,ext)
  name = File.basename(name,File.extname(name)) if ext == ".xz"

  inst,met,id = name.split("--")
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
end

inst = instances.values.first
met  = methods.values.first
id   = ids.values.first

if !(instances.size == 1) 
  raise "one instance is expected"
elsif !(methods.size == 1) 
  raise "one method is expected"
elsif !(ids.size == 1)
  raise "one experiment id is expected"
end 

# Create output files and  output directory if necessary  
FileUtils.mkdir_p(options["out-dir"]) unless Dir.exists?(options["out-dir"])
logfile = {}
metrics.each do |m|
  fname = File.join(options["out-dir"],"#{inst}--#{met}--#{id}--#{m}.merge")
  logfile[m] = File.open(fname,"w+")
end 

# Abrir cada archivo de entrada y concatenar en los archivos de salida 
# correspondientes cada valor

# Open each file, extract the values and concatenate to the corresponding 
# file 
#
files.each.with_index do |fname,fibnum|

  puts fname 
  file = if File.extname(fname) == ".xz" 
    %x(xz --force --decompress --keep #{fname})
    File.open(fname.sub(/\.[^.]+\z/,''))
  else 
    File.open(fname)
  end 

  file.each_line do |line|
    next unless line[/best-sol/]

    if val = line[/\(best-sol\s+(\(.+\))\)/,1]
      logfile["best-sol"].puts(val)
    end 

    if val  = line[/\(train-fit\s+(.+?)\)/,1]
      val = val.to_s.to_f 
      val = (val.abs < 1e-12) ? 0.0 : val 
      logfile["train-fit"].puts(val)
    end 

    if val = line[/\(val-fit\s+(.+?)\)/,1]
      val = val.to_s.to_f
      val = (val.abs < 1e-12) ? 0.0 : val 
      logfile["val-fit"].puts(val)
      logfile["sr"].puts((val < 1e-5) ? -1 : 0)
    end 

    if val = line[/\(size\s+(.+?)\)/,1]
      logfile["size"].puts(val)
    end 
  end
  file.close 
  File.delete(file.path) if File.extname(fname) == ".xz"

  logfile.each{|m,f| f.flush}
end

# Close all files 
logfile.each{|m,f| f.close}
