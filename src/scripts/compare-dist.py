#!/usr/bin/env python3
import sys
import scipy.stats as stats
import numpy as np 

def load_file(file_name,col = 0):
  ary = []

  file  = open(file_name,"r")
  for line in file:
    line = line.strip()
    if (not line.startswith("#")) and (line != "") :
      # val = float(line.split()[col])
      # val = val if val != 0 else 1e-40
      ary.append(round(float(line.split()[col]),12))

  return ary

def are_different_results(a_method,b_method,alpha=0.05,eps=1e-7):
    '''
    La función define si el método a tiene media inferior, igual o mayor que 
    el método b. 

    a_method : [num] -- arreglo 1-d con las mediciones del método a
    b_method : [num] -- arreglo 1-d con las mediciones del método b
    alpha    : float -- nivel de confianza (1 - alpha)% de que las medianas son 
                        distintas
    eps      : float -- valor de tolerancia para considerar dos números 
                        numéricamente iguales. 

    str -- regresa 'a' si las medianas a < b, 
           regresa '=' si las medianas a = b,
           regresa 'b' si las medianas a > b 
    '''

    # test de normalidad, el método trabaja bajo la hipótesis nula de que 
    # las mediciones provienen de una distribución normal, se rechaza la 
    # hipótesis nula (es decir que los datos no provienen de una distribución 
    # normal) si el p-valor es inferior al nivel de confianza (0.05). 
    val,a_pval = stats.shapiro(a_method)
    val,b_pval = stats.shapiro(b_method)

    # Como el p-valor es inferior al nivel de confianza, se rechaza la 
    # hipótesis de que los datos son normales, por lo que se realiza el test
    # no parametrico de kruskal-wallis. 
    if (a_pval <= alpha) or (b_pval <= alpha):
        # El test paramétrico de kruskal wallis trabaja bajo la hipótesis nula 
        # de que los datos tienen medianas iguales.
        val,p_val = stats.kruskal(methods[k],methods[l])
    
    # Como el p-valor de ambos es superior al nivel de confianza no se rechaza
    # la hipótesis y se considera que los datos son normales. 
    else :
        # test de varianzas, el método trabaja bajo la hipótesis nula de que 
        # los datos tienen la misma varianza.
        val,var_pval = stats.levene(methods[k],methods[l],center='mean')

        # el p-valor es inferior al nivel de confianza, se considera que los
        # datos tienen distintas varianzas. 
        if var_pval < alpha :
          # la prueba welch's t-test trabaja bajo la hipótesis nula de que las 
          # medias (igual a la mediana bajo normalidad) son iguales. 
          val,p_val = stats.ttest_ind(a_method,b_method,equal_var = False)

        # el p-valor es superior al nivel de confianza, se considera que los
        # datos tienen la misma varianza
        else : 
          # la prueba anova trabaja bajo la hipótesis nula de que las medias 
          # (igual a la mediana bajo normalidad) son iguales. 
          val,p_val = stats.f_oneway(a_method,b_method)
    
    # Si el p-valor es menor al intervalo de confianza, significa que las 
    # distribuciones tienen medias distintas, para considerar el error numérico 
    # además se pide que las medias sean diferentes con una tolerancia de 'eps'
    amed = np.median(a_method)
    bmed = np.median(b_method)
    diff = amed - bmed
    if (p_val < alpha) and (np.fabs(diff) > eps):
        if(diff < 0): 
            return "a" 
        else :
            return "b"
    else: 
        return "="


def statistics(data):
  median = np.median(data)
  avg = np.mean(data)
  mn  = np.amin(data)
  mx  = np.amax(data)
  std = np.std(data)
  return [mn,mx,avg,median,std]


if len(sys.argv) <= 2 : 
  print('''

  Este script se encarga de determinar si dos o más distribuciones 
  son estadísticamente diferentes. 

  Uso: 
    ./script a-met.txt b-met.txt ...

  donde cada [método].txt es un archivo que contiene por línea un 
  valor de la distribución. 

  Entrada 
    Cada archivo contiene los datos de una distribución, con un valor
    por línea. 
      [valor 1]
      [valor 2]
      ..
      [valor n]

  Salida: 
    
    Se imprime a 'stdout' cuatro tablas: 

    1. El resumen de los estadísticos de las distribuciones, la forma 
    de la tabla es: 

      ------------------------------
      método | min | max | mean | median | std
      ------------------------------
      a-met  | ... 
      b-met  | ...
      ...
      ------------------------------
    
    2. Si la fila es mejor  (>) que la columna
    3. Si la fila es peor   (<) que la fila 
    4. Si no hay diferencia (=)
    
    La forma de las tres tablas es: 

      ------------------------------
            | a-met | b-met | ... 
      ------------------------------
      a-met |       |       | ...
      a-met |       |       | ...
      ...   | ...   
      ------------------------------

  '''.strip())
  sys.exit(1)
  raise Exception("Se requieren al menos dos métodos")



# Cargar los archivos
methods = []
for file_name in sys.argv[1 : len(sys.argv)]:
  try:
    methods.append(load_file(file_name,0) )
  except:
    print("error in file: ",file=sys.stderr)
    print(file_name,file=sys.stderr)
    exit(-1)

# min_vl,mx_vl,mean,median,variance = 0,1,2,3,4
stats_tab  = [[0.0 for k in range(5)]  for k in methods]

# Comparison tables 
worse_tab  = [[0   for k in methods ]  for k in methods]
better_tab = [[0   for k in methods ]  for k in methods]
equal_tab  = [[0   for k in methods ]  for k in methods]




# Calcular las estadísticas de los métodos.
for k in range(len(methods)):
  stats_tab[k][0 : 5]  = statistics(methods[k])

  for l in range(k+1,len(methods)):

    better = are_different_results(methods[k],methods[l])
    
    if better == "=": 
        equal_tab[k][l] += 1
        equal_tab[l][k] += 1

    elif better == "a":  
        better_tab[k][l] += 1
        worse_tab[l][k]  += 1

    elif better == "b": 
        better_tab[l][k] += 1
        worse_tab[k][l]  += 1

print("# Valores estadísticos : min_val max_val median std")
for method in stats_tab:
    for col_val in method:
        print(col_val,end=" ")
    print("")

print("# Número de veces que fila ^ col ")
for method in better_tab:
    for col_val in method:
        print(col_val,end=" ")
    print("")

print("# Número de veces que fila v col")
for method in worse_tab:
    for col_val in method:
        print(col_val,end=" ")
    print("")

print("# Número de veces que fila == col")
for method in equal_tab:
    for col_val in method:
        print(col_val,end=" ")
    print("")
