require "../gp.cr"
require "option_parser"

# =============================================================================
# PARAMS
# =============================================================================

# Directorio 
out_dir = "pop"

# Métricas 
metrics = {
  "median-pair-train-fit-dist",
  "median-pair-val-fit-dist",
  "time",
  "iter-time",
  "avg-dcn-edit2",
  "avg-edit2",
  "avg-overlap",
  "avg-size",
  "avg-fit-dist",
  "avg-ssd",
  "median-fit-dist",
  "best-val-fit",
  "best-train-fit",
  "best-size",
  "uniq-elem",
  "max-mark-den",
  "mark-cnt",
  "pop-size", 
  "gens",
  "evals",
  "sr",
}

# Nombre del programa 
progname = File.basename(PROGRAM_NAME,File.extname(PROGRAM_NAME))

# =============================================================================
# HELP
# =============================================================================

parser = OptionParser.parse do |parser|
  parser.banner = <<-banner 
  Usage: 
    #{progname} [options]  FILE

  This program reads and calculates for each population measurement in 
  the given file various metrics.
  banner

  parser.on("-h","--help","shows this message") do 
    STDERR.puts parser
    exit(0)
  end 

  parser.on("--out-dir=DIR","output directory") do |dirname|
    out_dir = dirname
  end 

  parser.on("--man","a more detailed help") do 
    STDERR.puts <<-banner
    NAME
      #{progname.upcase}
    
    SYNOPSIS
      #{progname} [options] FILE

      INPUT
        File containing the population timestamps
      
      ARGUMENTS
        FILE           the file to open
        --out-dir      directory where files are placed
        -h, --help     help message
        --man            detailed help 

    DESCRIPTION
      The program opens the specified file, it expects that the file contains
      an associative list as an s-expression in each line. 
     
      Before any population entry a configuration s-expression is expected. 

      Then each population entry must contain the population as a list, an 
      example of the expected input is: 

      ((configuraton "v1.0") ....))
      ((generation 0) .... (population ((+ 1 2) (+ x0) 1 ... ) ...)
      ((generation 1) .... (population ((+ 1 2) (+ x0) 1 ... ) ...)
      ((generation 2) .... (population ((+ 1 2) (+ x0) 1 ... ) ...)
      (...)
      ((generation N) .... (population ((+ 1 2) (+ x0) 1 ... ) ...)
    
      The ouput is a file for each metric with the following path structure

      [out-dir]/[instance]--[method]--[id]--[metric].evo--[run-id]

    LICENSE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com

    banner
    exit 
  end
end 

unless ARGV.size > 0
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 

# =============================================================================
# PROGRAMA 
# ============================================================================

# Obtener el nombre del archivo 
fname  = ARGV.select{|e| File.exists?(e)}.first

# Obtener las instancias,métodos y etiqueta
name = File.basename(fname) 
ext = File.extname(fname) 
name = File.basename(fname,File.extname(fname)) if ext == ".xz" 
run = name.split("--").last
inst,met,id = File.basename(name,File.extname(name)).split("--")

# Crear los directorios en caso de que no existan
Dir.mkdir_p(out_dir) unless Dir.exists?(out_dir)

# Abrir los archivos a escribir
logfile  = {} of String => File
metrics.each do |m| 
  name = File.join(out_dir,"#{inst}--#{met}--#{id}--#{m}.evo--#{"%02d" % run}")
  logfile[m] = File.new(name,"w+")
end 

prev_time = 0
pop_str = [] of String
pop = [] of Indv
obj_fun = GP::ObjectiveFunction.mean_squared_error
env = Crisp::Env.new 
loaded_config = false

conf = ConfigParser.base(
  LogList.new("(
  (configuration v2.0)
  (run-name template) 
  (dataset-name name) 
  (alg-name dmdgp) 
  (vars x0) 
  (terms int) 
  (funs +)  
  (date \"2021-06-21 01:42:31 -05:00\") 
  (pop-size 10) 
  (off-size 10) 
  (max-size 1024) 
  (rand-tree-min-depth 1) 
  (rand-tree-max-depth 5) 
  (tournament-size 3) 
  (max-depth 13) 
  (mut-prob 0.2) 
  (cx-prob 0.9) 
  (infinity 1.0e+50) 
  (epsilon 1.0e-12) 
  (log-config true) 
  (log-pop true) 
  (log-traindata true) 
  (log-valdata true) 
  (log-evo true) 
  (log-stats true) 
  (xtrain (-10.0)) 
  (ytrain -19.5) 
  (xval (-10.0)) 
  (yval -19.5) 
  (max-evals 2000))"))

file = if File.extname(fname) == ".xz"
  %x(xz --force --decompress --keep #{fname}) 
  File.open(fname.sub(/\.[^.]+\z/,""))
else
  File.open(fname)
end 

# Abrir el archivo 
file.each_line do |line|

  # Cargar la configuración (xy-trai/val)
  if line[/configuration/]? 
    ll = LogList.new(line)
    conf = ConfigParser.base(ll)
    loaded_config = true

    # Environment 
    conf["funs"].each do |f| env.register_fun f end 
    conf["terms"].each do |t| env.register_term t end 
    conf["vars"].each do |v| env.register_input v end 

    next
  elsif !loaded_config 
    raise "cannot load config"
  elsif !(line[/\(\s*population\s+/]?)
    next 
  end 
  ll = LogList.new(line)

  # Get generation
  LogList.car?(LogList.cdr?(LogList.assoc?("generation",ll))).try do |gen|
    logfile["gens"].puts(gen.to_s.to_i32)
  end 

  # Obtener los tiempos de adquisición
  LogList.car?(LogList.cdr?(LogList.assoc?("time",ll))).try do |time|
    time  = time.to_s.to_f
    iter_time =  time - prev_time 
    prev_time = time

    logfile["time"].puts(time)
    logfile["iter-time"].puts(iter_time)
  end 

  # Obtener el número de evaluaciones
  LogList.car?(LogList.cdr?(LogList.assoc?("evaluations",ll))).try do |nevals| 
    logfile["evals"].puts(nevals.to_s)
  end 
  
  # Procesar la población
  LogList.cdr?(LogList.assoc?("population",ll)).try do |list|
    pop_str.clear
    while elem = LogList.car?(list)
      list = LogList.cdr?(list)
      pop_str.push(elem.to_s)
    end 

    pop.clear 
    pop_str.each do |s|
      indv = Indv.new(Crisp.read_str(s),env.clone)
      pop.push(indv)
    end

    # Tamaño de la población 
    logfile["pop-size"].puts(pop.size)

    # Distancia dcn promedio 
    # edit2
    edit2 = ->(a : Indv, b : Indv) do 
      Distance.edit2_normalized(a.sexp,b.sexp,0.5) 
    end 

    dist_array = Distance.dcn(pop,edit2,1e-7)
    d = dist_array.reduce(0.0){|acc,elem| acc + elem}
    d /= pop.size 
    logfile["avg-dcn-edit2"].puts(d)

    # Average edit2 distance 
    avg_edit2 = pop.map_with_index do |indv_a,i|
      avg = 0.0 
      pop.each_with_index do |indv_b,j|
        next if i==j
        avg += edit2.call(indv_a,indv_b)
      end 
      avg /= (pop.size-1)
      avg
    end 
    avg = avg_edit2.reduce(0.0){|acc,avgedit| acc + avgedit} / avg_edit2.size
    logfile["avg-edit2"].puts(avg)

    # Average overlap distance
    avg_overlap = pop.map_with_index do |indv_a,i|
      avg = 0.0 
      pop.each_with_index do |indv_b,j|
        next if i==j
        avg += Distance.overlap_normalized(indv_a.sexp,indv_b.sexp)
      end 
      avg /= (pop.size-1)
      avg
    end 
    avg = avg_overlap.reduce(0.0){|acc,avgoverlap| acc + avgoverlap} / avg_overlap.size
    logfile["avg-overlap"].puts(avg)

    # Size 
    avg = pop.reduce(0.0){|acc,elem| acc + elem.sexp.size} / pop.size
    logfile["avg-size"].puts(avg)

    # Evaluar la población  en entrenamiento 
    pop.each do  |indv| 
      indv.evaluate(conf.not_nil!["xtrain"], conf.not_nil!["ytrain"], conf.not_nil!["vars"],obj_fun, conf.not_nil!["infinity"])
    end
    pop_train_fit = pop.map do |i| i.fit end 
    pairs_fit_dists = get_pair_diffs(pop_train_fit)
    median = get_median(pairs_fit_dists) 
    logfile["median-pair-train-fit-dist"].puts(median)
     



    # Evaluar la población  en validación 
    pop.each do  |indv| 
      indv.evaluate(conf.not_nil!["xtrain"], conf.not_nil!["ytrain"], conf.not_nil!["vars"],obj_fun, conf.not_nil!["infinity"])
    end
    pop_val_fit   = pop.map do |i| i.fit end
 
    pairs_fit_dists = get_pair_diffs(pop_val_fit)
    median = get_median(pairs_fit_dists) 
    logfile["median-pair-val-fit-dist"].puts(median)


    # Calcular la mediana de la distancia de fitness
    plen = pop.size 
    mid = (plen - 1) // 2
    pop_fit = pop.map{|i| i.fit}
    median_per_indv = pop_fit.map_with_index do |fit,i|
      distances = pop_fit.map{ |f| (fit-f).abs }.sort 
      if plen.odd?
        distances[mid]
      else 
        (distances[mid] + distances[mid+1]) / 2
      end 
    end
    median_per_indv = median_per_indv.sort 
    median = if plen.odd?
      median_per_indv[mid]
    else
      (median_per_indv[mid]  + median_per_indv[mid+1]) / 2
    end
    logfile["median-fit-dist"].puts(median)

    
    pop_fit = pop.map{|i| i.fit}
    maxfit = pop_fit.max
    pop_fit = pop_fit.map!{|f| f / maxfit} 
    avg_fit = pop_fit.map_with_index do |fit,i|
      avg = 0.0
      pop_fit.each_with_index do |f,j|
        next if i == j 
        avg += (fit - f).abs
      end 
      avg /= (pop_fit.size-1)
      avg
    end 
    avg = avg_fit.reduce(0.0){|acc,avgfit| acc + avgfit} / avg_fit.size
    logfile["avg-fit-dist"].puts(avg)

    # Sampling Semantic Distance 
    pop_sem = pop.map{|i| i.semantic}
    avg_ssd = pop_sem.map_with_index do |sem,i|
      avg = 0.0
      pop_sem.each_with_index do |s,j|
        next if i==j
        ssd = (0...s.size).reduce(0.0){|acc,k| acc + (sem[k]-s[k]).abs} / s.size
        avg += ssd
      end 
      avg /= (pop_sem.size-1)
      avg
    end
    avg = avg_ssd.reduce(0.0){|acc,avgsem| acc + avgsem} / avg_ssd.size 
    logfile["avg-ssd"].puts(avg)

    # # Calcular la entropía de la población
    # h = Entropy.linear(pop.map(&.fit),conf.pop_size)
    # logfile["entropy"].puts(h);

    # Encontrar el mejor valor
    best = Selection.best_indv(pop)
    logfile["best-size"].puts(best.sexp.size)
    logfile["best-train-fit"].puts(best.fit)

    # Evaluar en el conjunto de prueba
    best.evaluate(conf.not_nil!["xval"], conf.not_nil!["yval"], conf.not_nil!["vars"],obj_fun, conf.not_nil!["infinity"])
    logfile["best-val-fit"].puts(best.fit)

    # Success Rate
    sr = (best.fit < 1e-5) ? 1 : 0
    logfile["sr"].puts(sr)

    # Número de individuos únicos
    logfile["uniq-elem"].puts(pop.uniq.size)

    # Densidad del marcador más frecuente con altura 5
    markercount = {} of String => Int32
    pop.each do |indv| 
      marker = Sexp.genetic_marker(indv.sexp,0,5)
      markercount[marker] = (markercount[marker]? || 0) + 1
    end 
    logfile["mark-cnt"].puts(markercount.size)
    logfile["max-mark-den"].puts(markercount.values.max / pop.size)

    # Flush
    logfile.each{|k,file| file.flush()} 
  end 
end
file.close 
File.delete(file.path) if File.extname(fname) == ".xz"

# Escribir un archivo con los valores de la evaluación de cada generación
logfile.each{|k,file| file.close} 

def get_median(arr)
  arr = arr.sort
  mid = (arr.size - 1) // 2
  median = if arr.size.odd? 
    arr[mid]
  else 
   (arr[mid] + arr[mid+1]) / 2.0
  end
  return median 
end 

def get_pair_diffs(arr)
  pair_diffs = [] of Float64
  (0 ... arr.size).each do |i|
    (i+1 ... arr.size).each do |j| 
      dfit = (arr[i] - arr[j]).abs
      pair_diffs.push(dfit)
    end
  end
  pair_diffs.push(0) if pair_diffs.empty? 
  return pair_diffs
end 
