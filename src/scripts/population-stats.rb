#!/usr/bin/env ruby 

require "optparse"
require "fileutils"

# =============================================================================
# PARAMETERS 
# =============================================================================

stats = ["mean","median","var","ptp","min","max"]

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))
opts = {} 
opts["out-dir"] = "proc/pop-stats"
opts["if"]=""

# =============================================================================
# ARGUMENTS 
# =============================================================================

parser = OptionParser.new do |parser|

  parser.banner = <<-banner 
  Usage: #{progname} [options] --if=FILE

  This program reads from the specified the filenames to process and calculates the 
  statistics of each column of the file.

  banner

  parser.on("--if=FILE","input file")  do |file|
    opts["if"] = file
  end 
  
  parser.on("--out-dir=DIR","output directory")  do |dirname|
    opts["out-dir"] = dirname 
  end 

  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end 

  parser.on("--man", " amore detailed help") do 
    puts <<-man
    NAME 
      #{progname.upcase}
    
    SYNOPSIS
      #{progname} --if=[file]
      
      ARGUMENTS
      
      --if=FILE    input file name
      --h, --help  help message
      --man          a more detailed help 
  
    DESCRIPTION
      The program reads from FILE the data, it is expected that each row is the
      measure data from one independent execution. And each column is contains
      the data in a "timestamp" for the different executions. 

      |1 2 3   ...  #file1|
      |0.7 0.3 .... #file2|
      | ...               |

      the expectred filename format is: 

      [instance]--[method]--[id]--[metric].merged
    
      The program writes a file for each statistics, in the following path: 

      [out-dir]/[filename].[statistic]

    EXAMPLES
      # Process a file 

      #{progname} --if=[dir-path]/[instance]--[method]--[id]--[metric].merged
      
    LICENSE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com

    man
    exit 0
  end 
end 
parser.parse!

if opts["if"].empty?
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end

# =============================================================================
# PROGRAM
# =============================================================================

# Get instance, method, id and metric 
inst,met,id,mtr = File.basename(opts["if"],File.extname(opts["if"])).split("--")

# Create the output directory 
FileUtils.mkdir_p(opts["out-dir"]) if !Dir.exists?(opts["out-dir"])


file = File.open(opts["if"]) do |file|
  # Array with the data 
  data = []

  # Read the executions
  file.each_line do |line|
    fdata = line.split(" ").map{|e| e.to_f}
    data.push(fdata)
  end

  # Open the statistics files 
  logfile = {}
  stats.each do |s|
    logfile[s] = File.open(File.join(opts["out-dir"],"#{inst}--#{met}--#{id}--#{mtr}.#{s}"),"w+")
  end 
  
  # Each column 
  data.first.size.times do |c|

    # Independent executions at c-th time (bin)
    vals = []

    data.size.times do |r|
      if data[r] && data[r][c]
        vals.push(data[r][c]) 
      else 
        STDERR.puts "WARNING: incomplete data: row:#{r} c:#{c} in #{inst}--#{met}"
      end 
    end 

    if vals.size < 2
      STDERR.puts "WARNING: no-data in #{inst}--#{met}"
      break
    end 

    # sort measures 
    vals.sort!

    # Calculate median 
    lower = vals[(vals.size / 2).floor.to_i] 
    upper = vals[(vals.size / 2).ceil.to_i] 
    median = (lower + upper) / 2 
    # Calculate mean 
    mean   = vals.reduce(0.0){|acc,val| acc + val} / vals.size
    # Calculate variance 
    variance = vals.reduce(0.0){|acc,val| acc + ((val - mean)**2.0)} / (vals.size-1)
    # Get min and max values 
    min = vals[0]
    max = vals[-1]
    # Peak to Peak 
    ptp = max-min

    # Write 
    logfile["mean"].puts(mean.clamp(1e-10,1e40))
    logfile["median"].puts(median.clamp(1e-10,1e40))
    logfile["var"].puts(variance.clamp(1e-10,1e40))
    logfile["ptp"].puts(ptp.clamp(1e-10,1e40))
    logfile["min"].puts(min.clamp(1e-10,1e40))
    logfile["max"].puts(max.clamp(1e-10,1e40))

    logfile.each{|s,f| f.flush}
  end

  # Save files 
  logfile.each{|s,f| f.close}
end
