require "../gp.cr"
require "option_parser"

# =============================================================================
# PARAMS
# =============================================================================

# Directorio 
out_dir = "gpdmd-analysis"

conf =  Config.parse 

conf.pop_init  = Init.new("rhh",conf)
conf.selection = Select.new("tournament",conf)
conf.crossover = Cross.new("subtree-exchange",conf)
conf.mutation  = Mut.new("subtree-replacement",conf)

distance_decrement_fun = ->() do
  conf.req_dist - ((conf.req_dist / (0.9*conf.max_evals))*conf.num_evals)
end


conf.replacement = Replace.new("rmddc",
  ->(pop : Array(Indv), off : Array(Indv)) do 
    req_dist = distance_decrement_fun.call
 
    candidates = (pop + off).uniq
    best = Select.best_indv(candidates)
    index = candidates.index(best).not_nil!
    candidates.swap(index,-1).pop

    new_pop = [] of Indv 
    new_pop.push(best)
    dcn = Dist.dcn(candidates,new_pop,conf.dist_fun,conf.epsilon)
    pfit = Array.new(dcn.size){conf.infinity}

    pen_freq = Array.new(conf.pop_size-1,0.0)
    selection_size = Array.new(conf.pop_size-1,0)
    selection_dcn  = Array.new(conf.pop_size-1,0.0)

    while new_pop.size < conf.pop_size && candidates.size > 0
      candidates.each_with_index do |indv,i|
        d = conf.dist_fun.call(indv.sexp,new_pop[-1].sexp)
        dcn[i] = d if (d - dcn[i]) < -conf.epsilon
      end 
      penalized = [] of Tuple(Indv,Float64)
      non_penalized = [] of Indv 
      candidates.each_with_index do |indv,index|
        if ((dcn[index] - req_dist) > -conf.epsilon)
          non_penalized.push(indv)
        else 
          penalized.push({indv,dcn[index]})
        end 
      end
      unless non_penalized.empty? 
        objectives = (0 ... non_penalized.size).map do |i|
          {non_penalized[i],{non_penalized[i].fit,candidates[i].sexp.size}}
        end 
        non_dom = MultObj.nondom(objectives,conf.epsilon) do |_,_| {false,false} end  
        selected = non_dom.sample 
      else 
        selected = (penalized.max_by do |(indv,d)| d end).first
      end 

      pen_freq[new_pop.size-1] = penalized.size.to_f / candidates.size 
      selection_size[new_pop.size-1] = selected.sexp.size   
      selection_dcn[new_pop.size-1] = Dist.dcn([selected],new_pop,conf.dist_fun,conf.epsilon)[0]
     
      # delete the selected object 
      index = candidates.index(selected).not_nil!
      candidates.swap(index,-1).pop
      dcn.swap(index,-1).pop

      # add the selected indv to the new population 
      new_pop.push(selected)
    end 

    print(conf.num_evals," ",pen_freq.join(" "),"\n")
    print(conf.num_evals," ",selection_size.join(" "),"\n")
    print(conf.num_evals," ",selection_dcn.join(" "),"\n")
    new_pop
  end) 


pop = conf.pop_init.call(conf.pop_size)
pop.each do |indv| 
  indv.eval(conf.xtrain,conf)
  indv.fit = conf.obj_fun.call(indv.yhats,conf.ytrain)
end 
best = Select.best_indv(pop).clone
conf.timer.tic

until conf.stop_criteria.call() 
  parents = conf.selection.call(pop)
  off = conf.crossover.call(parents,pop)
  off = conf.mutation.call(off,pop)
  pop = conf.replacement.call(pop,off)
  genbest = Select.best_indv(pop).clone
  best = Select.best_indv([best,genbest])
  conf.gen += 1
end 

