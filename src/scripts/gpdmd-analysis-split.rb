#!/usr/bin/env ruby 

require "optparse"
require "fileutils"

metrics = [ 
  "size",
  "penratio",
  "dcn",
] 

progname = File.basename($PROGRAM_NAME, File.extname($PROGRAM_NAME))
options = {}
options["out-dir"] = "proc/pop-analysis"

parser = OptionParser.new do |parser|
  parser.banner = <<-banner
  Usage: 
    ls -d [dir-path]/[instance]--*--[id].pr-sz-dcn--* | #{progname} --if=-
  
  This program reads from FILE the fulenames to process and extracts from them 
  the penalization ratio, size and dcn from each measurement in the file
  banner

  parser.on("--if=FILE","File to read") do |fname|
    options["if"] = fname
  end 

  parser.on("--out-dir=DIR","Output directory")  do |dirname|
    options["out-dir"] = dirname  
  end 

  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end

  parser.on("--man", " a more detailed help") do 
    puts <<-man 
    NAME
      #{progname.upcase} 
    SYNOPSIS
      ls [files] | #{progname} --if=-
      
      #{parser.summarize("",20,80,"    ")}
    
    DESCRIPTION
      
      The program reads from STDIN the filenames to process. Each file is 
      the ouput of a gpdmd analysis in a problem. 

      the expected file format is a pop_size+1 columns and 3*generation 
      lines.  At each generation 3 lines are expected in the following 
      order: 

      1. penalization ratio
      2. size 
      3. dcn 

      Each line has pop_size+1 columns, the first column is the number of 
      evaluations and the following values are the measurements for the 
      additions of elements to the new population in order

      nevals first-addition second-addition .... 

      The output is a file per metric and an aditional file for 
      the evaluations of pop_size, where each line is are the measurements

      a0 a1 a2 ...
      a0 a1 a2 ...

    EXAMPLES 
      ls -d [dir-path]/[instance]--*--[id].pr-sz-dcn--* | #{progname} --if=-

    LISCENCE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com
      
    man
    exit 0
  end 
end 
parser.parse!

unless options["if"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 


# =============================================================================
# MAIN PROGRAM 
# =============================================================================

# Read files from standard input or from file and filter invalid file names
files = if options["if"] == "-"
  STDIN.each_line(chomp: true).to_a
else 
  File.read(options["if"]).each_line(chomp: true).to_a 
end 
files = files.select{|fname| File.exists?(fname)}.sort!

# Create output files and  output directory if necessary  
FileUtils.mkdir_p(options["out-dir"]) unless Dir.exists?(options["out-dir"])

files.each do |fname| 
  name = File.basename(fname)
  ext = File.extname(fname) 
  name = File.basename(name,File.extname(name)) if ext == ".xz"
  run = name.split("--").last.to_i 
  inst,met,id = name.split("--")

  file = if File.extname(fname) == ".xz"
    %x(xz --force --decompress --keep #{fname}) 
    File.open(fname.sub(/\.[^.]+\z/,""),"r")
  else 
    File.open(fname,"r") 
  end 

  logfile = {}
  metrics.each do |m|
    mfname = File.join(options["out-dir"],"#{inst}--#{met}--#{id}--#{m}.evo--#{"%02d" % run}")
    logfile[m] = File.open(mfname,"w+")
  end

  file.each_line.each_slice(3) do |line|
    logfile["penratio"].puts(line[0])
    logfile["size"].puts(line[1])
    logfile["dcn"].puts(line[2])
  end 

  file.close
  File.delete(file.path) if File.extname(fname) == ".xz"
  logfile.each do |k,f| f.close end 
end
