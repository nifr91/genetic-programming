#!/usr/bin/env ruby 
require "optparse"

# =============================================================================
# ARGUMENTS PARSING 
# =============================================================================

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME)) 
opts = {} 
opts["comp-dist-script"] = File.join("bin","compare-dist.py") 

parser = OptionParser.new do |parser|
  parser.banner = <<~banner
  Usage: ls [path]/[instance]--*--[id]--[metric].merge | #{progname} [options] --if=-

  This program reads from STDIN the filenames to process and uses the 
  script #{opts["comp-dist-script"]} to compare for each instance 
  for different methods. 

  It is expected that the filenames are in the form: 

  [instance]--[method]--[id]--[metric].merge  

  banner

  parser.on("--if=FILE", "input file") do |fname|
    opts["if"] = fname
  end 

  parser.on("-h","--help","shows a short help") do 
    puts parser
    exit(0)
  end 
end 
parser.parse!

# =============================================================================
# MAIN PROGRAM 
# =============================================================================

# Read files from standard input or from file and filter invalid file names
files = if opts["if"] == "-"
  STDIN.each_line(chomp: true).to_a
else 
  File.read(options["if"]).each_line(chomp: true).to_a 
end 
files = files.select{|fname| File.exists?(fname)}.sort!


# Get instances and methods, then check for only one id and one metric to be 
# processed

instances = {} 
methods = {} 
ids = {} 
metrics = {} 

files.each do |fname| 
  name = File.basename(fname,File.extname(fname))
  inst,met,id,mtr = name.split("--") 
  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
end 

if !(ids.size == 1) 
  raise "one experiment id is expected"
elsif !(metrics.size == 1)
  raise "one metric is expected" 
end 

instances = instances.values.sort
methods   = methods.values.sort
id        = ids.values.first
mtr       = metrics.values.first


# Array for medians (the best is going to be colored red)
medians_str = []
# Array for stats 
stats_str =  []
# Counter for each method of number of times it was the best or there was no 
# statistically significant difference 
times_met_was_eq_to_best = Array.new(methods.size,0)
# Counter for best equal end worse 
cnt = Array.new(methods.size) do 
  Array.new(methods.size){ [0,0,0] } 
end 

# Configure the asciidoc 
puts "= Method pairwise comparison #{mtr}" 
puts <<~conf
:prewrap!:
:source-highlighter: rouge
:stem: latexmath
:toc:

conf

instances.each do |inst|

  # Header for asciidoc 
  puts "== #{inst}"

  # Generate a string of the methods 
  methods_str = files
    .select{|f| f[/#{inst}--/] && f[/\.merge$/]}
    .sort!
    .join(" ")
 
  # Get the pairwise comparison with the statistics script, it returns the 
  # stats 
  # min,max,mean,median,std
  # and tree tables of methods.size x methods.size for 
  # better, worse, equal  each element indicates if the row is 
  # better/worse/equal than the column (1) or not (0)
  #
  mats_lines = `#{opts["comp-dist-script"]} #{methods_str}`
    .each_line
    .reject{|e| e =~ /^\s*#.*$/}
    .map{|e| e.strip}
    .to_a 

  # parse stats and the 3 matrices
  nmet = methods.size; 
  stats = mats_lines[0*nmet...1*nmet].map{|e| e.split(" ").map{|e| e.to_f}}
  stats_str.push(stats.map{|e| e.map{|e| "%0.5f" % e}})

  b = mats_lines[1*nmet...2*nmet].map{|e| e.split(" ").map{|e| e.to_f.to_i}}
  w = mats_lines[2*nmet...3*nmet].map{|e| e.split(" ").map{|e| e.to_f.to_i}}
  e = mats_lines[3*nmet...4*nmet].map{|e| e.split(" ").map{|e| e.to_f.to_i}}

  nmet.times do |r|
    nmet.times do |c|
      tb,te,tw = cnt[r][c]; 
      cnt[r][c] = [b[r][c]+tb, e[r][c]+te, w[r][c]+tw]
    end 
  end 


  # find the method with the best median 
  median = stats.map{|e| e[3]} 
  best_median_method = 0
  best = median[0]
  (1 ... median.size).each do |i| 
    if best > median[i] 
      best = median[i] 
      best_median_method = i;
    end 
  end 

  # Color the median of methods without statistically significant difference to
  # best
  str =  [] 
  (0 ... median.size).each do |i|
    m = "%0.5g" % median[i]
    if (i == best_median_method) || (e[i][best_median_method] > 0)
      times_met_was_eq_to_best[i] += 1
      m = "[red]*#{m}*"
    end 
    str.push(m.to_s)
  end 
  medians_str.push(str)

  # Print the raw stats and medians 
  pad = methods.map{|e| e.size}.max + 2
  mstr = methods.map{|e| e.ljust(pad,' ')} 

  puts <<~output
  .#{inst}
  [options="nowrap"]
  ----
  # method min max median std
  #{stats
    .map.with_index{|e,i| 
    ([mstr[i]] + e.map{|ee| ("%0.8f" % ee).rjust(10,' ')}).join(" ")}.join("\n")} 
  
  # row ^ col 
  #{b.map{|e| e.join(" ")}.join("\n")}

  # row v col 
  #{w.map{|e| e.join(" ")}.join("\n")}

  # row == col
  #{e.map{|e| e.join(" ")}.join("\n")}
  ----

  output
end 


# Print colored medians 
puts <<~output
== Methods medians per instance 

.Median of each method in [red]*red* the statistically best median
[width="90%"]
[.center]
|===
|#{"|"+methods.join("|")}
#{medians_str.map.with_index{|e,i| "|"+([instances[i]] + e).join("|")}.join("\n")}
|=== 
output


# Print pairwise comparison 
won_loss_diff = cnt.map{|row| row.reduce(0){|acc,tup| acc + (tup[0] - tup[-1])  } }
puts <<-output
== Pairwise method comparison

.Comparación de medias entre métodos (^ mejor, = igual , v peor )
[width="90%"]
[cols="<,#{(["^."]*methods.size).join(",")},^."]
[.center]
|===
|#{"|"+methods.join("|") + " 1.2+^.^|" +  "stem:[\\sum\\Delta_{\\land\\lor} ]" }
|#{"|"+(["^^:==:vv"]*methods.size).join("|") }
#{cnt.map.with_index{|row,r|
    "|"+methods[r] + "|" + 
    row.map.with_index{|tup,c|
      if c == r 
        ["  ","--","  "].join(" ")
      else 
        tup.map{|e| e.to_s.rjust(2,'0')}.join(":")
      end 
}.join("|") + "|" + won_loss_diff[r].to_s }
    .join("\n")}
|===
output

# Print number of times a method was best 
puts <<-output
== Statistically best

.Number of instances that algorithm had no statistical significant difference with best median
[width="90%"]
[.center]
[cols="#{(["^"]*methods.size).join(",")}"]
|===
|#{methods.join("|")}
|#{times_met_was_eq_to_best.join("|")}
|===
output


# Sort methods by rank (difference of best and worse) 
sindex = (0 ... methods.size).to_a.sort_by{|i| won_loss_diff[i]} 
won_loss_diff = sindex.map{|si| won_loss_diff[si]} 
methods = sindex.map{|si| methods[si]}
cnt = sindex.map{|si| sindex.map{|sj| cnt[si][sj]}}
times_met_was_eq_to_best = sindex.map{|si| times_met_was_eq_to_best[si]}

# Print sorted by rank pairwise comparison 
puts <<-output
== Condensed rank sorted pairwise comparison 

.Comparación de medias entre métodos (mejor - peor)
[width="90%"]
[.center]
[cols="<,#{(["^."]*methods.size).join(",")},^."]
|===

|#{"|"+methods.join("|") + " 1.2+^.^|" +  "stem:[\\sum\\Delta_{\\land\\lor}]" }
|#{"|"+(["^^:vv"]*methods.size).join("|")  }
#{cnt.map.with_index{|row,r|
    "|"+methods[r] + "|" + 
    row.map.with_index{|tup,c|
      if c == r 
        "--"
      else 
        b,e,w = tup
        [b,w].map{|e| e.to_s.rjust(2,'0')}.join(":")
      end 
}.join("|") + "|" + won_loss_diff[r].to_s }
    .join("\n")}
|===
output

# Tables in latex format ------------------------------------------------------

puts "== latex"

puts <<~output
.Statistically best
[source,latex]
--
\\begin{table}
\\centering
\\label{}
\\caption{#{mtr} número de veces que fue mejor}
\\begin{adjustbox}{max width=\\linewidth}
\\begin{tabular}{l*{100}{c}}
\\hline\\noalign{\\smallskip}
#{methods.join(" & ")} \\\\
#{times_met_was_eq_to_best.join(" & ")}\\\\
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
\\noalign{\\smallskip}\\hline
\\end{tabular}
\\end{adjustbox}
\\end{table}
--
output



puts <<~output
.Pairwise comparison
[source,latex]
--
\\begin{table}
\\centering
\\label{}
\\caption{#{mtr}}
\\begin{adjustbox}{max width=\\linewidth}
\\begin{tabular}{l*{100}{c}}
\\hline\\noalign{\\smallskip}
#{" & "+methods.map{|m| "\\multicolumn{2}{c}{#{m}}"}.join(" & ") + 
  " & " + "\\multirow{2}{*}{$\\sum\\Delta\\updownarrows$}"} \\\\
#{" & "+(["$\\uparrow$ & $\\downarrow$"]*methods.size).join(" & ") + " & " + " "} \\\\
\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}
#{cnt.map.with_index{|row,r|
    methods[r] + "&" + 
    row.map.with_index{|tup,c|
      if c == r
        "\\multicolumn{2}{c}{---}"
      else 
        b,e,w = tup
        [b,w].map{|e| e.to_s.rjust(2,' ')}.join("&")
      end 
}.join("& ") + "&" + won_loss_diff[r].to_s }
    .join("\\\\\n")} \\\\
\\noalign{\\smallskip}\\hline
\\end{tabular}
\\end{adjustbox}
\\end{table}
--
output


puts <<~output
.Medians 
[source,latex]
--
\\begin{table}
\\centering
\\label{}
\\caption{#{mtr} median}
\\begin{adjustbox}{max width=\\linewidth}
\\begin{tabular}{l*{100}{c}}
\\hline\\noalign{\\smallskip}

instance & min & max & mean & median & $\\sigma$ \\\\

\\noalign{\\smallskip}\\hline\\noalign{\\smallskip}

#{stats_str.map.with_index{|e,i|
    instances[i] + " & " +e.map.with_index{|e,j| 
      (methods[j] =~ /rmddc|dmdgp/) ? e.join(" & ") : ""
    }.select{|e| !e.empty?}.join(" & ") 
    }.join("\\\\\n") }\\\\

\\noalign{\\smallskip}\\hline
\\end{tabular}
\\end{adjustbox}
\\end{table}

--
output


