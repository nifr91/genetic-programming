#!/usr/bin/env ruby 
require "optparse"
require "fileutils"
# ============================================================================
# ARGUMENTS PARSING 
# ============================================================================

progname = File.basename($PROGRAM_NAME,File.extname($PROGRAM_NAME))

opts              = {}
opts["nbins"]     = 256
opts["rngmetric"] = "gens"
opts["rngbegin"]  = 0.0
opts["rngend"]    = 256.0
opts["if"]        = "" 
opts["out-dir"]   = "proc/pop-merged"

parser = OptionParser.new do |parser|
  parser.banner = <<-banner 
  Usage: #{progname} [options] -

  This program reads from STDIN the filenames to process and merges among
  measurements.
  banner

  parser.on("--if=FILE","flag to read from stdin")  do |file|
    opts["if"] = file
  end 

  parser.on("--nbins=INT","number of segmentations of range") do |num|
    opts["nbins"] = num.to_i
  end 

  parser.on("--begin=FLOAT","initial value of range") do |num|
    opts["rngbegin"] = num.to_f
  end 

  parser.on("--end=FLOAT","last value of range") do |num|
    opts["rngend"] = num.to_f
  end 

  parser.on("--rng-metric=STR","metric used for the range") do |str|
    opts["rngmetric"] = str 
  end 

  parser.on("--out-dir=DIR","output directory")  do |dirname|
    opts["out-dir"] = dirname
  end 

  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end 


  parser.on("--man", "a more detailed help") do 
    puts <<-banner
    NAME 
      #{progname.upcase}

    SYNOPSIS 
      ls [files] | #{progname} --nbins 252 --begin 0 --end 252

      INPUT
        List of files to process (a pair of measure and range is expected for
        each independent execution). Each pair corresponds of a parallel 
        execution for the same problem, method and metric. 

        [dir-path]/[instance]--[method]--[id]--[metric].evo--[run-id]
        [dir-path]/[instance]--[method]--[id]--[range-metric].evo--[run-id]
      
      ARGUMENTS
        files 
        --nbins    INT   number of segmentation of range
        --begin    NUM   initial value of range
        --end      NUM   last value of range
        -h,--help        help message
        --man              detailed help 

    DESCRIPTION 
      The program merges the values of parallel executions for a measured 
      value of the same experiment. 

      It opens the specified files pair, it expects that each file 
      contains one line per measurement and each line in the first file 
      corresponds to the same line in the second file.
      
      |#metric file 1| |#range file 1| |#metric file 2 | |#range file 2  |...
      |1             | |0.3          | |4              | |0.1            |...
      |2             | |0.4          | |3              | |0.3            |...
      |...           | |...          | |...            | |...            |...

  
      For each pair a histogram with the specified (start .. end) inclusive
      range is generated. The measurements in the same bin are averaged and
      when no measurement in the i-bin is found, the previous (i-1)-bin is
      used.

      The output of the program is a file with the following path structure

      [out-dir]/[metric]/[instance]--[method]--[id]--[metric].merged
      
      each ith line corresponds to the histogram of the ith file. 
      
      
      For example the files 

      # File 1
      bin         [     ][1,2  ][5    ][        ] 
      bin range   [0.00 )[0.25 )[0.50 )[0.75 1.0]
     
      # File 2
      bin         [4    ][3    ][9    ][10      ] 
      bin range   [0.00 )[0.25 )[0.50 )[0.75 1.0]
      
      ... 

      generates the output file 

      |# Merge file
      |0.0 1.5 5.0 5.0
      |4.0 3.0 9.0 10.0
      |...
      
    LICENSE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com
    banner
    exit
  end 
end 
parser.parse!

if opts["if"].empty?
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 

rng = (opts["rngbegin"] .. opts["rngend"])

if opts["rngmetric"].empty? 
  STDERR.puts "ERROR missing name of metric to use for histogram range"
  STDERR.puts parser
  exit(1)
end 

# =============================================================================
# PROGRAM
# =============================================================================

# Get files to merge from FILE 
files = case opts["if"]
  when "-" then STDIN.each_line.map{|e| e.strip}.select{|e| File.exists?(e)}.to_a
  else File.read(infile).each_line(chomp: true).to_a
  end

# Split files into metric and range 
metric_files = files.reject{|e| e =~ /--#{opts["rngmetric"]}\.evo/}
rng_files = files.select{|e| e =~ /--#{opts["rngmetric"]}\.evo/}
rng_files = rng_files.map do |e|
  run_id = File.extname(File.basename(e)).split("--").last
  [run_id,e]
end 
rng_files = rng_files.to_h

if metric_files.empty?
  STDERR.puts opts["if"]
  STDERR.puts files[0..2].inspect
  STDERR.puts rng_files[0..2].inspect
  STDERR.puts "ERROR no files for metrics"
  exit(1)
end 

if rng_files.empty?
  STDERR.puts "ERROR no files for range"
  exit(1)
end 

# Check if there are as many metric files as range 
if metric_files.size < rng_files.size 
  STDERR.puts "ERROR mismatch in metric and range file pairs"
  STDERR.puts "metric_files: #{metric_files.size}"
  STDERR.puts "rng_files: #{rng_files.size}"
  exit(1)
end 

# Get id instance methods  and metric 
instances = {}
methods   = {}
ids       = {}
metrics   = {}

metric_files.each do |fname|
  name = File.basename(fname)
  name = File.basename(name,File.extname(name))
  inst,met,id,mtr = name.split("--")

  instances[inst] = inst
  methods[met] = met
  ids[id] = id
  metrics[mtr] = mtr
end
inst = instances.values.first
met  = methods.values.first
id   = ids.values.first
mtr  = metrics.values.first

if !(instances.size == 1)
  raise "more than one instance #{instances.values[0..1].join(" ")}"
elsif !(methods.size == 1)
  raise "more than one method"
elsif !(ids.size == 1)
  raise "more than one experiment id"
elsif !(metrics.size == 1)
  raise "more than one metric"
end 

# Create output directory
FileUtils.mkdir_p(opts["out-dir"]) unless Dir.exists?(opts["out-dir"])

# Create histogram 
h = Array.new(opts["nbins"]){ [0.0,0] }
binstep = (rng.end - rng.begin) / opts["nbins"]
hrngs   = Array.new(h.size){|i| (i+1)*binstep}


# Write concatenated file 
File.open(File.join(opts["out-dir"],"#{inst}--#{met}--#{id}--#{mtr}.merge"),"w+") do |file|
  metric_files.each do |fname|
    # Get independent execution number 
    run_id = File.extname(File.basename(fname)).split("--").last
    # Extract measures 
    data = File.readlines(fname).map{|e| e.to_f}.to_a

    if data.empty? 
      STDERR.puts "WARNING: empty file #{fname}"
      next
    end 

    # Get ranges for measure
    time = File.readlines(rng_files[run_id]).map{|e| e.to_f}.to_a

    # Create pairs [range,data] 
    pnts = time.zip(data)

    # Fill Histogram 
    (h.size).times{|i| h[i] = [0.0,0] } 
    pnts.each do |t,d|
      if b = hrngs.bsearch_index{|r| t < r}
        h[b][0] += d
        h[b][1] += 1
      end 
    end 
  
    # Average the measure in the same bin 
    h = h.map do |d,cnt| [d / [cnt,1].max,cnt] end  
 
    # Fill begining zeros 
    first_data_index = 0
    (0...h.size).each do |i| 
      if h[i].last != 0
        first_data_index = i
        break 
      end 
    end 
    (0 ... first_data_index).reverse_each do |i|
      h[i] = h[i+1].dup
    end 

    # Fill zeros 
    (1 ... h.size).each do |i|
      h[i] = h[i-1].dup if h[i].last == 0 
    end 

    # Write histogram into file 
    file.puts(h.map{|e| e.first}.join(" "))
    file.flush
  end 
end 




