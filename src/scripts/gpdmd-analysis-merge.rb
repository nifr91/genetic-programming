#!/usr/bin/env ruby 

require "optparse"
require "fileutils"

progname = File.basename($PROGRAM_NAME, File.extname($PROGRAM_NAME))
options = {}
options["out-dir"] = "proc/pop-analysis-merged"
options["nbins"] = 252

parser = OptionParser.new do |parser|
  parser.banner = <<-banner
  Usage: 
    ls -d [files] | #{progname} --if=- --nbins=252
  
  This program reads from FILE the filenames to process and  merges the 
  independent executions into a single file 
  banner

  parser.on("--if=FILE","File to read") do |fname|
    options["if"] = fname
  end 

  parser.on("--nbins=INT","number of segmentations of range") do |num|
    opts["nbins"] = num.to_i
  end 

  parser.on("--out-dir=DIR","Output directory")  do |dirname|
    options["out-dir"] = dirname  
  end 

  parser.on("-h","--help","shows this message") do 
    puts parser
    exit(0)
  end

  parser.on("--man", " a more detailed help") do 
    puts <<-man 
    NAME
      #{progname.upcase} 
    SYNOPSIS
      ls [files] | #{progname} --if=- --nbins=252
      
      #{parser.summarize("",20,80,"    ")}
    
    DESCRIPTION
     
      The program merges the values of parallel executions for 
      independent executions.  The program reads from STDIN the filenames to 
      process.

      INPUT 

        The expected file format is a pop_size+1 columns measurements lines.  
        At each generatio the line line has pop_size+1 columns, the first column 
        is the number of evaluations and the following values are the 
        measurements for the additions of elements to the new population in order

        nevals first-addition second-addition .... 

      OUTPUT 
        The output file is a file with the x y z triplet of the merged measure
        [nevals, new_pop_size, measurement]

        t00 t01 t02
        t10 t12 t12
        ... 

    EXAMPLES 
      ls -d [dir-path]/[instance]--*--[id].pr-sz-dcn--* | #{progname} --if=-

    LISCENCE
      MIT

    AUTHOR
      Ricardo Nieto Fuentes
      nifr91@gmail.com
      
    man
    exit 0
  end 
end 
parser.parse!

unless options["if"]
  STDERR.puts "ERROR missing required argument"
  STDERR.puts parser
  exit(1)
end 


# =============================================================================
# MAIN PROGRAM 
# =============================================================================

# Read files from standard input or from file and filter invalid file names
files = if options["if"] == "-"
  STDIN.each_line(chomp: true).to_a
else 
  File.read(options["if"]).each_line(chomp: true).to_a 
end 
files = files.select{|fname| File.exists?(fname)}.sort!


# Get id instance methods  and metric 
instances = {}
methods   = {}
ids       = {}
metrics   = {}


# Get range 
rngbegin = 1/0.0
rngend   = -1/0.0
pop_size = 0 
files.each do |fname| 
  lines = File.readlines(fname) 
  pop_size = lines[0].split(" ").size - 1
  rngbegin = [rngbegin, lines[0].split(" ").first.to_f].min 
  rngend   = [rngend, lines[-1].split(" ").first.to_f].max 
end 
rng = (rngbegin .. rngend)

# Create histogram
# h = Array.new(opts["nbins"]){[[0.0]*(pop_size+1)] }
# binstep = (rng.end - rng.begin) / opts["nbins"]
# hrngs   = Array.new(h.size){|i| (i+1)*binstep}

# Write concatenated file 

files.each do |fname|
  lines = File.readlines(fname) 
  lines.each do |line|
    nevals, *data = line.split(" ")
    (0 ... data.size).each do |i|
      print nevals," ",i," ",data[i],"\n"
    end 
  end 
end 
  

