#!/usr/bin/env ruby 

require "optionparser" 

VERSION = 0.1
AUTHOR  = "Ricardo Nieto Fuentes"
EMAIL   = "nifr91@gmail.com"
LICENSE = "mit"

HELP = <<~help
#{$PROGRAM_NAME} 
  This scripts reads from a file a population loglist and extracts the 
  requested argument example

  Usage: #{$PROGRAM_NAME} [options]

help

MAN =<<~help    
  Example: #{$PROGRAM_NAME} --if=file --key=value-a

  file
  -------------
  ((gen 0) (value-a 101.0) (value-f "b") ... )
  ...
  ((gen 10) (value-a 110.0) (value-f "a") ... ) 
  -------------
  
  -------------
  101.0
  ...
  110.0
  -------------
help

options = {} 
parser = OptionParser.new do |opts|
  opts.banner = HELP
  opts.on("-f","--if=FNAME","the name of the file to read from") do |file|
    options[:file] = file 
  end 

  opts.on("-k","--key=SYMBOL","the key to search for") do |symb|
    options[:key] = symb
  end 
  opts.on("-H","--man","a more detaled description") do 
    puts MAN
    exit 0
  end 
  opts.on("-v","--version") do
    puts VERSION
    exit 0
  end 
end
parser.parse!

unless options[:file] && options[:key]
  puts parser.parse %w[--help]
  exit(1)
end 

# ----------------------------------------------------------------------------

def parse_sexp(str) 
  tokens = str.scan /[\s,]*(~@|[\[\]{}()'`~^@]|"(?:\\.|[^\\"])*"?|;.*|[^\s\[\]{}('"`,;)]*)/
  tokens = tokens.map do |m| m[0] end.select do |t| t!= "" && t[0..0] != ";" end 
  
  peek = ->() do tokens.first end 
  next_token = ->() do tokens.shift end 

  read_list = ->() do 
    if peek.call == "(" 
      list = [] 
      while true
        next_token.call
        if peek.call == ")"
          return list 
        end 
        list << read_list.call
      end 
    elsif peek == ")"
      next_token.call
      raise "unexpected )"
    else 
      return peek.call
    end 
  end 
  
  read_list.call 
end 

def asqq_ref(arr,key) 
  arr.each do |e|
    if e[0] == key 
      return e[1..] 
    end 
  end 
end 

def lisp_str(arr) 
  str = ""  
  if arr.is_a?(Array)
    str = "(" 
    (0 ... arr.size-1).each do |i| 
      str << lisp_str(arr[i]) << " "
    end 
    str << lisp_str(arr[-1])
    str << ")" 
  else 
    str << arr
  end 
  return str 
end 

# -----------------------------------------------------------------------------

inio = if options[:file] == "-" 
  STDIN
else
  File.open(options[:file])
end 

outio = STDOUT

key = options[:key] 

while line = inio.gets
  next if line =~ /\(configuration (.+?)\)/ 
  next unless line =~ /#{key}/
  arr = parse_sexp(line)
  val = asqq_ref(arr,key)
  puts (val.size == 1) ? val.first : lisp_str(val)

end 

inio.close

