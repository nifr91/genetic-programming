#!/usr/bin/env ruby


require "optparse"
options = {} 
ARGV.push "-h" if ARGV.empty? 
OptionParser.new do |opts|
  opts.banner = "Usate set.rb --instance=STR --nsamples=INT"
  opts.on("--instance=STR","Generated instance") do |str|
    options[:instance] = str
  end 
  opts.on("--nsamples=INT","number of samples") do |int|
    options[:nsamples] = int.to_i
  end
  opts.on("--val","is validation sampling") do 
    options[:val] = true
  end 
  opts.on_tail("-h","--help","Show this message") do 
    puts opts
    exit
  end 
end.parse!

val = options[:val]
nsamples = options[:nsamples] || 20


def linspace(low,high,num)
  step = (high-low).to_f/(num-1)
  [*0...num].map { |i| low + i.to_f * step }
end 

set = case options[:instance]
  when 
    "koza-01",
    "koza-02",
    "koza-03",
    "nguyen-01",
    "nguyen-03",
    "nguyen-04",
    "nguyen-05",
    "nguyen-06"
    nsamples.times.map{[rand(-1.0..1.0)]}.to_a
  when "nguyen-07"
    nsamples.times.map{[rand(0.0..2.0)]}.to_a
  when "nguyen-08"
    nsamples.times.map{[rand(0.0..4.0)]}.to_a
  when 
    "nguyen-09",
    "nguyen-10"
    rng =(-1.0..1.0)
    nsamples.times.map{ [rand(rng),rand(rng)]}.to_a
  when "pagie-01"   
    v = linspace(-5.0,5.0,nsamples)
    v.product(v)
  when "korns-01",
    "korns-02",
    "korns-03",
    "korns-04",
    "korns-05",
    "korns-06",
    "korns-07",
    "korns-08",
    "korns-09",
    "korns-10",
    "korns-11",
    "korns-12",
    "korns-13",
    "korns-14",
    "korns-15"
    rng = case options[:instance]
    when 
      "korns-05",
      "korns-06",
      "korns-08",
      "korns-09",
      "korns-15"
      (0.0..50.0)
    else 
      (-50.0..50.0)
    end 
    nsamples.times.map{5.times.map{rand(rng)}.to_a}.to_a
  when "keijzer-01" then linspace(-1.0,1.0,nsamples).map{|e| [e]}
  when "keijzer-02" then linspace(-2.0,2.0,nsamples).map{|e| [e]}
  when "keijzer-03" then linspace(-3.0,3.0,nsamples).map{|e| [e]}
  when "keijzer-04" 
    if val 
      linspace(0,10,nsamples).map{|e| [e]}
    else 
      linspace(0.05,10.05,nsamples).map{|e| [e]}
    end 
  when "keijzer-05"
    xzrng = (-1.0..1.0)
    yrng = (1.0..2.0)
    nsamples.times.map{[rand(xzrng),rand(yrng),rand(xzrng)]}.to_a
  when "keijzer-06"
    if val 
      linspace(1,121,nsamples).map{|e| [e.to_i]}
    else
      linspace(1,51,nsamples).map{|e| [e.to_i]}
    end 
  when "keijzer-07"
    linspace(1,100,nsamples).map{|e| [e]}
  when 
    "keijzer-08",
    "keijzer-09"
    linspace(0,100,nsamples).map{|e| [e]}
  when "keijzer-10"
    if val
      v = linspace(0.0,1.0,nsamples)
      v.product(v)
    else 
      rng = (0.0..1.0)
      nsamples.times.map{[rand(rng),rand(rng)]}.to_a
    end 
  when 
    "keijzer-11",
    "keijzer-12",
    "keijzer-13",
    "keijzer-14",
    "keijzer-15"
    if val
      v = linspace(-3.0,3.0,nsamples)
      v.product(v)
    else 
      rng = (-3.0..3.0)
      nsamples.times.map{[rand(rng),rand(rng)]}.to_a
    end 
  when "vladislavleva-01"
    if val 
      v = linspace(-0.2,4.2,nsamples)
      v.product(v)
    else
      rng = (0.3..4.0)
      nsamples.times.map{[rand(rng),rand(rng)]}.to_a
    end 
  when "vladislavleva-02"
    if val 
      linspace(-0.5,10.5,nsamples).map{|e| [e]}
    else
      linspace(0.05,10,nsamples).map{|e| [e]}
    end 
  when "vladislavleva-03"
    if val 
      x = linspace(-0.5,10.5,nsamples)
      y = linspace(-0.5,10.5,nsamples/10)
    else
      x = linspace(0.05,10,nsamples)
      y = linspace(0.05,10.05,nsamples/10)
    end
      x.product(y)
  when "vladislavleva-04" 
    if val 
      rng = (-0.25..6.35)
      nsamples.times.map{[
        rand(rng),
        rand(rng),
        rand(rng),
        rand(rng),
        rand(rng)]}.to_a 
    else 
      rng = (0.05..6.05)
      nsamples.times.map{[
        rand(rng),
        rand(rng),
        rand(rng),
        rand(rng),
        rand(rng)]}.to_a 
    end 
  when "vladislavleva-05"
    if val 
      x = linspace(-0.05,2.1,nsamples)
      y = linspace(0.95,2.05,nsamples)
      z = linspace(-0.05,2.1,nsamples)
      x.product(y).product(z)
    else
      xzrng = (0.05..2.0)
      yrng = (1.0..2.0)
      nsamples.times.map{[
        rand(xzrng),
        rand(yrng),
        rand(xzrng)]}.to_a
    end 
  when "vladislavleva-06"
    if val 
      v = linspace(-0.05,6.05,nsamples)
      v.product(v)
    else 
      rng = (0.1..5.9)
      nsamples.times.map{[rand(rng),rand(rng)]}.to_a
    end 
  when "vladislavleva-07"
    if val 
      rng = (-0.25..6.35)
    else
      rng = (0.05..6.05)
    end 
    nsamples.times.map{[rand(rng),rand(rng)]}.to_a
  when "vladislavleva-08"
    if val 
      v = linspace(-0.25,6.36,nsamples)
      v.product(v)
    else
      rng = (0.05..6.05)
      nsamples.times.map{[rand(rng),rand(rng)]}.to_a
    end 
  when nil then 
    raise "an instance name is required" 
  else raise "unkown instance #{options[:instance]}"
  end

set.each do |vars|
  puts vars.join(" ")
end
