#!/usr/bin/env ruby

require "optparse"
options = {} 
ARGV.push "-h" if ARGV.empty? 
OptionParser.new do |opts|
  opts.banner = "Usate set.rb --instance=STR --nsamples=INT"
  opts.on("--dir=STR","Directory where files are written") do |str|
    options[:dir] = str
  end 
  opts.on("--instance=STR","Generated instance") do |str|
    options[:instance] = str
  end 
  opts.on("--nsamples=INT","number of samples") do |int|
    options[:nsamples] = int.to_i
  end 
  opts.on("--val","is validation sample") do 
    options[:val] = true
  end 
  opts.on_tail("-h","--help","Show this message") do 
    puts opts
    exit
  end 
end.parse!

val = options[:val]
nsamples = options[:nsamples] || 20
instance = options[:instance]
dir = options[:dir]
fin = %(#{instance}.in)
fout = %(#{instance}.out)

if dir && !dir.empty?
  puts `mkdir -p #{dir}` 
  fout = File.join(dir,fout)
  fin = File.join(dir,fin)
end 

cmd = %(./set.rb --instance=#{instance} --nsamples=#{nsamples} #{val ? "--val" : ""})
cmd += " | "
cmd += %(tee #{fin})
cmd += " | "
cmd += %(./objfun.rb --instance=#{instance}) 
cmd += %( > #{fout})

puts cmd 
puts `#{cmd}`
