#!/usr/bin/env ruby
require "optparse"
options = {} 
stdin = nil
ARGV.push "-h" if ARGV.empty? 
OptionParser.new do |opts|
  opts.banner = "Usate set.rb --instance=STR --"
  opts.on("--instance=STR","Generated instance") do |str|
    options[:instance] = str
  end 
  opts.on("--nsamples=INT","number of samples") do |int|
    options[:nsamples] = int.to_i
  end 
  opts.on_tail("-h","--help","Show this message") do 
    puts opts
    exit
  end 
end.parse!


objfun = case options[:instance]
  when "koza-01"   then ->(x){x**4 + x**3 + x**2 + x}
  when "koza-02"   then ->(x){x**5 - (2*(x**3)) + x}
  when "koza-03"   then ->(x){x**6 - (2*(x**4)) + x}
  when "nguyen-01" then ->(x){x**3 + x**2 + x}
  when "nguyen-03" then ->(x){x**5 + x**4 + x**3 + x**2 + x}
  when "nguyen-04" then ->(x){x**6 + x**5 + x**4 + x**3 + x**2 + x}
  when "nguyen-05" then ->(x){Math.sin(x**2)*Math.cos(x) - 1}
  when "nguyen-06" then ->(x){Math.sin(x) + Math.sin(x + x**2)}
  when "nguyen-07" then ->(x){Math.log(x+1) + Math.log(x**2 + 1)}
  when "nguyen-08" then ->(x){Math.sqrt(x)}
  when "nguyen-09" then ->(x,y){Math.sin(x) + Math.sin(y**2)}
  when "nguyen-10" then ->(x,y){2*Math.sin(x)*Math.cos(y)}
  when "pagie-01"  then ->(x,y){(1.0/(1+(x**-4)))+(1.0/(1+(y**-4)))}
  when "korns-01"  then ->(x,y,z,v,w){1.57 + (24.3*v)}
  when "korns-02"  then ->(x,y,z,v,w){0.23 + (14.2*((v+y)/(2*w)))}
  when "korns-03"  then ->(x,y,z,v,w){-5.41 + (4.9*((v - x + (y/w))/(3*w)))}
  when "korns-04"  then ->(x,y,z,v,w){-2.3 + (0.13*Math.sin(z))}
  when "korns-05"  then ->(x,y,z,v,w){3 + (2.13*Math.log(w))}
  when "korns-06"  then ->(x,y,z,v,w){1.3 + (0.13*Math.sqrt(x))}
  when "korns-07"  then ->(x,y,z,v,w){213.80940889*(1.0-Math.exp(-0.54723748542*x))}
  when "korns-08"  then ->(x,y,z,v,w){6.87 + (11*Math.sqrt(7.23*x*v*w))}
  when "korns-09"  then ->(x,y,z,v,w){(Math.sqrt(x)*Math.exp(z))/(Math.log(y)*(v**2))}
  when "korns-10"  then ->(x,y,z,v,w){0.81 + (24.3)*(((2*y)+(3*(z**2)))/((4*(v**3))+(5*(w**4))))}
  when "korns-11"  then ->(x,y,z,v,w){6.87 + (11*Math.cos(7.23*(x**3)))}
  when "korns-12"  then ->(x,y,z,v,w){2 - (2.1*Math.cos(9.8*x)*Math.sin(1.3*w))}
  when "korns-13"  then ->(x,y,z,v,w){32 - (3*((Math.tan(x)*Math.tan(z))/(Math.tan(y)*Math.tan(v))))}
  when "korns-14"  then ->(x,y,z,v,w){22 - (4.2*(Math.cos(x) - Math.tan(y))*(Math.tanh(z)/Math.sin(v)))}
  when "korns-15"  then ->(x,y,z,v,w){12 - (6*(Math.tan(x)/Math.exp(y))*(Math.log(z) - Math.tan(v)))}
  when 
    "keijzer-01",
    "keijzer-02",
    "keijzer-03"          then ->(x){0.3*x*Math.sin(2*Math::PI*x)}
  when "keijzer-04"       
    ->(x){(x**3)*Math.exp(-x)*Math.cos(x)*Math.sin(x)*((Math.sin(x)**2)*Math.cos(x)-1)}
  when "keijzer-05"       then ->(x,y,z){(30*x*z)/((x-10)*(y**2))}
  when "keijzer-06"       then ->(x){(1..x).reduce(0.0){|acc,i| acc + (1.0/i)}}
  when "keijzer-07"       then ->(x){Math.log(x)}
  when "keijzer-08"       then ->(x){Math.sqrt(x)}
  when "keijzer-09"       then ->(x){Math.log(x + Math.sqrt(x**2 + 1))}
  when "keijzer-10"       then ->(x,y){x**y}
  when "keijzer-11"       then ->(x,y){x*y + Math.sin((x-1)*(y-1))}
  when "keijzer-12"       then ->(x,y){x**4 - x**3 + ((y**2)/2) - y}
  when "keijzer-13"       then ->(x,y){6*Math.sin(x)*Math.cos(y)}
  when "keijzer-14"       then ->(x,y){8.0/(2+(x**2)+(y**2))}
  when "keijzer-15"       then ->(x,y){((x**3)/5.0) + ((y**3)/2.0) - y - x}
  when "vladislavleva-01" 
    ->(x,y){(Math.exp(-((x-1)**2)))/(1.2 + ((y-2.5)**2))}
  when "vladislavleva-02" 
    ->(x){Math.exp(-x)*(x**3)*(Math.cos(x)*Math.sin(x))*(Math.cos(x)*(Math.sin(x)**2)- 1)}
  when "vladislavleva-03"
    ->(x,y){Math.exp(-x)*(x**3)*(Math.cos(x)*Math.sin(x))*(Math.cos(x)*(Math.sin(x)**2) - 1)*(y - 5)}
  when "vladislavleva-04" 
    ->(x,y,z,v,w){10.0/(5 + ((x-3)**2) + ((y-3)**2) + ((z-3)**2) + ((v-3)*2) + ((w-3)**2))}
  when "vladislavleva-05"
    ->(x,y,z){30*((x-1.0)*(z-1.0)/((y**2)*(x-10)))}
  when "vladislavleva-06"
    ->(x,y){6*Math.sin(x)*Math.cos(y)}
  when "vladislavleva-07"
    ->(x,y){(x-3)*(y-3)+2*Math.sin((x-4)*(y-4))}
  when "vladislavleva-08"
    ->(x,y){((x-3)**4 + ((y-3)**3) - (y-3))/(((y-2)**4)+10)}
  when nil         then raise "an instance name is required"
  else raise "unkown instance #{options[:instance]}"
end 

while line = STDIN.gets 
  vars = line.split(/\s+/).map{|e| e.to_f}
  puts objfun.call(*vars)

end 
