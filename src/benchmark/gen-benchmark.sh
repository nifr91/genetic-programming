#!/usr/bin/env sh

# Training 
./sample.rb --instance=koza-01          --nsamples=20     --dir=training
./sample.rb --instance=koza-02          --nsamples=20     --dir=training
./sample.rb --instance=koza-03          --nsamples=20     --dir=training
./sample.rb --instance=nguyen-01        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-03        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-04        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-05        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-06        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-07        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-08        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-09        --nsamples=20     --dir=training
./sample.rb --instance=nguyen-10        --nsamples=20     --dir=training
./sample.rb --instance=pagie-01         --nsamples=26     --dir=training 
./sample.rb --instance=korns-01         --nsamples=10000  --dir=training
./sample.rb --instance=korns-02         --nsamples=10000  --dir=training
./sample.rb --instance=korns-03         --nsamples=10000  --dir=training
./sample.rb --instance=korns-04         --nsamples=10000  --dir=training
./sample.rb --instance=korns-05         --nsamples=10000  --dir=training
./sample.rb --instance=korns-06         --nsamples=10000  --dir=training
./sample.rb --instance=korns-07         --nsamples=10000  --dir=training
./sample.rb --instance=korns-08         --nsamples=10000  --dir=training
./sample.rb --instance=korns-08         --nsamples=10000  --dir=training
./sample.rb --instance=korns-09         --nsamples=10000  --dir=training
./sample.rb --instance=korns-10         --nsamples=10000  --dir=training
./sample.rb --instance=korns-11         --nsamples=10000  --dir=training
./sample.rb --instance=korns-12         --nsamples=10000  --dir=training
./sample.rb --instance=korns-13         --nsamples=10000  --dir=training
./sample.rb --instance=korns-14         --nsamples=10000  --dir=training
./sample.rb --instance=korns-15         --nsamples=10000  --dir=training
./sample.rb --instance=keijzer-01       --nsamples=21     --dir=training
./sample.rb --instance=keijzer-02       --nsamples=41     --dir=training
./sample.rb --instance=keijzer-03       --nsamples=61     --dir=training
./sample.rb --instance=keijzer-04       --nsamples=201    --dir=training
./sample.rb --instance=keijzer-05       --nsamples=1000   --dir=training
./sample.rb --instance=keijzer-06       --nsamples=50     --dir=training
./sample.rb --instance=keijzer-07       --nsamples=100     --dir=training
./sample.rb --instance=keijzer-08       --nsamples=101    --dir=training
./sample.rb --instance=keijzer-09       --nsamples=101    --dir=training
./sample.rb --instance=keijzer-10       --nsamples=100    --dir=training
./sample.rb --instance=keijzer-11       --nsamples=20     --dir=training
./sample.rb --instance=keijzer-12       --nsamples=20     --dir=training
./sample.rb --instance=keijzer-13       --nsamples=20     --dir=training
./sample.rb --instance=keijzer-14       --nsamples=20     --dir=training
./sample.rb --instance=keijzer-15       --nsamples=20     --dir=training
./sample.rb --instance=vladislavleva-01 --nsamples=100    --dir=training
./sample.rb --instance=vladislavleva-02 --nsamples=96    --dir=training
./sample.rb --instance=vladislavleva-03 --nsamples=96    --dir=training
./sample.rb --instance=vladislavleva-04 --nsamples=1024   --dir=training
./sample.rb --instance=vladislavleva-05 --nsamples=300    --dir=training
./sample.rb --instance=vladislavleva-06 --nsamples=30     --dir=training
./sample.rb --instance=vladislavleva-07 --nsamples=300    --dir=training
./sample.rb --instance=vladislavleva-08 --nsamples=50     --dir=training

# Validation 
./sample.rb --instance=koza-01          --nsamples=100    --dir=validation --val
./sample.rb --instance=koza-02          --nsamples=100    --dir=validation --val
./sample.rb --instance=koza-03          --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-01        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-03        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-04        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-05        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-06        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-07        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-08        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-09        --nsamples=100    --dir=validation --val
./sample.rb --instance=nguyen-10        --nsamples=100    --dir=validation --val
./sample.rb --instance=pagie-01         --nsamples=101    --dir=validation --val
./sample.rb --instance=korns-01         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-02         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-03         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-04         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-05         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-06         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-07         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-08         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-08         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-09         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-10         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-11         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-12         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-13         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-14         --nsamples=10000  --dir=validation --val
./sample.rb --instance=korns-15         --nsamples=10000  --dir=validation --val
./sample.rb --instance=keijzer-01       --nsamples=2001   --dir=validation --val
./sample.rb --instance=keijzer-02       --nsamples=4001   --dir=validation --val
./sample.rb --instance=keijzer-03       --nsamples=6001   --dir=validation --val
./sample.rb --instance=keijzer-04       --nsamples=201    --dir=validation --val
./sample.rb --instance=keijzer-05       --nsamples=10000  --dir=validation --val
./sample.rb --instance=keijzer-06       --nsamples=120    --dir=validation --val
./sample.rb --instance=keijzer-07       --nsamples=1000   --dir=validation --val
./sample.rb --instance=keijzer-08       --nsamples=1001   --dir=validation --val
./sample.rb --instance=keijzer-09       --nsamples=1001   --dir=validation --val
./sample.rb --instance=keijzer-10       --nsamples=101    --dir=validation --val
./sample.rb --instance=keijzer-11       --nsamples=601    --dir=validation --val
./sample.rb --instance=keijzer-12       --nsamples=601    --dir=validation --val
./sample.rb --instance=keijzer-13       --nsamples=601    --dir=validation --val
./sample.rb --instance=keijzer-14       --nsamples=601    --dir=validation --val
./sample.rb --instance=keijzer-15       --nsamples=601    --dir=validation --val
./sample.rb --instance=vladislavleva-01 --nsamples=441    --dir=validation --val
./sample.rb --instance=vladislavleva-02 --nsamples=221    --dir=validation --val
./sample.rb --instance=vladislavleva-03 --nsamples=221    --dir=validation --val
./sample.rb --instance=vladislavleva-04 --nsamples=5000   --dir=validation --val
./sample.rb --instance=vladislavleva-05 --nsamples=16     --dir=validation --val
./sample.rb --instance=vladislavleva-06 --nsamples=306    --dir=validation --val
./sample.rb --instance=vladislavleva-07 --nsamples=1000   --dir=validation --val
./sample.rb --instance=vladislavleva-08 --nsamples=34     --dir=validation --val
