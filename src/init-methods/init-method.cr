require "../individual.cr" 
class GP::InitMethod
  @proc : Proc(Array(Indv))
  @name : String 
  def initialize(
    @name = "dummy",
    @proc  = ->(){Array.new(50){Indv.new(Crisp::Sexp.new(Crisp::Num.new(0)),Crisp::Env.new)}})
  end 
  def call(*args)
    @proc.call(*args)
  end 
  def to_s(io)
    io << @name
  end 
end 

require "./rhh.cr"
