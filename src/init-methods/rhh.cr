class GP::InitMethod
  private def self.sample_term(tset)
    term = tset.sample 
    term = (term.is_a?(Ephemeral) ? term.val : term)
  end 

  private def self.sample_func(fset)
    fset.sample
  end 

  private def self.grow(
    min_depth : Int32,
    max_depth : Int32,
    parent : Sexp | Nil,
    depth : Int32,
    env : Env,
    fprb : Float64)

    func = sample_func env.fset
    term = sample_term env.tset

    var = if ( depth > max_depth -1)
      term 
    elsif depth < min_depth || (rand(0.0..1.0) < fprb)
      func
    else 
      term 
    end 

    sexp = Sexp.new(var,parent)

    sym = sexp.val
    return sexp unless sym.is_a?(Symb)

    sym.arity.times do 
      s = grow(min_depth,max_depth,sexp,depth+1,env,fprb)
      sexp.push(s)
    end 
   
    return sexp
  end 

  def self.grow( min_depth : Int32, max_depth : Int32, env : Env, fprb=0.5)
    grow(min_depth,max_depth,nil,1,env,fprb)
  end 

  def self.full(depth : Int32, env : Env) 
    grow(depth,depth,nil,1,env,1.0) 
  end 
  
  def self.rhh(pop_size,max_depth,env) 
    proc = ->() do 
      sexps = self.ramped_half_and_half(pop_size,max_depth,env) 
      sexps.map do |s|
        Indv.new(s,env.clone)
      end 
    end 
    return self.new("ramped-half-and-half",proc)
  end 

  def self.ramped_half_and_half(pop_size,max_depth,env)
    ng = ((pop_size) // (max_depth))//2 
    pop = [] of Sexp 
    (1 .. max_depth).each do |d|
      (0 ... ng).each do 
        pop.push(grow(1,d,env))
        pop.push(full(d,env))
      end 
    end 
    while pop.size < pop_size 
      pop.push(grow(1,max_depth,env))
    end 

    pop.shuffle!
    return pop
    end 
end 
