class GP::Distance
  
  @proc : Proc(Sexp,Sexp,Float64)
  @name : String 

  def initialize(
    @name = "constant",
    @proc = ->(ta : Sexp, tb : Sexp){1.0})
  end 
  def call (*args) 
    @proc.call(*args)
  end 
  def to_s(io)
    io << @name 
  end 



  # The distance of two nodes is 0 if they are equal and 1 if they are 
  # different. 
  # 
  # p : node -- first node 
  # q : node -- second node
  # 
  # int -- 1 if different 0 otherwise 
  #
  def self.node(p,q)
    return (p == q) ? 0 : 1
  end 

 
  # The distance of two trees as the sum of the distances of the corresponding
  # nodes (i.e nodes that overlap when the two trees are overlaid, starting
  # from the root).
  # 
  # based on: 
  #   reducing bloat and promoting diversity using multi objective methods
  #   edwin d. de jong
  #   2001
  # 
  # ta : sexp -- first tree 
  # tb : sexp -- second tree 
  #  
  # int -- the number of nodes that are different
  #
  def self.overlap_normalized(ta : Sexp, tb : Sexp)
    return Distance.overlap(ta,tb) / Math.min(ta.size, tb.size)
  end 
  def self.overlap(ta : Sexp,tb : Sexp)
    sum = 0
    # recursive case, there can be an overlap 
    unless ta.term? || tb.term? 
      arity = Math.min(ta.childs.size,tb.childs.size)
      sum = 0
      arity.times do |i|
        if (sta = ta.childs[i]?) && (stb = tb.childs[i]?)
          sum += Distance.overlap(sta,stb)
        end 
      end 
    end 
    sum += Distance.node(ta.val,tb.val)
    return sum 
  end 



  # 
  def self.edit2_normalized(ta : Sexp, tb : Sexp,k : Float64)
    dist,tree_size = Distance.edit2(ta,tb,k: k,dn: Sexp.new(Symb.new("nil",0)))
    return (dist / tree_size)
  end 

  def self.edit2(ta : Sexp | Nil, tb : Sexp | Nil,k : Float64,dn : Sexp) 
    total_dist = 0 
    expanded_tree_size = 0  

    # recursive case, one of the nodes have a child, so the childs are 
    # compared
    unless (ta.term? && tb.term?) 
      arity = Math.max(ta.childs.size,tb.childs.size)
      arity.times do |i|
        sta = ta.childs[i]? || dn
        stb = tb.childs[i]? || dn

        dist,nnodes = Distance.edit2(sta,stb,k,dn)
        total_dist += dist
        expanded_tree_size += nnodes
      end 
    end 
    
    # base case, the nodes are compared
    expanded_tree_size *= k 
    expanded_tree_size += 1.0
    total_dist = Distance.node(ta.val,tb.val) + k * total_dist

    return {total_dist,expanded_tree_size}
  end 



  # Average of the distance function of each element in 'pop' to each element 
  # in 'ref'. 
  #
  # pop      : [indv]             -- array of individuals 
  # ref      : [indv]             -- reference population to compare to
  # dist_fun : (sexp,sexp)->float -- distance function of the s-expressions
  # 
  # [float] -- average of the distances
  #
  def self.avg(pop,ref,dist_fun)
    pop.map do |indv|
      dist = 1.0 
      ref.each do |rindv| 
        d = dist_fun.call(indv.sexp,rindv.sexp)
        dist += d
      end 
      dist /= ref.size
    end 
  end 

  # Average of the distance function of each element in 'pop' to each other 
  # element in 'pop - {indv}'
  #
  # pop      : [indv]             -- array of individuals
  # dist_fun : (sexp,sexp)->float -- distance function between s-expressions
  #
  # [float] -- average distances
  #
  def self.avg(pop,dist_fun)
    pop.map_with_index do |indv,i|
      dist = 1.0 
      pop.each_with_index do |rindv,j| 
        next if (i == j)
        d = dist_fun.call(indv.sexp,rindv.sexp)
        dist += d
      end 
      dist /= (pop.size-1)
    end 
  end 

  # Distance to the closest neighbor of each element in 'pop' to each element
  # in 'ref'. 
  #
  # pop      : [indv]             -- array of individuals 
  # ref      : [indv]             -- reference population to compare to
  # dist_fun : (sexp,sexp)->float -- distance function of the s-expressions
  # eps      : float              -- epsilon to consider two numbers equal
  # 
  # [float] -- the minimum distance
  #
  def self.dcn(pop,ref,dist_fun,eps)
    pop.map do |indv| 
      dist = 1.0 
      ref.each do |rindv|
        d = dist_fun.call(indv,rindv)
        dist = d if (d-dist) < -eps
      end 
      dist
    end 
  end

  # Distance to the closest neighbor of each element in 'pop' to each element
  # in 'pop - {indv}'. 
  #
  # pop      : [indv]             -- array of individuals 
  # dist_fun : (indv,indv)->float -- distance function of the s-expressions
  # eps      : float              -- epsilon to consider two numbers equal
  # 
  # [float] -- the minimum distance
  #
  def self.dcn(pop,dist_fun,eps)
    pop.map_with_index do |indv,i| 
      dist = 1.0 
      pop.each_with_index do |rindv,j|
        next if (i == j)
        d = dist_fun.call(indv,rindv)
        dist = d if (d-dist) < -eps
      end 
      dist
    end 
  end 


  # The hamming distance between two strings of equal length is the number 
  # of positions at which the corresponding symbols are different.  
  # It measures te minimum number of substitutions required to change one 
  # string into the other. 
  def self.hamming(a,b) 
    return a.size.times.reduce(0) do |acc,i| 
      acc + ((a[i] == b[i]) ?  0 : 1)
    end 
  end 
  
  def self.euclidean(a,b)
    sum = a.size.times.reduce(0.0) do |acc,i|
      acc + (a[i].to_f - b[i].to_f)**2.0
    end
    return Math.sqrt(sum)
  end 

  # Distance of y to p
  # 
  # How much far is y to p , the max distance is when y is 0 and p is infinity. The distance 
  # is 0 when p dominates y. 
  #
  #
  def self.igdplus(y,p,epsilon)
    sum = y.size.times.reduce(0.0) do |acc, i|
      diff = p[i] - y[i] 
      diff = (diff > epsilon) ? diff : 0 
      acc + (diff**2.0)
    end
    return Math.sqrt(sum)
  end 
  
end  
